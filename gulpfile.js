var
    autoprefixer    = require('gulp-autoprefixer'),
    chmod           = require('gulp-chmod'),
    convertEncoding = require('gulp-convert-encoding'),
    concat          = require('gulp-concat'),
    del             = require('del'),
    gulp            = require('gulp'),
    notify          = require('gulp-notify'),
    plumber         = require('gulp-plumber'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify');

var libs= [
        'script/libs/jquery-2*.js',
        'script/libs/*.js'
    ],
    jsPrimary = [
        'script/*.js'
    ];

//FUNCION  DE ERROR DE HUGO, CON EL GULP-NOTIFY
var onError = function (err) {
    notify.onError({
        title: "Gulp",
        subtitle: "Failure!",
        message: "Error: <%= error.message %>",
        sound: "Beep"
    })(err);

    this.emit('end');
};

gulp.task('js', function(){

    console.log('Eliminando todos los ficheros de la carpeta ./dist');
    del.sync('./dist/*.js');//S�lo eliminamos los js al estar en la tarea de js ( obvio )
    console.log('Ficheros eliminados');

    gulp.src(libs)
        .pipe(plumber({ errorHandler: onError }))
        .pipe(concat('libs.js'))
        // .pipe(
        //     uglify({
        //     output: { // http://lisperator.net/uglifyjs/codegen
        //         beautify: false,
        //         comments: false
        //     },
        //     compress: { // http://lisperator.net/uglifyjs/compress, http://davidwalsh.name/compress-uglify
        //         sequences    : true,
        //         booleans     : true,
        //         conditionals : true,
        //         hoist_funs   : true,
        //         hoist_vars   : true,
        //         warnings     : true
        //     },
        //     mangle: true,
        //     outSourceMap: false})
        // )
        .pipe(gulp.dest('./dist'));

    console.log(jsPrimary);

    gulp.src(jsPrimary)
        .pipe(plumber({ errorHandler: onError }))
        .pipe(concat('myodon.js'))
        // .pipe(
        //     uglify({
        //     output: { // http://lisperator.net/uglifyjs/codegen
        //         beautify: false,
        //         comments: false
        //     },
        //     compress: { // http://lisperator.net/uglifyjs/compress, http://davidwalsh.name/compress-uglify
        //         sequences    : true,
        //         booleans     : true,
        //         conditionals : true,
        //         hoist_funs   : true,
        //         hoist_vars   : true,
        //         warnings     : true
        //     },
        //     mangle: true,
        //     outSourceMap: false})
        // )
        .pipe(gulp.dest('./dist'));

});

gulp.task('css', function () {

    del.sync('./dist/*.css');//S�lo eliminamos los css al estar en la tarea de css ( obvio )

  gulp.src(['./css/jquery-ui.css','./scss/**/main.scss'])
  //  gulp.src(['./scss/**/main.scss'])
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass( {errLogToConsole: true} ))
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist'));

});

gulp.task('printcss', function () {

    del.sync('./dist/*.css');//S�lo eliminamos los js al estar en la tarea de js ( obvio )

    gulp.src('./scss/**/print.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass( {errLogToConsole: true} ))
        .pipe(concat('print.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist'));

});
gulp.task('images', function () {

    del.sync('./dist/img/*.*');//S�lo eliminamos los js al estar en la tarea de js ( obvio )

    gulp.src('./img/*.*')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(gulp.dest('./dist/img'));

});
gulp.task('fonts', function () {

    del.sync('./dist/fonts/*.*');//S�lo eliminamos los js al estar en la tarea de js ( obvio )

    gulp.src('./fonts/*.*')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(gulp.dest('./dist/fonts'));

});
gulp.task('svg', function () {

    del.sync('./dist/svg/**/*.svg');//S�lo eliminamos los svg al estar en la tarea de svg ( obvio )

    gulp.src('./svg/**/*.svg')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(gulp.dest('./dist/svg'));

});
gulp.task('dev',function(){
    del.sync('./dist/*.*');
    gulp.run('css');
    gulp.run('printcss');
    gulp.run('js');
    gulp.run('svg');
    gulp.run('images');
    gulp.run('fonts');

});

gulp.task('watch', function () {

    gulp.run('dev');

    gulp.watch(['./script/*.js'], function () {
        gulp.run('js');
    });

    gulp.watch( './scss/**/*.scss' , function () {
        gulp.run('css');
        gulp.run('printcss');
    });
    gulp.watch(['./svg/**/*.svg'], function () {
        gulp.run('svg');
    });

});

gulp.task('default', ['watch']);