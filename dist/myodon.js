var configGlobal = {
    'constants': {
        'sex': {
            'male':0,
            'female':1,
            'unknown':2
        },
        'ajaxLoader': 1,
        'pathSVG': './dist/svg',
        'pathPrintCss': './dist/print.css',
        'idsvgbase': 'base',
        'svgbase': '/baseTotal.svg',
        'generateimg': {
            'save' : 0,
            'end' : 1
        },
        'yrect': {
            'finalBot': 950,
            'finalTop': 0,
            'tempsBot': 900,
            'tempsTop': 50
        },
        'zones': {
            'mesial'     : 'm',
            'lingual'    : 'l',
            'distal'     : 'd',
            'oclusal'    : 'o',
            'vestibular' : 'v',
            'raiz1'      : 'r1',
            'raiz2'      : 'r2',
            'raiz3'      : 'r3'

        },
        'ynumber': {
            'finalBot': 850,
            'finalTop': 150,
            'tempsBot': 520,
            'tempsTop': 490
        },
        'positiondent'  : {
            'top': {
                'final': 0,
                'temp': 1
            },
            'bot': {
                'temp': 2,
                'final': 3
            }
        },
        'actiontype' : {
            'new' : 1,
            'remain' : 2
        },
        'tipotool'  : {
            'desdedienteadiente' : 1,
            'todafila' : 2,
            'detallezona' : 3,
            'pieza': 4,
            'grupopiezas': 5,
            'dosdientes': 6,
            'detallelinea': 7,
            'detallecirculo': 8
        }
    },
    'teeth': [
    {
        'contiguous': [
        21,
        12
        ],
        'id': 11,
        'map': 'd11',
        'position': 0,
        'svg': '/dientes/d11.svg',
        'text': 11
    },
    {
        'contiguous': [
        14,
        16
        ],
        'id': 15,
        'map': 'd15',
        'position': 0,
        'svg': '/dientes/d15.svg',
        'text': 15
    },
    {
        'contiguous': [
        17,
        15
        ],
        'id': 16,
        'map': 'd16',
        'position': 0,
        'svg': '/dientes/d16.svg',
        'text': 16
    },
    {
        'contiguous': [
        11,
        13
        ],
        'id': 12,
        'map': 'd12',
        'position': 0,
        'svg': '/dientes/d12.svg',
        'text': 12
    },
    {
        'contiguous': [
        12,
        14
        ],
        'id': 13,
        'map': 'd13',
        'position': 0,
        'svg': '/dientes/d13.svg',
        'text': 13
    },
    {
        'contiguous': [
        13,
        15
        ],
        'id': 14,
        'map': 'd14',
        'position': 0,
        'svg': '/dientes/d14.svg',
        'text': 14
    },
    {
        'contiguous': [
        11,
        22
        ],
        'id': 21,
        'map': 'd21',
        'position': 0,
        'svg': '/dientes/d21.svg',
        'text': 21
    },
    {
        'contiguous': [
        21,
        23
        ],
        'id': 22,
        'map': 'd22',
        'position': 0,
        'svg': '/dientes/d22.svg',
        'text': 22
    },
    {
        'contiguous': [
        22,
        24
        ],
        'id': 23,
        'map': 'd23',
        'position': 0,
        'svg': '/dientes/d23.svg',
        'text': 23
    },
    {
        'contiguous': [
        23,
        25
        ],
        'id': 24,
        'map': 'd24',
        'position': 0,
        'svg': '/dientes/d24.svg',
        'text': 24
    },
    {
        'contiguous': [
        24,
        26
        ],
        'id': 25,
        'map': 'd25',
        'position': 0,
        'svg': '/dientes/d25.svg',
        'text': 25
    },
    {
        'contiguous': [
        25,
        27
        ],
        'id': 26,
        'map': 'd26',
        'position': 0,
        'svg': '/dientes/d26.svg',
        'text': 26
    },
    {
        'contiguous': [
        26,
        28
        ],
        'id': 27,
        'map': 'd27',
        'position': 0,
        'svg': '/dientes/d27.svg',
        'text': 27
    },
    {
        'contiguous': [
        27
        ],
        'id': 28,
        'map': 'd28',
        'position': 0,
        'svg': '/dientes/d28.svg',
        'text': 28
    },
    {
        'contiguous': [
        84
        ],
        'id': 85,
        'map': 'd85',
        'position': 2,
        'svg': '/dientes/d85.svg',
        'text': 85
    },
    {
        'contiguous': [
        46,
        44
        ],
        'id': 45,
        'map': 'd45',
        'position': 3,
        'svg': '/dientes/d45.svg',
        'text': 45
    },
    {
        'contiguous': [
        18,
        16
        ],
        'id': 17,
        'map': 'd17',
        'position': 0,
        'svg': '/dientes/d17.svg',
        'text': 17
    },
    {
        'contiguous': [
        17
        ],
        'id': 18,
        'map': 'd18',
        'position': 0,
        'svg': '/dientes/d18.svg',
        'text': 18
    },
    {
        'contiguous': [
        55,
        53
        ],
        'id': 54,
        'map': 'd54',
        'position': 1,
        'svg': '/dientes/d54.svg',
        'text': 54
    },
    {
        'contiguous': [
        54,
        52
        ],
        'id': 53,
        'map': 'd53',
        'position': 1,
        'svg': '/dientes/d53.svg',
        'text': 53
    },
    {
        'contiguous': [
        53,
        51
        ],
        'id': 52,
        'map': 'd52',
        'position': 1,
        'svg': '/dientes/d52.svg',
        'text': 52
    },
    {
        'contiguous': [
        52,
        61
        ],
        'id': 51,
        'map': 'd51',
        'position': 1,
        'svg': '/dientes/d51.svg',
        'text': 51
    },
    {
        'contiguous': [
        51,
        62
        ],
        'id': 61,
        'map': 'd61',
        'position': 1,
        'svg': '/dientes/d61.svg',
        'text': 61
    },
    {
        'contiguous': [
        61,
        63
        ],
        'id': 62,
        'map': 'd62',
        'position': 1,
        'svg': '/dientes/d62.svg',
        'text': 62
    },
    {
        'contiguous': [
        62,
        64
        ],
        'id': 63,
        'map': 'd63',
        'position': 1,
        'svg': '/dientes/d63.svg',
        'text': 63
    },
    {
        'contiguous': [
        85,
        83
        ],
        'id': 84,
        'map': 'd84',
        'position': 2,
        'svg': '/dientes/d84.svg',
        'text': 84
    },
    {
        'contiguous': [
        84,
        82
        ],
        'id': 83,
        'map': 'd83',
        'position': 2,
        'svg': '/dientes/d83.svg',
        'text': 83
    },
    {
        'contiguous': [
        83,
        81
        ],
        'id': 82,
        'map': 'd82',
        'position': 2,
        'svg': '/dientes/d82.svg',
        'text': 82
    },
    {
        'contiguous': [
        82,
        71
        ],
        'id': 81,
        'map': 'd81',
        'position': 2,
        'svg': '/dientes/d81.svg',
        'text': 81
    },
    {
        'contiguous': [
        81,
        72
        ],
        'id': 71,
        'map': 'd71',
        'position': 2,
        'svg': '/dientes/d71.svg',
        'text': 71
    },
    {
        'contiguous': [
        71,
        73
        ],
        'id': 72,
        'map': 'd72',
        'position': 2,
        'svg': '/dientes/d72.svg',
        'text': 72
    },
    {
        'contiguous': [
        72,
        74
        ],
        'id': 73,
        'map': 'd73',
        'position': 2,
        'svg': '/dientes/d73.svg',
        'text': 73
    },
    {
        'contiguous': [
        73,
        75
        ],
        'id': 74,
        'map': 'd74',
        'position': 2,
        'svg': '/dientes/d74.svg',
        'text': 74
    },
    {
        'contiguous': [
        74
        ],
        'id': 75,
        'map': 'd75',
        'position': 2,
        'svg': '/dientes/d75.svg',
        'text': 75
    },
    {
        'contiguous': [
        63,
        65
        ],
        'id': 64,
        'map': 'd64',
        'position': 1,
        'svg': '/dientes/d64.svg',
        'text': 64
    },
    {
        'contiguous': [
        64
        ],
        'id': 65,
        'map': 'd65',
        'position': 1,
        'svg': '/dientes/d65.svg',
        'text': 65
    },
    {
        'contiguous': [
        47
        ],
        'id': 48,
        'map': 'd48',
        'position': 3,
        'svg': '/dientes/d48.svg',
        'text': 48
    },
    {
        'contiguous': [
        48,
        46
        ],
        'id': 47,
        'map': 'd47',
        'position': 3,
        'svg': '/dientes/d47.svg',
        'text': 47
    },
    {
        'contiguous': [
        47,
        45
        ],
        'id': 46,
        'map': 'd46',
        'position': 3,
        'svg': '/dientes/d46.svg',
        'text': 46
    },
    {
        'contiguous': [
        45,
        43
        ],
        'id': 44,
        'map': 'd44',
        'position': 3,
        'svg': '/dientes/d44.svg',
        'text': 44
    },
    {
        'contiguous': [
        44,
        42
        ],
        'id': 43,
        'map': 'd43',
        'position': 3,
        'svg': '/dientes/d43.svg',
        'text': 43
    },
    {
        'contiguous': [
        43,
        41
        ],
        'id': 42,
        'map': 'd42',
        'position': 3,
        'svg': '/dientes/d42.svg',
        'text': 42
    },
    {
        'contiguous': [
        42,
        31
        ],
        'id': 41,
        'map': 'd41',
        'position': 3,
        'svg': '/dientes/d41.svg',
        'text': 41
    },
    {
        'contiguous': [
        41,
        32
        ],
        'id': 31,
        'map': 'd31',
        'position': 3,
        'svg': '/dientes/d31.svg',
        'text': 31
    },
    {
        'contiguous': [
        31,
        33
        ],
        'id': 32,
        'map': 'd32',
        'position': 3,
        'svg': '/dientes/d32.svg',
        'text': 32
    },
    {
        'contiguous': [
        32,
        34
        ],
        'id': 33,
        'map': 'd33',
        'position': 3,
        'svg': '/dientes/d33.svg',
        'text': 33
    },
    {
        'contiguous': [
        33,
        35
        ],
        'id': 34,
        'map': 'd34',
        'position': 3,
        'svg': '/dientes/d34.svg',
        'text': 34
    },
    {
        'contiguous': [
        34,
        36
        ],
        'id': 35,
        'map': 'd35',
        'position': 3,
        'svg': '/dientes/d35.svg',
        'text': 35
    },
    {
        'contiguous': [
        35,
        37
        ],
        'id': 36,
        'map': 'd36',
        'position': 3,
        'svg': '/dientes/d36.svg',
        'text': 36
    },
    {
        'contiguous': [
        36,
        38
        ],
        'id': 37,
        'map': 'd37',
        'position': 3,
        'svg': '/dientes/d37.svg',
        'text': 37
    },
    {
        'contiguous': [
        37
        ],
        'id': 38,
        'map': 'd38',
        'position': 3,
        'svg': '/dientes/d38.svg',
        'text': 38
    },
    {
        'contiguous': [
        54
        ],
        'id': 55,
        'map': 'd55',
        'position': 1,
        'svg': '/dientes/d55.svg',
        'text': 55
    }
    ],
    'tools': [
    {

        'id'      : 1,
        'ico'     : '/tools/01_ApOrtFijo.svg',
        'text'    : 'Ap. Ort. Fijo',
        'desc'    : 'Aparato ortodóntico fijo',
        'tipo'    : 1,
        'svg'     : '/ops/01_ApOrtFIjo_Medio.svg',
        'svg2'    : '/ops/01_ApOrtFIjo_Extremo.svg',
        'class'   : ['operation--blue', 'operation--red'],
        'lastturned': 1,
        'reversible' : 1,
        'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },



    },
    {

        'id'      : 2,
        'ico'     : '/tools/02_ApOrtRem.svg',
        'text'    : 'Ap. Ort. Remov.',
        'desc'    : 'Aparato ortodóntico removible',
        'tipo'    : 2,
        'svg'     : '/ops/02_ApOrtRem.svg',
        'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },

    },
    {
        'id'    : 3,
        'ico'   : '/tools/03_Caries.svg',
        'text'  : 'Caries',
        'desc'  : 'Caries',
        'tipo'  : 3,
        'zoom'  : true,
        'class' : 'caries',
        'zones': ['v','o','m','l','d','r1','r2','r3']

    },
    {
        'id'       : 4,
        'box' : ['CC','CF','CMC','3/4','4/5','7/8','CV','CJ'],
        'ico'      : '/tools/04_CoronaDef.svg',
        'text'     : 'Corona Definitiva',
        'desc'     : 'Corona Definitiva',
        'tipo'     : 4,
        'svg'        : '/ops/04_CoronaDef.svg',
        'reversible' : 1,
        'txtclass'            : 'od-op-txt od-op-txt--blue',

    },
    {
        'color'            : 'red',
        'id'               : 5,
        'box'         : ['CC','CF','CMC','3/4','4/5','7/8','CV','CJ'],
        'txtclass'            : 'od-op-txt od-op-txt--blue',
        'ico'              : '/tools/05_CoronaTemp.svg',
        'text'             : 'Corona Temporal',
        'desc'             : 'Corona Temporal',
        'tipo'             : 4,
        'svg'        : '/ops/05_CoronaTemp.svg',
        'reversible' : 1,
    },

    {
     'id'         : 6,
     'ico'        : '/tools/06_Diastema.svg',
     'text'       : 'Diastema',
     'desc'       : 'Diastema',
     'tipo'       : 6,
     'svg'        : '/ops/06_Diastema.svg',
     'reversible' : 1
 },
 {
     'color'      : 'blue',
     'id'         : 7,
     'ico'        : '/tools/07_Ausente.svg',
     'text'       : 'Diente Ausente',
     'desc'       : 'Diente Ausente',
     'tipo'       : 4,
     'svg'        : '/ops/07_Ausente.svg',
     'reversible' : 1
 },
 {
    'color'            : 'blue',
    'id'               : 8,
    'box'              : ['DIS'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
    'ico'              : '/tools/08_Discromico.svg',
    'text'             : 'Discrómico',
    'desc'             : 'Diente Discrómico',
    'tipo'             : 4,

},
{
 'id'         : 9,
 'ico'        : '/tools/09_Ectopico.svg',
 'text'       : 'Ectópico',
 'desc'       : 'Diente Ectópico',
 'tipo'       : 4,
    'box'              : ['E'],
    'class'            : 'operation--blue',
    'txtclass'            : 'od-op-txt od-op-txt--blue',

},
{
 'color'      : 'blue',
 'id'         : 10,
 'ico'        : '/tools/10_Erupcion.svg',
 'text'       : 'Erupción',
 'desc'       : 'Diente En Erupción',
 'tipo'       : 4,
 'svg'        : '/ops/10_Erupcion.svg',
 'reversible' : 1
},
{
    'id'               : 11,
    'ico'              : '/tools/11_Clavija.svg',
    'text'             : 'Clavija',
    'desc'             : 'Diente En Clavija',
    'svg'        : '/ops/11_Clavija.svg',
    'tipo'             : 4,
    'offsetsvg' : {
                'y': -55,
                'y1': 135,
                'y2': -65,
                'y3': 110,
             },
},
{
    'color'            : 'blue',
    'id'               : 12,
    'ico'              : '/tools/12_Extrusion.svg',
    'text'             : 'Extrusión',
    'desc'             : 'Diente Extruido',
      'svg'        : '/ops/12_Extrusion.svg',
    'tipo'             : 4,
    'offsetsvg' : {
                'y': 135,

             },
    'reversible' :1,
},
{

    'id'               : 13,
    'ico'              : '/tools/13_Intrusion.svg',
    'text'             : 'Intrusión',
    'desc'             : 'Diente Intruido',
    'svg'        : '/ops/13_Intrusion.svg',
    'tipo'      : 4,
        'offsetsvg' : {
                'y': 135,
             },
    'reversible' :1,

},
{
    'color'            : 'blue',
    'id'               : 14,
    'ico'              : '/tools/14_Edentulo.svg',
    'text'             : 'Edéntulo Total',
    'desc'             : 'Edéntulo Total',
    'tipo'             : 2,
    'svg'              : '/ops/14_Edentulo.svg',
    'offsetsvg' : {
                'x': -7,
                'y': 85,
                'y2': 0,
                'y3': 0
             },
},
{
    'id'    : 15,
    'ico'   : '/tools/15_Fractura.svg',
    'text'  : 'Fractura',
    'desc'  : 'Fractura',
    'tipo'  : 7,
    'zoom'  : true,
    'class' : 'fractura'


},
{
    'id'               : 16,
    'ico'              : '/tools/16_Geminacion.svg',
    'svg'              : '/ops/16_Geminacion.svg',
    'text'             : 'Geminación Fusión',
    'desc'             : 'Geminación Fusión',
    'tipo'             : 6,
    'reversible' : 1,
    'deciduoinverso' : 1,
    'offsetsvg' : {
        'x':-20 ,
        'y': -40,
        'y3': -25,
        'xtemp':-23,
        'ytemp': 145
    }
},
{
    'color'            : 'blue',
    'id'               : 17,
    'ico'              : '/tools/17_Giroversion.svg',
    'text'             : 'Giroversión',
    'desc'             : 'Giroversión',
    'svg'              : '/ops/17_Giroversion.svg',
    'tipo'             : 4,
    'class'            : ['operation--noturned', 'operation--rotated'],
        'offsetsvg' : {
                'y': 120,

             },
    'reversible' : 2,
},
{
    'color'            : 'blue',
    'id'               : 18,
    'ico'              : '/tools/18_Impactacion.svg',
    'text'             : 'Impactación',
    'desc'             : 'Impactación',
    'tipo'             : 4,
    'box'              : ['I'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 19,
    'ico'              : '/tools/19_Implante.svg',
    'text'             : 'Implante',
    'desc'             : 'Implante',
    'tipo'             : 4,
      'box'              : ['IMP'],
      'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 20,
    'ico'              : '/tools/20_Macrodoncia.svg',
    'text'             : 'Macrodoncia',
    'desc'             : 'Macrodoncia',
    'tipo'             : 4,
    'box'              : ['MAC'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 21,
    'ico'              : '/tools/21_Microdoncia.svg',
    'text'             : 'Microdoncia',
    'desc'             : 'Microdoncia',
    'tipo'             : 4,
       'box'              : ['MIC'],
       'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 22,
    'ico'              : '/tools/22_Migracion.svg',
    'svg'              : '/ops/22_Migracion.svg',
    'text'             : 'Migración',
    'desc'             : 'Migración',
    'tipo'             : 4,
    'class'            : ['operation--noturned', 'operation--rotated'],
            'offsetsvg' : {
                'y': 120,
 'y2': -40,
 'y3': -40,
             },
    'reversible' :0,

},
{
    'color'            : 'blue',
    'id'               : 23,
    'ico'              : '/tools/23_Movilidad.svg',
    'text'             : 'Movilidad',
    'desc'             : 'Movilidad',
    'tipo'             : 4,
    'box'              : ['M1','M2','M3'],
    'class'            : 'operation--blue',
    'txtclass'            : 'od-op-txt od-op-txt--blue',
},
{
 'class'            : ['operation--blue', 'operation--red'],
 'id'               : 24,
 'ico'              : '/tools/24_Perno.svg',
 'text'             : 'Perno M. Espigo. M.',
 'desc'             : 'Perno Muñón Espigo Muñón',
 'tipo'             : 4,
 'svg'              : '/ops/24_Perno.svg',
 'reversible'        : 1

},
{
    'color'            : 'blue',
    'id'               : 25,
    'ico'              : '/tools/25_ProtesisFija.svg',
    'text'             : 'Prótesis Fija',
    'desc'             : 'Prótesis Fija',
    'svg'              : '/ops/25_ProtesisFija_Medio.svg',
    'svg2'              : '/ops/25_ProtesisFija_Extremo.svg',
    'tipo'             : 1,
    'lastturned'        :1,
    'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },
},
{
    'color'            : 'blue',
    'id'               : 26,
    'ico'              : '/tools/26_ProtesisRemov.svg',
    'text'             : 'Prótesis Removible',
    'desc'             : 'Prótesis Removible',
    'svg'              : '/ops/26_ProtesisRemov.svg',
    'tipo'             : 1,
    'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },

},
{
    'color'            : 'blue',
    'id'               : 27,
    'ico'              : '/tools/27_ProtesisTotal.svg',
    'class'   : ['operation--blue', 'operation--red'],
    'text'             : 'Prótesis Total',
    'desc'             : 'Prótesis Total',
    'svg'              : '/ops/27_ProtesisTotal.svg',
    'tipo'             : 2,
    'offsetsvg' : {
                'x': -7,
                'y': 85,
                'y2': 0,
                'y3': 0
             },
},
{
    'color'      : 'red',
    'id'         : 28,
    'ico'        : '/tools/28_RemRadic.svg',
    'text'       : 'Remanente Radicular',
    'desc'       : 'Remanente Radicular',
    'tipo'       : 4,
    'svg'        : '/ops/28_RemRadic.svg',
    'reversible' : 0
},
{
    'id'               : 29,
    'ico'              : '/tools/29_RestDef.svg',
    'text'             : 'Rest. Definitiva',
    'box'           :  ['AM','R','IV','IM','IE','CP'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'desc'             : 'Restauración Definitiva',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'restDef',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 30,
    'ico'              : '/tools/30_RestTemp.svg',
    'text'             : 'Rest. Temporal',
    'desc'             : 'Restauración Temporal',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'restTemp',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 31,
    'ico'              : '/tools/31_Sellantes.svg',
    'text'             : 'Sellantes',
    'desc'             : 'Sellantes',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'sellantes',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 32,
    'ico'              : '/tools/32_SemiImpactacion.svg',
    'text'             : 'Semi-Impactación',
    'desc'             : 'Semi-Impactación',
    'tipo'             : 4,
    'box'              : ['SI'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 33,
    'ico'              : '/tools/33_SuperfDesgast.svg',
    'text'             : 'Superficie Desgastada',
    'desc'             : 'Superficie Desgastada',
    'tipo'             : 4,
    'box'              : ['DES'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 34,
    'ico'              : '/tools/34_Supernumerario.svg',
    'text'             : 'Super numerario',
    'desc'             : 'Supernumerario',
    'tipo'             : 6,
    'svg'              : '/ops/34_Supernumerario.svg',
    'offsetsvg':  {'x':0,'y':-20},
    'reversible' :1
},
{
    'id'               : 35,
    'ico'              : '/tools/35_Transposicion.svg',
    'svg'              : '/ops/35_Transposicion.svg',
    'text'             : 'Transposición',
    'desc'             : 'Transposición',
    'tipo'             : 6,
    'offsetsvg':  {'x':0,'y':-40,
 'ytemp': -20},
    'reversible': 1
},
{

    'id'   : 36,
    'ico'  : '/tools/36_TratPulpar.svg',
    'text' : 'Trat. Pulpar',
    'desc' : 'Tratamiento Pulpar',
    'tipo' : 3,
    'zoom': 1,
    'tipo'  : 3,
    'class' : 'tratpulpar',
    'zones': ['r1','r2','r3'],
    'fn' : 'drawMiddleLane',
    'box' : ['TC','PC','PP'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',

},
{
    'id'               : 0,
    'ico'              : '/tools/00_Zoom.svg',
    'text'             : 'Zoom',
    'desc'             : 'Zoom',
    'tipo'             : 0,
    'default'   :1,
    'hidden' : 1,
    'zoom': 1
}

]
}
"use strict";
jQuery.fn.extend({
	createOd: function(customConfig) {
		var _this = this;
		var Odon = new Odontogram();
		customConfig = customConfig || function(){},

		Odon.configure(_this[0].id,customConfig);

		return Odon;
	},
	clearOd: function(){
		return '' ;
	}
});
$.attrHooks['viewbox'] = {
	set: function(elem, value, name) {
		elem.setAttributeNS(null, 'viewBox', value + '');
		return value;
	}
};
"use strict";
function Odontogram(yFinalTop,yTempsTop,yFinalBot,yTempsBot) {
	this.idodontogram = null;
	this.teeth = [];
	this.mainView = '';
	this.$mainView = {};
	this.datadocument = {};
	this.configuration = {};
	this.configurationDefault = {};
	this._toolSelected = { };
	this.operations = [];
	this.dateod = {};
	this.disabled = 0;
	this.$divToothDrag = {};
	this.$divHistory = {};
	this.$divMbox = {};
	this.ready = 0;
	var _odont = this;
	Object.defineProperty(this,'toolSelected',{
		get: function() { return this._toolSelected; },
		set: function(newValue) {
			var _this = this;
			var $divboxes = {};
			_this._toolSelected = newValue;
			if(_this.toothSelected.length >0){
				$.grep(_this.teeth, function(el) {
					//if ($.inArray(el, _this.toothSelected) >= 0) el.cancelActive();
					if (_this.toothSelected.indexOf(el) > -1){
						el.cancelActive();
					}
				});
			}
			$('.od-media__title','.od-toolbar__tool--selected').text(newValue.text);
			$('.od-media__image','.od-toolbar__tool--selected').find('img').attr('src', newValue.ico);
			$divboxes = $('.od-media__options', '.od-toolbar__tool--selected');
			$divboxes.html('');
			if (!!newValue.box){
				if(newValue.box.length> 1){
					for (var i = 0; i < newValue.box.length; i++) {


						$divboxes.append($('<div class="od-formgroup">' +
							'     <input class="od-radio" id="'+newValue.box[i]+'" name="tool" type="radio"  class="od-radio" value="'+newValue.box[i]+'">' +
							'     <label class="od-radio__label" for="'+newValue.box[i]+'">'+newValue.box[i]+'</label>' +
							'</div>'));
					}
					$('input[name=tool]:radio').change(function () {
						_this.toolSelected.boxSelected = this.id;
					});
					$('input[name=tool]:radio:first').prop( 'checked', true );

				}
				else{
					_this.toolSelected.boxSelected = newValue.box[0];
				}
			}
		}
	});
	this.toothSelected = [];
	this.configModalHist = {
		autoOpen:false,
		width:'auto',
		close: function(event) {
			$(this).html('');
			$(this).closest("div.ui-dialog").removeClass('pop-history--mousedown')
			if(!_odont.disabled) {
				enableTools();
				enableHistory();
			}
		},
		open: function(){
			disableTools();
			disableHistory();
		},
		modal:true,
	};
	this.divToothDragSave = function() {};
	this.configModalTooth = {
		width: 'auto',
		overflow: 'hidden',
		autoOpen: false,
		modal:true,
		buttons: {
			'Guardar': function(tooth) {
				$(this).data('odontogram').divToothDragSave();
				$(this).dialog( 'close' );
			},
			Cancelar: function() {
				$(this).dialog( 'close' );
			}
		},
		close: function(event) {
			$(this).html('');
			if(!$(this).data('odontogram').disabled) { enableTools(); }
		},
		open: function(){
			disableZoomTools();
		}
	};


	this.configModalMbox = {
		width: 'auto',
		overflow: 'hidden',
		autoOpen: false,
		modal:true,

		close: function(event) {
			$(this).html('');
		},

	};

	this.teethByPositions = {};
}

Odontogram.prototype.load = function (jsonLoad){
	var _this = this;
	if (!!jsonLoad.dateod) { _this.dateod = jsonLoad.dateod; }
	if (!!jsonLoad.idodontogram) { _this.idodontogram = jsonLoad.idodontogram; }
	if (!!jsonLoad.observations) { $('#txtobservations').val(jsonLoad.observations); }
	if (!!jsonLoad.specifications) { $('#txtspecifications').val(jsonLoad.specifications); }
	if (!!jsonLoad.patient) {
		if(!!jsonLoad.patient.dateborn){
			console.log(jsonLoad.patient.dateborn);
			$('#odon_birthday').html(new Date(jsonLoad.patient.dateborn.substring(0,10)).toLocaleDateString());
		}
		if(!!jsonLoad.patient.name){
			$('#odon_name').html(jsonLoad.patient.name);
		}
		if(!!jsonLoad.patient.photo){
			$('#odon_photo').attr('src',jsonLoad.patient.photo);
		}
	}
	if(!!jsonLoad.operations){
		_this.loadOperations(jsonLoad.operations);
	}
	if(!!jsonLoad.odonthist && jsonLoad.odonthist.length>0){
		_this.initHistory(jsonLoad.odonthist);
	}
	if (!!jsonLoad.closed) { _this.disable(); }


}
var _odHistory = [];
Odontogram.prototype.initHistory = function(odHistory){
	var _this = this;
	var selMenuHist = {};
	var year = '';
	var but = '';
	_odHistory = odHistory;
	selMenuHist = $('#menu-hist');
	selMenuHist.children().remove();
	for (var i = _odHistory.length - 1; i >= 0; i--) {
		_odHistory[i].dt = new Date(_odHistory[i].dateodhist.substring(0,10));
		_odHistory[i].idodontogram = _odHistory[i].idodhist;
	}
	_odHistory = _odHistory.sort(function(a,b){
		return a.dt > b.dt ? -1 : 1;
	}
	);
	for (var i = 0; i < _odHistory.length; i++) {
		year = _odHistory[i].dt.getFullYear();
		if($('.od-timeline__year[data-year="'+year+'"]',selMenuHist).length === 0){
			$('<li class="od-timeline__year" data-year="'+ year+'">'+ year+' </li>').appendTo(selMenuHist);
		}
		but = '<button class="od-btn od-btn--rounded od-btn--xs od-btn--star '+(!!_odHistory[i].fav? 'od-btn--checked':'')+'"></button>';
		$('<li class="od-timeline__item '+ ((_odHistory[i].idodhist ==_this.idodontogram) ? 'od-timeline__item--active' :'') +'" data-idodhist='+_odHistory[i].idodhist+'>'+'<a>'+
			_odHistory[i].dt.toLocaleDateString()  +'</a>'+but+'</li>').appendTo(selMenuHist);

	}
	$('.od-btn--star',selMenuHist).click(function(event){
		var li= $(event.target).closest('li');
		var idodhist =li.attr('data-idodhist');
		$(event.target).toggleClass('od-btn--checked');
		var his = $.grep(_odHistory, function(his) { return his.idodhist == idodhist  })[0];
		his.fav = !!his.fav ? 0 : 1;
		var json = { 'idodontogram' :   idodhist  , 'fav' : his.fav };
		_this.$mainView.trigger('Odontogram.updateFav',json);
		var childs = li.parents().children();
		for (var i = childs.index(li) - 1; i >= 0; i--) {
			var child = $(childs[i]);
			if(!!child.attr('data-year') && child.hasClass('od-timeline__year--collapsed')){
				li.toggle();
			}
		}
		event.preventDefault();
		event.stopPropagation();
	});
	$('.od-timeline__item',selMenuHist).click(function(event) {
		var idodhist = $(event.target).closest('li').attr('data-idodhist');
		var his = $.grep(_odHistory, function(his) { return his.idodhist === idodhist  });
		if (!!his.odontoimg) {
			_this.loadHistory(his);
		}
		else{
			var json  = { 'idodontogram' :   idodhist  };
			_this.$mainView.trigger('Odontogram.getHistory', json);
		}
	});
	$('.od-timeline__year',selMenuHist).click(function(asd,das,qwe,rewqd) {
		$(this).toggleClass('od-timeline__year--collapsed');
		var childs = $(this).parent().children();
		for (var i = childs.index(this)+1; i < childs.length; i++) {
			var child = $(childs[i]);
			if(!!child.attr('data-year')){
				break;
			}
			if(($('.od-btn--checked',child).length === 0)){
				child.toggle();
			}
		}
	} );
}


Odontogram.prototype.loadOperations = function(operationsJSON) {
	var _this = this;
	for (var i = operationsJSON.length - 1; i >= 0; i--) {
		var op = operationsJSON[i];
		var operation = new Operation();
		operation.idtypeoperation = op.idtypeoperation;
		var operConfig =  $.grep(_this.configuration.tools, function(e) { return e.id == op.idtypeoperation })[0];
		operation.dateop = op.dateop;
		operation.teeth = op.teeth;
		operation.text =  operConfig.text;
		operation.status = op.status;
		operation.action = op.action;
		if (!!op.zone){
			operation.zone = op.zone;
		}
		operation.id = _getOperationName(operation.teeth, op.idtypeoperation,op.zone);

		//if (operConfig.tipo == _this.configuration.constants.tipotool.detallelinea)
		//{
			if(!!op.positions) {
				operation.positions = op.positions;
			}
			if (!!op.box){
				operation.box =  op.box;
			}
		//}
		_drawOperation(operation,_this);
		//_writeHistoryOperation(operation, _this, operation.id, _this.configuration.constants.pathSVG);
		_this.operations.push(operation);
	}
}
Odontogram.prototype.disable = function() {
	this.disabled = 1;
	disableTools();
	disableHistory();
	disableOdontButtons();
	$('.od-toolbar').hide();
	$('.od-media-msgdisabled').show();
	this.toolSelectDefault();
	//$('.od-media__image','.od-toolbar__tool--selected').find('img').removeAttr('src');
	//$('.od-media__title','.od-toolbar__tool--selected').addClass('od-media__title--readonly').html('Odontograma en modo solo lectura');
	$('.od-media__date','.od-media-msgdisabled').html(new Date(this.dateod).toLocaleString());
}
Odontogram.prototype.enable = function() {
	this.disabled = 0;
	this.$divToothDrag.dialog('close')
	enableTools();
	enableHistory();
	enableOdontButtons();
	//$('.od-media__title','.od-toolbar__tool--selected').removeClass('od-media__title--readonly')
	$('.od-toolbar').show();
	$('.od-media-msgdisabled').hide();
	this.toolSelectDefault();
}
var enableOdontButtons = function(){
	$('.od-btn','.od-media__buttons').removeClass ('od-btn--disabled');
}

var disableOdontButtons = function(){
	$('.od-btn','.od-media__buttons').addClass('od-btn--disabled');
}

var _createDivSpinner = function(mainView){
	$('<div class="odonspinner"><div class="odonspinner__item"/></div>').insertBefore('#'+mainView);
	$(document)
        // Listener a ajaxStart
        .ajaxStart(function () {
        	Odontogram.spinnerShow();
        })
        // Listener a ajaxStop
        .ajaxStop(function () {
        	Odontogram.spinnerHide();
        });
    }

    Odontogram.spinnerShow = function(nodelay){
    	showSpinner = true;
    	if(!!nodelay){
    		$('.odonspinner').removeClass('none');
    	}
    	window.setTimeout(function () {
    		if ( !$('.odonspinner').is(':visible') ){
    			if ( showSpinner ){
    				$('.odonspinner').removeClass('none');
    			}
    		}
    	}, 300);
    };

    Odontogram.spinnerHide = function(){

    	showSpinner = false;

    	if ( $('.odonspinner').is(':visible') ){
    		$('.odonspinner').addClass('none');
    	}

    };
    Odontogram.prototype.mbox = function(title,message,buttons){
    	var _this = this;
    	var cfg = {};
    	if (!buttons) {
    		buttons = { 	Cerrar: function() { 		$(this).dialog( 'close' );    	}}
    	}
    	cfg.buttons = buttons;
    	cfg.title = title;
    	_this.$divMbox.dialog($.extend(cfg,_this.configModalMbox));
    	_this.$divMbox.html(message);
    	_this.$divMbox.dialog('open');
    }
    Odontogram.prototype.configure = function (mainView,config,failure) {
    	var _this = this,
    	failure = failure || function(){};
    	_this.configuration = configGlobal;
    	_this.configurationDefault = configGlobal;
    	_this.mainView = mainView;
    	_overlapConfig(_this.configuration,config);
    	_createDivSpinner(mainView);
    	_menuLateralLeftCreate(mainView,_this.configuration.constants.pathSVG);
    	_menuLateralRightCreate(mainView,_this.configuration.constants.pathSVG);
    	_toolboxGenerate(_this,'main-menu');
    	_loadPrintCss(_this.configuration.constants.pathPrintCss);
    	_loadTeethByPositions(_this);
    	$.ajax({
    		type: 'GET',
    		url: _this.configuration.constants.pathSVG + _this.configuration.constants.svgbase,
    		datatype: 'text',
    	}).done(function(data) {
    		_this.$mainView =$('#'+mainView);
    		_this.datadocument = data.documentElement;
		//_this.datadocument.classList.add('odontogram-base');
		$(_this.datadocument).addClass('odontogram-base');

		_this.$mainView.append( _this.datadocument);
		_loadTeeth(_this,config,failure);
		_this.$divToothDrag = $('<div id="tooth_drag">');
		_this.$divToothDrag.appendTo(_this.$mainView);
		_this.$divToothDrag.dialog(_this.configModalTooth);
		_this.$divToothDrag.closest('div.ui-dialog').addClass('pop-tooth');
		_this.$divHistory = $('<div id="odont_hist"/>');
		_this.$divHistory.appendTo(_this.$mainView.parent());
		_this.$divHistory.dialog(_this.configModalHist);
		_this.$divHistory.closest("div.ui-dialog").addClass('pop-history');
		_this.$divMbox = $('<div id="odont_mbox"/>');
		_this.$divMbox.appendTo(_this.$mainView.parent());
		_this.$divMbox.dialog(_this.configModalMbox);
		_this.$divMbox.closest("div.ui-dialog").addClass('pop-mbox');
		_this.$mainView.on( 'Odontogram.init', function(){_init(_this);});
		_createFilters(_this.datadocument);
		$(".od-tab__content").mCustomScrollbar({
			theme:"minimal-dark"

					// autoExpandScrollbar:true
				});
		$(".od-container").mCustomScrollbar({
			theme:"minimal-light",
			autoHideScrollbar: true
					// autoExpandScrollbar:true
				});

	}).fail( function(error){
		failure(error);
	});
}
var _createFilterBorder = function(id,color,docum){
	var NS = "http://www.w3.org/2000/svg";
	var filter = document.createElementNS( NS, "filter" );
	filter.setAttribute( 'id', id );
	var feMorphology = document.createElementNS( NS, 'feMorphology' );
	//feMorphology.setAttribute( 'operator', 'erade' );
	feMorphology.setAttribute( 'radius', '3' );
	feMorphology.setAttribute( 'in', 'SourceGraphic' );
	feMorphology.setAttribute( 'result', 'thickInner' );
	var feFlood = document.createElementNS( NS, 'feFlood' );
	feFlood.setAttribute( 'flood-color', color );
	feFlood.setAttribute( 'result', 'COLOR' );

	var feComposite1 = document.createElementNS( NS, 'feComposite' );
	feComposite1.setAttribute( 'in', 'COLOR' );
	feComposite1.setAttribute( 'in2', 'SourceGraphic' );
	feComposite1.setAttribute( 'operator', 'in' );
	feComposite1.setAttribute( 'result', 'thickTotal' );

	var feComposite2 = document.createElementNS( NS, 'feComposite' );
	feComposite2.setAttribute( 'in', 'COLOR' );
	feComposite2.setAttribute( 'in2', 'thickInner' );
	feComposite2.setAttribute( 'operator', 'in' );
	feComposite2.setAttribute( 'result', 'thickInner' );

	var feComposite3 = document.createElementNS( NS, 'feComposite' );
	feComposite3.setAttribute( 'operator', 'out' );
	feComposite3.setAttribute( 'in', 'thickTotal' );
	feComposite3.setAttribute( 'in2', 'thickInner' );
	feComposite3.setAttribute( 'result', 'thickDif' );

	var feComposite4 = document.createElementNS( NS, 'feComposite' );
	feComposite4.setAttribute( 'in', 'SourceGraphic' );
	feComposite4.setAttribute( 'in2', 'thickDif' );
	feComposite4.setAttribute( 'operator', 'out' );
	feComposite4.setAttribute( 'result', 'specOut' );


	var feMerge = document.createElementNS( NS, 'feMerge' );
	var feMergeNode1 = document.createElementNS( NS, 'feMergeNode' );
	feMergeNode1.setAttribute( 'in', 'specOut' );

	var feMergeNode2 = document.createElementNS( NS, 'feMergeNode' );
	feMergeNode2.setAttribute( 'in', 'thickDif' );

	document.getElementById('base').appendChild(filter);

	feMerge.appendChild(feMergeNode1);
	feMerge.appendChild(feMergeNode2);

	filter.appendChild(feMorphology);
	filter.appendChild(feFlood);
	filter.appendChild(feComposite1);
	filter.appendChild(feComposite2);
	filter.appendChild(feComposite3);
	filter.appendChild(feComposite4);
	filter.appendChild(feMerge);

}
var _createFilters = function(docu){
	_createFilterBorder('borderblue','#028bca',docu);
	_createFilterBorder('borderred','#aa0000',docu);

}
var _loadTeethByPositions = function(odont){
	for (var i = odont.configuration.teeth.length - 1; i >= 0; i--) {
		var t = odont.configuration.teeth[i];
		if (t.contiguous.length == 1){
			if($.isEmptyObject(odont.teethByPositions) || !odont.teethByPositions[t.position]){
				odont.teethByPositions[t.position] = _getAllContiguousTeethFirstTime(t.id,odont.configuration.teeth);
			}
			else{
				if(odont.teethByPositions[t.position][0] > t.id){
					odont.teethByPositions[t.position] = _getAllContiguousTeethFirstTime(t.id,odont.configuration.teeth);
				}
			}
		}
	}
}

var _menuLateralRightCreate = function(mainView,pathSVG){
	$('<div class="aside--right">' +
		'      <button class="od-btn od-btn--primary od-btn--rounded aside__trigger">' +
		'        <img src="'+ pathSVG + '/ui/panel_ico.svg" alt="" class="od-ico ico--panel">' +
		'        <img src="'+ pathSVG + '/ui/arrow_fwd.svg" alt="" class="od-ico ico--fwd">' +
		'      </button>' +
		'      <div class="panel">' +
		'        <div class="panel__title">Especificaciones</div>' +
		'        <div class="panel__body">' +
		'          <textarea name="especificaciones" id="txtspecifications" cols="30" rows="10"></textarea>' +
		'        </div>' +
		'      </div>' +
		'      <div class="panel">' +
		'        <div class="panel__title">Observaciones</div>' +
		'        <div class="panel__body">' +
		'          <textarea name="Observaciones" id="txtobservations" cols="30" rows="10"></textarea>' +
		'        </div>' +
		'      </div>' +
		'      <div class="panel panel--historia">' +
		'        <div class="panel__title">Historial</div>' +
		'        <div class="panel__body">' +
		'				<input type="radio" name="tabs-radio"  class="od-radio tabs-radio" id="od-tab__rad1" checked >'+
		'				<div class="od-tab">'+
		'          			<label for="od-tab__rad1" class="od-tabs__label">Operaciones</label>' +
		'					<div class="od-tab__content">'+
		'          				<ul class="od-list od-list--selectable" id="menu-ops">' +
		'         				</ul>' +
		'					</div>'+
		'				</div>'+
		'				<input type="radio" name="tabs-radio" id="od-tab__rad2" class="od-radio tabs-radio"  >'+
		'				<div class="od-tab">'+
		'          			<label for="od-tab__rad2" class="od-tabs__label">Odontogramas</label>' +
		'					<div class="od-tab__content">'+
		'          				<ul class="od-timeline" id="menu-hist">' +
		'          				</ul>' +
		'					</div>'+
		'				</div>'+
		'        </div>' +
		'      </div>' +
		'    </div>').insertAfter('#'+mainView);

}
var _menuLateralLeftCreate = function(mainView,pathSVG){
	$('<div id="menu-lateral" class="main-nav menu-lateral">' +
		'<header class="menu-lateral__header">' +
		'<a href="javascript:void(0);" class="main-logo">' +
		'<img src="img/hosix_logo.png" alt="HOSIX" data-pin-nopin="true">' +
		'<br> ODONTOGRAMA' +
		'</a>' +
		'</header>' +
		'<div class="od-media od-media--top od-user-profile">' +
		'<div class="od-media__image ">' +
		'<img id="odon_photo" class="od-thumbnail od-thumbnail--rounded ">' +
		'</div>' +
		'<div class="od-media__body  ">' +
		'<h4 class="od-media__title" id="odon_name"></h4>' +
		'<p class="od-media__text" id="odon_birthday"></p>' +
		'<div class="od-media__options od-media__buttons">' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Restaurar" data-balloon-pos="down" id="btnRestaurar">' +
		'<img src="'+ pathSVG + '/ui/restore.svg" alt="" class="od-ico">' +
		'</button>' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Guardar" data-balloon-pos="down" id="btnGuardar">' +
		'<img src="'+ pathSVG + '/ui/save.svg" alt="" class="od-ico">' +
		'</button>' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Finalizar" data-balloon-pos="down" id="btnFinalizar">' +
		'<img src="'+ pathSVG + '/ui/lock.svg" alt="" class="od-ico">' +
		'</button>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="od-media-msgdisabled" style="display:none;" >' +
		'<h5 class="od-media__title od-media__title--readonly">Odontograma en modo solo lectura</h5>' +
		'<div class="od-media__options--left od-media__date"></div>' +
		'</div>' +
		'<div class="main-menu" id="main-menu">' +
		'</div>' +
		'</div>').insertBefore('#'+mainView);
}

var _init = function (odontograma) {
	var _this = odontograma;
	//_this.resize();


	$('.tooth-trigger',_this.$mainView).click(function(event){
		var dataid = $(this).attr('data-idtooth');
		var tooth = $.grep(_this.teeth, function(e) { return e.id == dataid});
		if(tooth.length>0) {
			tooth[0].click(event);
		}
	});

	$(document).on('keyup.tooth',{},function(e) {
		if (e.keyCode === 27) {//ESC
			_this.enableTeeth();
			_this.deactivateTeeth();
			if (!_this.disabled) {
				enableHistory();
			}
		}
	});
	$('.od-btn','#menu-hist').bind('click', { odontogram : _this  },deleteOperation);
	$('.aside__trigger').click(function() {
		$('.aside--right').toggleClass('aside--collapsed');
	});
	_this.$mainView.trigger('Odontogram.ready');
	_this.ready = 1;

	$('#btnGuardar').click(function(){
		var json = _this.save(0,_this.configuration.constants.generateimg.save);
		_this.$mainView.trigger('Odontogram.save', [json]);
	});
	$('#btnFinalizar').click(function(){
		//var json = _this.save(1,_this.configuration.constants.generateimg.end);
		//_this.$mainView.trigger('Odontogram.end', [json]);
		var buttons = {
			Finalizar: function() {
				_this.dateod = new Date();
				var json = _this.save(1,_this.configuration.constants.generateimg.end);
				_this.$mainView.trigger('Odontogram.end', [json]);
				$(this).dialog( 'close' );
			},
			Cancelar: function() {
				$(this).dialog( 'close' );
			}};
			_this.mbox('Confirmación','Se va a finalizar el odontograma y no se podrá modificar, ¿desea continuar?',buttons);
		});
	$('#btnRestaurar').click(function(){
		_this.clear();
		var json = _this.save(0,0);
		_this.$mainView.trigger('Odontogram.restore', [json]);
	});
/*
	odontograma.$divHistory.on('mousedown',function(event){
 		//$(this).closest("div.ui-dialog").addClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").toggleClass('pop-history--mousedown');
 	});*/
	/*
	odontograma.$divHistory.on('mousedown',function(event){
 		//$(this).closest("div.ui-dialog").addClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").addClass('pop-history--mousedown');
	});
	odontograma.$divHistory.on('mouseup',function(){
		//$(this).closest("div.ui-dialog").removeClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").removeClass('pop-history--mousedown');
 	});*/

 }

 Odontogram.prototype.loadHistory =  function(json) {
 	var _this = this;
 	var w =_this.$mainView.parent().outerWidth();
 	var h =_this.$mainView.outerHeight();

 	$('#base').attr('width',w).attr('height',  h);

 	var odhis = $.grep(_odHistory, function(his) { return his.idodontogram == json.idodontogram})[0];
 	odhis.odontoimg = json.odontoimg;
 	odhis.specifications = json.specifications;
 	odhis.observations = json.observations;
 	_this.$divHistory.html('');
 	_this.$divHistory.append($('<img class="pop-history__img"  data-balloon="Click en la imagen para hacerla transparente"  data-balloon-pos="down" src="'+odhis.odontoimg+'"/>'));
 	_this.$divHistory.append(
 		$('<div class="pop-history__text">' +
 			'<h2 class="pop-history__heading">Especificaciones</h2>'+
 			'<p class="pop-history__p">'+odhis.specifications+'</p>' +
 			'<h2 class="pop-history__heading">Observaciones</h2>'+
 			'<p class="pop-history__p">'+odhis.observations+ '</p>' +
 			'</div>'
 			));
 	$('.pop-history__text',this.$divHistory).mCustomScrollbar({
 		theme:"minimal-dark"
 	});


 	_this.$divHistory.dialog({
 		'title': function() { $(this).html( 'Odontograma Histórico: <strong>'+odhis.dt.toLocaleDateString()+'</strong>')} ,
 		'width' : w-100,
 		'height' : h-100,
 	}).dialog('open');
 	$('.pop-history__img',_this.$divHistory).on('click',function(event){
 		_this.$divHistory.closest("div.ui-dialog").toggleClass('pop-history--mousedown');
 	});;


 }

 Odontogram.prototype.save =  function(close,generateImg) {
 	return odontogramToJSON(this,close,generateImg);
 }


 Odontogram.prototype.resize =  function() {
 	var _this = this.$mainView;
 	var w =_this.outerWidth();
 	var h =_this.outerHeight();

 	$('#base').attr('width',w).attr('height',  h);
 }

 Odontogram.prototype.clear =  function() {
 	var _this = this;
 	_clearOperations(_this);
 }


 var _overlapConfig =function(defaultConfig, newConfig) {
 	var keys = Object.keys(newConfig);
 	if(newConfig.hasOwnProperty('id')){
 		keys.splice(keys.indexOf('id'),1);
 	}
 	_subOverLapConfig(defaultConfig,newConfig,keys);
 }

 var _subOverLapConfig = function(defaultConfig,newConfig, keys){
 	for (var i = keys.length - 1; i >= 0; i--) {
 		if(typeof(defaultConfig[keys[i]]) === 'object'){
 			var  nextDefConfig = defaultConfig[keys[i]];
 			if(!defaultConfig[keys[i]].hasOwnProperty('id')){
 				_overlapConfig(defaultConfig[keys[i]],newConfig[keys[i]]);
 			}
 			else{
 				nextDefConfig = $.grep(defaultConfig, function(e){ return e.id == newConfig[keys[i]].id; })[0];
 				_overlapConfig(nextDefConfig,newConfig[keys[i]]);
 			}
 		}
 		else{
 			defaultConfig[keys[i]] = newConfig[keys[i]];
 		}
 	}
 }
 var _loadTeeth = function(odontogram,config,failure) {
 	var dien =  {},
 	toothConfiguration = {},
 	i = 0,
 	j =0,
 	objToothConfig = {},
 	times = {},
 	failure = failure || function(){};

 	times = { number: 0 ,total: odontogram.configuration.teeth.length};
 	for (i = 0; i < odontogram.configuration.teeth.length; i++) {
 		odontogram.teeth.push(new Tooth(odontogram.configuration.teeth[i]));
 		dien = odontogram.teeth[odontogram.teeth.length-1];
 		dien.callSVG(odontogram,function(){_teethWait(times,odontogram.$mainView);},failure);
 	}
 }
 var _teethWait = function(times,mainView){
 	times.number++;
 	if (times.number === times.total){
 		mainView.trigger('Odontogram.init');
 	}
 }

 Odontogram.prototype.toolSelectDefault =  function() {
 	$('.od-btn--active').removeClass('od-btn--active');
 	var _this = this;
 	_this.toolSelected = $.grep(_this.configuration.tools, function(obj) {
 		return obj.default === 1;
 	})[0];
 	if(!!_this.toolSelected.box) {
 		_this.toolSelected.boxSelected= _this.toolSelected.box[0];
 	}else{
 		_this.toolSelected.boxSelected= null;
 	}
 	if (!_this.toolSelected.hidden) {
 		$('.od-toolbar__btn[data-idtool='+_this.toolSelected.id+']').addClass('od-btn--active');
 	}
 }

 Odontogram.prototype.disableTeeth =  function(exceptions) {
 	var _this = this,
 	exceptions = exceptions || [];
 	for (var i = _this.teeth.length - 1; i >= 0; i--) {
 		if($.inArray(_this.teeth[i].id, exceptions)==-1){
 			_this.teeth[i].$datadocument.addClass('minitooth--disabled');
 			_this.teeth[i].$recuadrotooth.addClass('tooth-rect--disabled');
 		}
 	}
 }
 Odontogram.prototype.disableTeethExceptOperation =  function(operationId) {
 	var _this = this;
 	var oper = $.grep(_this.operations,function(e) { return e.id == operationId })[0];
 	var operConfig =  $.grep(_this.configuration.tools, function(e) { return e.id == oper.idtypeoperation })[0];
 	if (operConfig.tipo == _this.configuration.constants.tipotool.detallezona || $.isEmptyObject(oper.dom)){
 		var dtotal = $('#d'+oper.teeth[0]).find('#diente_total');
		//$('[id=diente_total]').not(dtotal).addClass('disabled');
		_this.disableTeeth(oper.teeth);
		$('.operation[id!='+operationId+']').addClass('disabled');
		//$('[id!='+oper.zone+']',dtotal).not('#dibujo').not('#corona').addClass('disabled')
		$.each(this.configuration.constants.zones, function(pos,zone)
			{if(zone != oper.zone)
				$('[id=' +zone+']',dtotal).addClass('disabled');
			});

	}
	else{
		//$('[id=diente_total]').addClass('disabled');
		_this.disableTeeth(oper.teeth);
		$('.operation[id!='+operationId+']').addClass('disabled');
	}
}

Odontogram.prototype.enableTeeth =  function() {
	$('.minitooth--disabled').removeClass('minitooth--disabled');
	$('.tooth-rect--disabled').removeClass('tooth-rect--disabled');
	$('[id=diente_total]').removeClass('disabled');
	$('.operation').removeClass('disabled');
	$.each(this.configuration.constants.zones, function(pos,zone) {$('[id=' +zone+']').removeClass('disabled');});
}
Odontogram.prototype.deactivateTeeth =  function() {
	var _this = this;
	$('.minitooth--active').removeClass('minitooth--active');
	$('.tooth-rect--active').removeClass('tooth-rect--active');
	_this.toothSelected = [];
}

Odontogram.prototype.findOperationsByTeeth =  function(teethIds,toolId){
	var _this = this;
	result = $.grep(_this.operations,function(e) {
		return (!toolId || e.idtypeoperation == toolId) && teethIds.every(function (t) { return e.teeth.indexOf(t) >= 0 })
	});
	return	 result;
}


_getAllContiguousTeethFirstTime = function(toothId,confTeeth){
	var _this = this;
	var wholeContiguous = [toothId];
	return _continueGetAllContiguousTeeth(toothId,wholeContiguous,confTeeth);
}

var _continueGetAllContiguousTeeth = function(toothId,wholeContiguous,configTeeth){
	var teethCfg  =$.grep(configTeeth, function(t) { return t.id == toothId})[0];
	for (var i = teethCfg.contiguous.length - 1; i >= 0; i--) {
		if(wholeContiguous.indexOf(teethCfg.contiguous[i]) == -1){
			wholeContiguous.push(teethCfg.contiguous[i]);
			_continueGetAllContiguousTeeth(teethCfg.contiguous[i],wholeContiguous,configTeeth);
		}
	}
	return wholeContiguous;
}

Odontogram.prototype.getAllContiguousTeeth = function(toothId){
	var tooth = $.grep(this.configuration.teeth, function(t) {  return t.id == toothId})[0];
	var copy = this.teethByPositions[tooth.position].slice(0);
	copy.sort();
	return copy;
}


Odontogram.prototype.getContiguousBetween2Tooth = function(toothIni,toothEnd){
	var tooth = $.grep(this.configuration.teeth, function(t) {  return t.id == toothIni})[0];
	var copy = this.teethByPositions[tooth.position].slice(0);
	var indexIni = copy.indexOf(toothIni);
	var indexEnd = copy.indexOf(toothEnd);
	if (indexIni > indexEnd){
		indexIni = [indexEnd, indexEnd = indexIni][0];//Esto es un swap de las variables
	}
	return copy.slice(indexIni,indexEnd+1).sort();
}





"use strict";
function Operation() {
	this.id = 0;
	this.idtypeoperation = 0;
	this.dom = {};
	this.teeth = [];
	this.text = '';
	this.dateop = new Date();
	this.status = 0; // 0 – mal estado 1- buen estado
	this.action = 1; //Nuevo / mantenida
	this.positions = []; //Solo para detalles de linea
}

var _replaceArray = ('dom id').split( ' '  );
function replacer(key, value) {
	if (_replaceArray.indexOf(key) >= 0){
		return undefined;
	}
	return value;
}
var operationToJSON = function(operation){
	// var operationCopy = $(this).clone(boolean, boolean);
	//var operationCopy = {};
	return JSON.stringify(operation,replacer)
	/*
	$.extend( operationCopy, operation );
	delete operationCopy['dom'];
	delete operationCopy['teeth'];
	return JSON.stringify(operationCopy);*/
}
var odontogramToJSON = function(odontogram,closed,genretaImg){
	var jsonCreator = {};
	jsonCreator.dateod = new Date();
	jsonCreator.observations = $('#txtobservations').val();
	jsonCreator.specifications = $('#txtspecifications').val();
	jsonCreator.active = 1;
	jsonCreator.closed = closed;
	jsonCreator.operations = [];
	if (!!odontogram.idodontogram) jsonCreator.idodontogram = odontogram.idodontogram;
	if(!!genretaImg) {
		jsonCreator.odontosvg = _generateImg();
	}
	for (var i = odontogram.operations.length - 1; i >= 0; i--) {
		jsonCreator.operations.push(JSON.parse(operationToJSON(odontogram.operations[i])));
	}
	return JSON.stringify(jsonCreator);
}

/*
var _drawSVGOperation = function(operation,tooth,data,$node) {
	var bbox = {};
	var $circChild = {};
	var $datadocument = $(data.documentElement);
	if ($node.find('#op'+operation.id)){

	}
	$datadocument.attr('id','op'+operation.id).addClass(operation.class);
	if (!!operation.class) {
		if ($.isArray(operation.class)) {
			$datadocument.addClass(operation.class[0]);
		}else		{
			$datadocument.addClass(operation.class);
		}
	}
	$node.append( $datadocument[0]);
}*/
var _checkClassSVG = function(configClass,operation,$datadocument,odont){
	if (!!configClass) {
		if ($.isArray(configClass)) {
			$datadocument.addClass(configClass[operation.status]);
			operation.class = configClass;
			if(configClass.length > 1){
				$datadocument.addClass('operation--click')
				$datadocument.click(function(){

					_clickStatusSVG(configClass,operation,odont);
				});
			}
		}else		{
			$datadocument.addClass(configClass);
			operation.class = configClass;
		}
	}
}

var _clickStatusSVG = function(configClass,operation,odont){
	var oldClass;
	var newClass;
	if($.isArray(configClass) > 0){
		operation.status++;
		operation.dateop = new Date();
		if ( operation.status >= configClass.length )	{
			operation.status= 0;
			oldClass = configClass[configClass.length-1];
			newClass = configClass[operation.status];
		}else{
			newClass = configClass[operation.status];
			oldClass = configClass[operation.status-1];
		}
		$('.'+oldClass,operation.dom).removeClass(oldClass).addClass(newClass);
		operation.dom.removeClass(oldClass).addClass(newClass);

		if(newClass == 'operation--rotated'){
			operation.dom.attr('transform',   operation.dom.attr('transform') + ' translate(50,0)  scale(-1,1)');
		}else if(oldClass){
			operation.dom.attr('transform', operation.dom.attr('transform').replace(' translate(50,0)  scale(-1,1)',''));
		}
		_writeHistoryOperation(operation, odont, operation.id, odont.configuration.constants.pathSVG);
	}
}
var _drawSVGOperation = function(operation,odont,$node) {
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var opname = _getOperationName(operation.teeth, operation.idtypeoperation);
	var $datadocument = {};

	_checkClassSVG(operConfig.class,operation,$node,odont);

	if (operConfig.tipo == odont.configuration.constants.tipotool.pieza ||
		operConfig.tipo == odont.configuration.constants.tipotool.dosdientes	)
	{
		$datadocument = operConfig.$svgdata.clone();
	//	_checkClassSVG(operConfig.class,operation,$datadocument,odont);

	$node.append( $datadocument[0]);
	_adjustPosition(operation,odont,$node,operConfig);
}
else{
	if(!!operConfig.$svgdata2){
		for (var i = operation.teeth.length - 1; i >= 0; i--) {
			var tooth = $.grep(odont.configuration.teeth, function(t) { return t.id == operation.teeth[i];})[0];
					//tooth.contiguous.length == 1  Eso es que es una esquina, por lo que tiene el data de la esquina
					if(tooth.contiguous.length == 1  ||
						!tooth.contiguous.every(function (tid) { return operation.teeth.indexOf(tid) >= 0 })) {
						$datadocument = operConfig.$svgdata2.clone();
				}
				else{
					$datadocument = operConfig.$svgdata.clone();
				}
		//		_checkClassSVG(operConfig.class,operation,$datadocument,odont);
		createNewGroup($node[0],operation.id+'_d'+tooth.id).append($datadocument[0]);
	}
	_adjustPosition(operation,odont,$node,operConfig);
}else{
	for (var i = operation.teeth.length - 1; i >= 0; i--) {
		$datadocument = operConfig.$svgdata.clone();
			//	_checkClassSVG(operConfig.class,operation,$datadocument,odont);

			createNewGroup($node[0],operation.id+'_d'+operation.teeth[i]).append($datadocument[0]);
		}
		_adjustPosition(operation,odont,$node,operConfig);
	}
}

}


var _checkOffsetConfig = function(operConfig,tooth){
	var offsetConfigx = 0;
	var offsetConfigy = 0;
	if (!!operConfig.offsetsvg) {

		if (tooth.position === 1 ||tooth.position === 2){
			offsetConfigx = !!operConfig.offsetsvg.xtemp ? operConfig.offsetsvg.xtemp : operConfig.offsetsvg.x;
			offsetConfigy = !!operConfig.offsetsvg.ytemp ? operConfig.offsetsvg.ytemp : operConfig.offsetsvg.y;
		}
		else{
			offsetConfigx=operConfig.offsetsvg.x;
			offsetConfigy=operConfig.offsetsvg.y;
		}
		if (operConfig.offsetsvg['x'+tooth.position] != null){
			offsetConfigx=operConfig.offsetsvg['x'+tooth.position];
		}
		if (operConfig.offsetsvg['y'+tooth.position] != null){
			offsetConfigy=operConfig.offsetsvg['y'+tooth.position];
		}
	}
	if (!offsetConfigx) offsetConfigx = 0;
	if (!offsetConfigy) offsetConfigy = 0;
	return  { "x": offsetConfigx, "y" : offsetConfigy};
}



var _adjustPosition = function(operation,odont,$node,operConfig){

	var x1=0;
	var x2=0;
	var y =0;
	var x = 0;
	var toothSelected = {};
	var teeth =  $.grep(odont.teeth , function(t) { return operation.teeth.indexOf(t.id) >=0 });

	var offsetConfig = _checkOffsetConfig(operConfig,teeth[0]);

	if (teeth.length == 2 && operConfig.tipo == odont.configuration.constants.tipotool.dosdientes){
		x1 = teeth[0].x;
		x2 = teeth[1].x;
		y = teeth[0].y;
		x = x1+(x2-x1)/2;
	}
	else if (teeth.length > 1){
		var xMax = {x:-1};
		var xMin = {x:10000};
		var mov = {x:0, y:0};
		for (var i = teeth.length - 1; i >= 0; i--) {
			if (operConfig.reversible) {
				if(teeth[i].position > 1){
					mov.x = -teeth[i].x;
					mov.y = -teeth[i].y;
				}else{
					mov.x = teeth[i].x;
					mov.y = teeth[i].y;
				}
			}else{
				mov.x = teeth[i].x;
				mov.y = teeth[i].y;
			}
			if(teeth[i].x > xMax.x) {
				xMax = teeth[i];
			}
			if(teeth[i].x < xMin.x) {
				xMin = teeth[i];
			}
			$('#'+operation.id + '_d'+teeth[i].id).attr('transform','translate('+mov.x +','+mov.y+') ');
		}
		if (!!operConfig.lastturned) 	{
			if (operConfig.reversible) {
				if(xMax.position > 1){
					$('#'+operation.id + '_d'+xMin.id).attr('transform', $('#'+operation.id + '_d'+xMin.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
				}
				else{
					$('#'+operation.id + '_d'+xMax.id).attr('transform', $('#'+operation.id + '_d'+xMax.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
				}
			}else{
				$('#'+operation.id + '_d'+xMax.id).attr('transform', $('#'+operation.id + '_d'+xMax.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
			}
		}
	}
	$node.attr('transform','');
	if (!!operConfig.reversible) {
		if(teeth[0].position > 1){
			if(operConfig.reversible == 1) {
				$node.attr('transform',' translate(50,130)  scale(-1,-1) ');
			}else{
				$node.attr('transform',' translate(0,130)  scale(1,-1) ');
			}
		}
	}

	$node.attr('transform','translate('+x+','+y+') '+  $node.attr('transform') + 'translate('+offsetConfig.x+','+ offsetConfig.y+')');
	if($node.hasClass('operation--rotated')){
		$node.attr('transform',   $node.attr('transform') + ' translate(50,0)  scale(-1,1)');
	}
}

var disableHistory = function(disableHover){
	$('.od-btn','.od-history').addClass ('od-btn--disabled');
	if(!!disableHover){
		$('.od-history','.od-list--selectable').addClass('disabled');
	}
}
var enableHistory = function(){
	$('.od-btn','.od-history').removeClass ('od-btn--disabled');
	$('.od-history','.od-list--selectable').removeClass('disabled');
}
var _writeHistoryOperation = function(operation,odont ,id,pathSVG){

	if($('#menu-ops').find('#hist_'+id).length==0){
		var linew = $('<li/>' ,{'class': 'od-list__item od-history', 'id': 'hist_'+id});
		var textOp ='';
		if (operation.teeth.length <3) {
			var strzone= '';
			if (!!operation.zone){ strzone =' (' + operation.zone + ')';}
			textOp = operation.teeth.toString().replace(/[,]/g,'-') + strzone;
		}else{
			var min = 1000;
			var max = -1;
			var position= $.grep(odont.configuration.teeth, function(t) { return t.id == operation.teeth[0]; })[0].position;
			for (var i = operation.teeth.length - 1; i >= 0; i--) {

				var comp = odont.teethByPositions[position].indexOf(operation.teeth[i]);
				if(comp != -1){
					if(min >comp){
						min = comp;
					}
					if (max < comp){
						max= comp;
					}
				}
			}
			textOp = '['+ odont.teethByPositions[position][min]+ '-' + odont.teethByPositions[position][max]+']';
		}
		var spanId = $('<span/>',{'class': 'od-history__id'}).html(textOp);
		var spanDesc = $('<span/>',{'class': 'od-history__desc'}).html(operation.text);
		var btn = $('<button/>',{'class': 'od-btn od-btn--primary od-btn--rounded od-btn--xs'}).attr('data-balloon','Eliminar operación').attr('data-balloon-pos','left');
		var img = $('<img/>',{'class': 'od-ico'}).attr('src',pathSVG + '/ui/delete.svg');
		linew.append(spanId);
		linew.append(spanDesc);
		btn.append(img);
		linew.append(btn);
		btn.bind('click', { odontogram : odont  },deleteOperationEvent);
		linew.hover( function(){
			odont.disableTeethExceptOperation(id);
		},function(){
			odont.enableTeeth();
		});
		$('#menu-ops').prepend(linew);
		if(odont.disabled){
			btn.addClass('od-btn--disabled');
		}
	}else{
		var $li = $('#menu-ops').find('#hist_'+id);
		$li.parent().prepend($li);
	}
}
var _checkSVGData = function(operation, odont,callback){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	callback = callback || function(){};
	$.ajax({
		type: 'GET',
		url: odont.configuration.constants.pathSVG + operConfig.svg,
		datatype: 'text'
	}).done(function(data){
		if (!!operConfig.svg2){
			$.ajax({
				type: 'GET',
				url: odont.configuration.constants.pathSVG + operConfig.svg2,
				datatype: 'text'
			}).done(function(data2){
				operConfig.$svgdata = $(data.documentElement);
				operConfig.$svgdata2 = $(data2.documentElement);
				callback(operation,odont);

			});
		}
		else{
			operConfig.$svgdata = $(data.documentElement);
			callback(operation,odont);
		}
	});
}

var _drawOperation = function(operation,odont){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	//SVG data contiene la cache de los svgs para no tener que consultarlos todo el rato
	if(!operConfig.$svgdata)
	{
		//Compruebo si tiene imagen
		if(!!operConfig.svg)
		{
			_checkSVGData(operation,odont,_drawOperation);
			//Si entra por aquí  tiene que acabar
			return;
		}
	}
	else{
		var $node= {};
		if (operConfig.tipo == odont.configuration.constants.tipotool.pieza){
			$node = $('#'+operation.id);
			if (!$node.length){
				var tooth = $.grep(odont.teeth, function(t) { return t.id == operation.teeth[0];})[0];
				$node = createNewGroup(tooth.$map[0],operation.id);
				$node.attr('class', 'operation');
			}
		}
		else{
			$node = $(odont.datadocument).find('#'+operation.id);
			if(!$node.length) {
				$node = createNewGroup(odont.datadocument,operation.id);
				$node.attr('class', 'operation');
			}

		}
		_drawSVGOperation(operation,odont,$node);
		operation.dom = $node;
	}
	if (operation.positions.length > 0){
		//if(operConfig.tipo == odont.configuration.constants.tipotool.detallelinea) {
			_drawLine(operation,odont);
		//}
	}
	else{
		if(!!operation.zone){
			_changeClassTooth(operation,odont,1);
		}
	}
	if (!!operation.box){
	//	if(!operation.class && !!operConfig.class){ operation.class = $.isArray(operConfig.class)? operConfig.class[0] : operConfig.class; }
		$.each(operation.teeth, function(index, val) {
			$.grep(odont.teeth, function(t) { return t.id == val })[0].box.push({
				'text' : operation.box,
				'class' : operConfig.txtclass,
				'id' : operation.id,
			}
			);
		});
		_writeBox(operation.teeth,odont);
	}
	_writeHistoryOperation(operation, odont, operation.id, odont.configuration.constants.pathSVG);
}
var _changeClassTooth = function(operation,odont,insert){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var tooth = $.grep(odont.teeth,function(e){return e.id ==  operation.teeth[0]})[0];
	if (insert){
		tooth.$datadocument.find('#'+operation.zone).addClass(operConfig.class);
		tooth.$datadocument.find('#'+operation.zone).children().addClass(operConfig.class);
		operation.class = operConfig.class;
	}else{
		tooth.$datadocument.find('#'+operation.zone).children().removeClass(operConfig.class);
		tooth.$datadocument.find('#'+operation.zone).removeClass(operConfig.class);
		operation.class = operConfig.class;
	}
}
var _drawLine = function(operation,odont){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var line = {};
	var nameOp = '';
	var tooth = $.grep(odont.teeth,function(e){return e.id ==  operation.teeth[0]})[0];
	var group = {};
	nameOp = !!operation.zone ?  _getOperationName(operation.teeth,operConfig.id,operation.zone) : _getOperationName(operation.teeth,operConfig.id);
	group = tooth.$datadocument.find('#'+nameOp);


	if(!group.length) {
		group =  !!operation.zone ?	createNewGroup(tooth.$datadocument.find('#'+operation.zone)[0],nameOp) : createNewGroup(tooth.$datadocument[0],nameOp) ;
		group.attr('class', 'operation');
	}
	for (var i = operation.positions.length - 1; i >= 0; i--) {
		var pos = operation.positions[i];
		if (!!pos.x1)
		{
			line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
			line.setAttributeNS(null, 'x1', pos.x1);
			line.setAttributeNS(null, 'y1', pos.y1);
			line.setAttributeNS(null, 'x2', pos.x2);
			line.setAttributeNS(null, 'y2', pos.y2);
			line.setAttributeNS(null, 'class', operConfig.class);
			operation.class = operConfig.class;
			group.append(line);
		}
	}
	operation.dom = group;
}

var deleteOperationEvent = function(event) {
	var odont = event.data.odontogram;
	var histOper = $(this).closest('.od-history');
	var id = histOper.attr('id').replace('hist_','');
	var oper = $.grep(odont.operations,function(e) { return e.id == id })[0];
	if(!!oper){
		deleteOperation(oper,odont);
		odont.enableTeeth();
	}
}
var deleteOperation = function(operation,odont,deleteBox){
	var histOper =  $('#menu-ops').find('#hist_'+operation.id);

	if (!!operation.box && !deleteBox){
		$.each(operation.teeth, function(index,val){
			var tooth = $.grep(odont.teeth, function(t) { return t.id == val})[0];
			tooth.box = tooth.box.filter(function( obj ) {
				return (!(obj.id == operation.id ));
			});
		});
		_writeBox(operation.teeth,odont);
	}
	if (!!deleteBox){
			$.each(operation.teeth, function(index,val){
			var tooth = $.grep(odont.teeth, function(t) { return t.id == val})[0];
			tooth.box = [];
		})
		_writeBox(operation.teeth,odont);
	}
	if (operation.dom.length > 0) {
		operation.dom.remove();
	}
	else{
		_changeClassTooth(operation, odont, 0);

	}

	odont.operations = $.grep(odont.operations, function(op) { return op.id !== operation.id});
	histOper.remove();
}

var _clearOperations = function(odont) {
	var oper = {};
	var histOper = {};
	for (var i = odont.operations.length - 1; i >= 0; i--) {
		oper = odont.operations[i];
		deleteOperation(oper,odont,1);
		/*histOper = $('#menu-ops').find('#hist_'+oper.id);
		if (oper.dom.length > 0) oper.dom.remove();
		histOper.remove();*/
	}
	odont.operations = [];
}




///Funciones para dibujar el tratamiento pulpar
function drawMiddleLane(triangulo,operation,operConfig) {
	var t = triangulo.find('polygon');
	var myPoints=t.attr('points').split(" ");

	var finalPoints=[];
	for (var i=0; i<myPoints.length; i++){
		if (myPoints[i].replace(/\s/g, '').length) {
			var arrayMyPoints =  myPoints[i].split(",");
			finalPoints[i] = [];
			finalPoints[i][0] = parseFloat(arrayMyPoints[0]);
			finalPoints[i][1] = parseFloat(arrayMyPoints[1]);
		}
	}
	var matrizSorted=_calculaVerticeDispar(finalPoints);
  //calculamos coordenadas finales
  var _x1, _y1, _x2, _y2;
  _x1=finalPoints[matrizSorted[0]][0];
  _y1=finalPoints[matrizSorted[0]][1];
  _x2=(finalPoints[matrizSorted[1]][0]+finalPoints[matrizSorted[2]][0])/2;
  _y2=(finalPoints[matrizSorted[1]][1]+finalPoints[matrizSorted[2]][1])/2;
  //dibujamos la linea
  var line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
  line.setAttributeNS(null, 'x1', _x1);
  line.setAttributeNS(null, 'y1', _y1);
  line.setAttributeNS(null, 'x2', _x2);
  line.setAttributeNS(null, 'y2', _y2);
  //line.setAttributeNS(null, 'stroke', "red");
  line.setAttributeNS(null, 'class', operConfig.class);
  operation.positions = [{
  	x1: _x1,
  	y1: _y1,
  	x2: _x2,
  	y2: _y2
  }];
  triangulo.append(line);
  operation.dom = line;
}
function _calculaVerticeDispar(matriz){
//metemos los y en un array
pointsY=[];
for (var i=0; i<matriz.length; i++){
	pointsY[i]=matriz[i][1];
}
  //calculamos los puntos de los bordes
  maxIndex=_indexOfMax(pointsY);
  minIndex=_indexOfMin(pointsY);

  //y descartamos el que no es
  var discarded, dispar, otro;
  for (var i=0; i<matriz.length; i++){
  	if ((i!=maxIndex) && (i!=minIndex) ){
  		discarded=i;
  		break;
  	}
  }


  //calculamos las diferencias entre el descartado y los otros dos y el más separado es el dispar
  var dist1=Math.abs(pointsY[discarded]-pointsY[maxIndex]);
  var dist2=Math.abs(pointsY[discarded]-pointsY[minIndex]);
  if (dist1>dist2) {
  	dispar=maxIndex;
  	otro=minIndex;
  } else {
  	dispar=minIndex;
  	otro=maxIndex;
  }


  var matrizSorted=[]
  matrizSorted[0]=dispar;
  matrizSorted[1]=discarded;
  matrizSorted[2]=otro;
  return matrizSorted;


}

function _indexOfMax(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var max = arr[0];
	var maxIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] > max) {
			maxIndex = i;
			max = arr[i];
		}
	}

	return maxIndex;
}
function _indexOfMin(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var min = arr[0];
	var minIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] < min) {
			minIndex = i;
			min = arr[i];
		}
	}

	return minIndex;
}
var _writeBox = function(teeth,odont){
	var text=null;
	for (var i = 0; i < teeth.length; i++) {
		var tooth = $.grep(odont.teeth, function(t) { return t.id == teeth[i] })[0];
		iniX=2;
		incY=20;
		incX=5;
		posX=iniX;
		posY=0;
		maxX=48;
		maxY=50;
		fontSize = 22;
		 text = tooth.$recuadrotooth.find('text');
		if(tooth.box.length > 0){
			text.attr('style','font-size:'+fontSize+'px');
			_continueWriteBox(tooth.box, text)
		}else{
				text.children().remove();
		}
	}

}
var iniX=2;
var incY=20;
var incX=5;
var posX=iniX;
var posY=0;
var maxX=48;
var maxY=50;
var fontSize = 22;

var _continueWriteBox = function(opers,texto){
	texto.children().remove();
	for (var i = 0; i < opers.length ; i++) {
		var tspan =  document.createElementNS('http://www.w3.org/2000/svg','tspan');
		tspan.textContent = opers[i].text;
		if(!!opers[i].class){ tspan.setAttribute('class',opers[i].class); }
		tspan.setAttribute('dx',incX + 'px');

		texto.append( tspan);

		var widthTspan = texto[0].getBBox().width;

		if(texto.children().length > 1){
			if (widthTspan  > maxX){
				posX=iniX;
				tspan.setAttribute('dx','0px');
				tspan.setAttribute('x',posX + 'px');

				posY=incY;
			} else {
				posY=0;
			}
		}else{
			tspan.setAttribute('dx', iniX+ 'px');
       //  tspan.setAttribute('y',(incY)+'px');
       posY = incY;

   }
   tspan.setAttribute('dy',posY +'px');


}

//tspan.getBBox().height
if(texto[0].getBBox().height > maxY){
	fontSize= fontSize-0.5;
	incY = fontSize *1.1;
	posY=0;

	texto.attr('style','font-size:'+fontSize+'px');
	_continueWriteBox(opers,texto);
	return;
}
}
"use strict";

var _toolboxGenerate = function(odontogram,elementId){
  //var divulNew = $('<div/>',{'class': 'od-toolbar-div '});
  var ulNew = $('<ul/>' ,{'class': 'od-toolbar'});
  var _elementId = elementId;
  var items = [];
  var ulTools = {};
  var liContainer = {};
//divulNew.append(ulNew);
  ulNew.append($('<li/>', {
   'class': 'od-toolbar__title',
   html: 'Operaciones'
 }));

  ulNew.append(
    $('          <li class="od-toolbar__tool--selected">' +
      '            <div class="od-media od-media--top ">' +
      '              <div class="od-media__image">' +
      '                <img >' +
      '              </div>' +
      '              <div class="od-media__body  ">' +
      '                <h5 class="od-media__title" ></h4>' +
      '' +
      '<div class="od-media__options od-media__options--left">' +
      '</div>' +
      '                </div>' +
      '              </div>' +
      '            </li>'));

//despu�s de a�adir la lista de seleccionadas hacemos un li que va a contener todas las tools
 liContainer = $('<li/>' ,{'class': 'od-container'});
 ulNew.append(liContainer);

  ulTools = $('<ul/>' ,{'class': 'od-tools'});

  $.each( odontogram.configuration.tools, function( key, val ) {
    val.ico = odontogram.configuration.constants.pathSVG + val.ico;
    var pos = 'up';
    if(key % 4 == 0){
      pos = 'right';
    }
    else if(key % 4 == 3){
      pos = 'left';
    }
    else if(key <4){
       pos = 'down';
    }
    if (!!val.default && !val.hidden )
    {
      odontogram.toolSelected = val;
      ulTools.append(
       $('<li data-balloon="' + val.desc + '" data-balloon-pos="'+pos+'"><button class="od-toolbar__btn od-btn--active" data-develop="'+ !!val.develop +'" data-zoom="'+ !!val.zoom +'" data-idtool="'+val.id+'"><img class="od-btn__ico" src="' + val.ico + '">' +
        '<span class="od-btn__text">' + val.text + '</span>' +
        '</button></li>')
       );
    }
    else if(!val.hidden){
      ulTools.append(
       $('<li data-balloon="' + val.desc + '" data-balloon-pos="'+pos+'"><button class="od-toolbar__btn"  data-develop="'+ !!val.develop +'"  data-zoom="'+ !!val.zoom +'" data-idtool="'+val.id+'"><img class="od-btn__ico" src="' + val.ico + '" >' +
        '<span class="od-btn__text">' + val.text + '</span>' +
        '</button></li>')
       );
    }
  });

  liContainer.append(ulTools);
  ulNew.append(liContainer);
  ulNew.appendTo('#' + elementId);
  odontogram.toolSelectDefault();

  $('.od-toolbar__btn').click(function(event) {
   var $this = $(this);
   if ($this.hasClass('od-btn--active')){
    odontogram.toolSelectDefault();
  }
  else	{
    $('.od-btn--active').removeClass('od-btn--active');
    $this.addClass('od-btn--active');
    odontogram.toolSelected = $.grep(odontogram.configuration.tools, function(obj) {
      return obj.id == $this.attr('data-idtool');
    })[0];
    if(!!odontogram.toolSelected.box) {
      odontogram.toolSelected.boxSelected= odontogram.toolSelected.box[0];
    }else{
      odontogram.toolSelected.boxSelected= null;
    }
  }
}
);
}

var disableTools = function(){
 $('.od-toolbar__btn').addClass ('od-btn--disabled');
 $('textarea','.panel__body').attr('disabled','disabled');
}

var disableZoomTools = function(){
  $('.od-toolbar__btn[data-zoom!=true]').addClass('od-btn--disabled');
}

var enableTools = function(){
  $('.od-toolbar__btn').removeClass ('od-btn--disabled');
 $('textarea','.panel__body').removeAttr('disabled');
}

var printCssData = undefined;
var _loadPrintCss = function(printcss){
  $.ajax({
    type: 'GET',
    url: printcss,
    datatype: 'text',
  }).done(function(data) {
    printCssData = data;
  });
}
var _generateImg = function(){
  if (!!printCssData){
    var svg  = $(document.querySelector( "svg" )).clone().removeAttr('width').removeAttr('height')[0];
    var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rect.setAttribute('fill', 'white');
    rect.setAttribute('width', '100%');
    rect.setAttribute('height', '100%');
    svg.insertBefore(rect,svg.firstChild);

    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = printCssData;
    svg.insertBefore(style,svg.firstChild);

    var svgString = new XMLSerializer().serializeToString(svg);
    var data = "data:image/svg+xml;base64," + btoa(svgString);
    return  data;
  }
}









"use strict";
function Tooth(obj) {
	var objProp, i =0;
	this.id = 0;
	this.position = -1;
	this.map = '';
	this.svg = '';
	this.text = '0';
	this.x = 0;
	this.y = 0;
	this.$map = {};
	this.odontogram = {};
	this.$recuadrotooth = {};
	this.$datadocument = '';
	this.dragging = false;
	this.ox = 0;
	this.oy = 0;
	this.circle = {};
	this.line = {};
	this.tempOperations = [];
	this.box = [];
	objProp = Object.keys(obj);
	for (i = objProp.length - 1; i >= 0; i--) {
		this[objProp[i]] = obj[objProp[i]];
	}
}

Tooth.prototype.callSVG = function(odontog,callback,failure) {
	var _this = this,
	callback = callback || function(){},
	failure = failure || function(){};
	_this.odontogram = odontog;
	$.ajax({
		type: 'GET',
		url: _this.odontogram.configuration.constants.pathSVG + _this.svg,
		datatype: 'HTML'
	}).done(function(data,status,response){
		_this.drawSVG(data);
		if (!!_this.map) {
			_this.drawRectTooth();
			_this.drawNumber();
		}


		callback();
	}).fail(function(error){
		failure(error);
	});
}


Tooth.prototype.drawSVG =  function(data) {
	var _this = this;
	var bbox = {};
	var $circChild = {};
	_this.$datadocument = $(data.documentElement).addClass('tooth-trigger').attr('data-idtooth', _this.id);
	_this.$map = $('#'+_this.map);
	//_this.$map.addClass('tooth-trigger');
	_this.$datadocument.addClass('minitooth');
   	//Cambiar el id del tooth
   	_this.$datadocument.find('#diente-total').attr('id','dd'+_this.id).addClass('tooth');
    //Cambiar el número del text
    _this.$datadocument.find('#txtnumero').text(_this.text).addClass('tooth__txt');
    //Cambiar la posición del tooth y ajustarla al centro del circulo de la base.
    $circChild = _this.$map.children();
    if (!!_this.$map[0]) {
    	_this.x = Math.round($circChild.attr('cx'));

    	_this.y = Math.round($circChild.attr('cy'));
    	_this.$map.attr('transform','translate('+_this.x+','+_this.y+')');

    	_this.$map.html(data.documentElement);
	    //El positionamiento hay que hacerlo después de insertar el html
	    //ya que no se sabe lo que ocupa en pantalla previamente
	    _this.x = Math.round(_this.x-_this.$map[0].childNodes[0].width.baseVal.value/2);
	    _this.y = Math.round(_this.y-_this.$map[0].childNodes[0].height.baseVal.value/2);
	    _this.$map.attr('transform','translate('+_this.x+','+_this.y+')');
	    _this.$map.attr('data-idtooth', _this.id);
	}
	else{

		_this.map = '';
	}
}
Tooth.prototype.drawRectTooth = function() {
	var _this = this;
	var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
	var y = 0;
	var $gR  = null;
	var  txt = null;
	//rect.setAttributeNS(null, 'x', _this.x);
	rect.setAttributeNS(null, 'height', '50');
	rect.setAttributeNS(null, 'width', _this.$datadocument.attr('width'));
	rect.setAttributeNS(null, 'fill', 'transparent');
	rect.setAttributeNS(null, 'stroke', '#134876');
	rect.setAttributeNS(null, 'id', 'rd'+_this.id);
	rect.setAttributeNS(null, 'data-idtooth', _this.id);
	rect.setAttributeNS(null, 'class', 'tooth-rect tooth-trigger');

	if (_this.position === _this.odontogram.configuration.constants.positiondent.top.final){
		y = _this.odontogram.configuration.constants.yrect.finalTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.top.temp){
		y = _this.odontogram.configuration.constants.yrect.tempsTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.final){
		y = _this.odontogram.configuration.constants.yrect.finalBot;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.temp){
		y = _this.odontogram.configuration.constants.yrect.tempsBot;
	}
	//rect.setAttributeNS(null, 'y', y);
	if(!$(_this.odontogram.datadocument).find('#rects').length) {
		createNewGroup(_this.odontogram.datadocument,'rects');
	}
	$gR = createNewGroup($(_this.odontogram.datadocument).find('#rects')[0],'grd'+_this.id);
	$gR.attr('transform','translate('+ _this.x+','+y+')');
	$gR[0].appendChild(rect);
	_this.$recuadrotooth = $gR;
	txt = document.createElementNS('http://www.w3.org/2000/svg', 'text');
	$gR[0].appendChild(txt);

}

Tooth.prototype.drawNumber = function() {
	var _this = this;
	var txtNumber = document.createElementNS('http://www.w3.org/2000/svg', 'text');
	var data = document.createTextNode(_this.text);
	var y = 0;
	txtNumber.setAttributeNS(null, 'x', (_this.x + _this.$map[0].getBBox().width/2));
	txtNumber.setAttributeNS(null, 'fill', 'red');
	txtNumber.setAttributeNS(null, 'stroke', '#134876');
	txtNumber.setAttributeNS(null, 'id', 'nd'+_this.id);
	txtNumber.setAttributeNS(null, 'data-idtooth', _this.id);
	txtNumber.setAttributeNS(null, 'class', 'tooth-text tooth-trigger');
	txtNumber.setAttributeNS(null, 'text-anchor', 'middle');
	//txtNumber.innerHTML = _this.text;
	txtNumber.appendChild(data);
	if (_this.position === _this.odontogram.configuration.constants.positiondent.top.final){
		y = _this.odontogram.configuration.constants.ynumber.finalTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.top.temp){
		y = _this.odontogram.configuration.constants.ynumber.tempsTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.final){
		y = _this.odontogram.configuration.constants.ynumber.finalBot;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.temp){
		y = _this.odontogram.configuration.constants.ynumber.tempsBot;
	}
	txtNumber.setAttributeNS(null, 'y', y);
	if(!$(_this.odontogram.datadocument).find('#numbers').length) {
		createNewGroup(_this.odontogram.datadocument,'numbers');
	}
	$(_this.odontogram.datadocument).find('#numbers')[0].appendChild(txtNumber);
	//_this.$recuadrotooth = $('#'+txtNumber.id,_this.odontogram.$mainView);
}

var openWindow = function(tooth){
	var _this = tooth;
	var toothPopup = _this.$datadocument.clone().attr('viewbox','0 0 50 130').removeClass('minitooth').addClass('maxitooth').removeAttr('width').attr('height',_this.odontogram.$mainView.height()-250);
	var boxT ='';
	$.each(tooth.box, function(pos,box) { boxT = boxT + box.text + ' ' });
	var srcOperations =$.map(tooth.odontogram.operations, function(op) { if(op.teeth.indexOf(tooth.id) >= 0) { return op.idtypeoperation } });
	var srcs = [];
	for (var i = srcOperations.length - 1; i >= 0; i--) {
		$.each(tooth.odontogram.configuration.tools, function(pos,tool) {
			if (!tool.zoom){
				if (tool.id == srcOperations[i]) {
					if(srcs.indexOf(tool.ico) == -1) {
						srcs.push(tool.ico);
					}}
				}});
	}


	var icons=$('<div class="tooth__icons">' +

		'</div>'
		);
	for (var i = srcs.length - 1; i >= 0; i--) {
		$('<img class="tooth__icon" src="'+ srcs[i] +'">').appendTo(icons);
	}
	var p = $('<p class="pBox">' +boxT+ '</p>');
	_this.odontogram.$divToothDrag.html();
	if (srcs.length > 0){	_this.odontogram.$divToothDrag.append(icons); }
	_this.odontogram.$divToothDrag.append(p);
	_this.odontogram.$divToothDrag.append(toothPopup);
	_this.tempOperations= [];

	toothPopup.on('mousedown.toothpop',{ tooth: _this, toothPopup: toothPopup } , _mousedown);
	_this.odontogram.divToothDragSave = function() { _this.toothCopy(toothPopup); }
	_this.odontogram.$divToothDrag.dialog(
	{

		'title': function() { $(this).html( 'Detalle Pieza: <strong>'+_this.text+'</strong>')} ,
		// 'class':'pepito2'
	}
	).data('odontogram',_this.odontogram).dialog('open');
}

Tooth.prototype.click =  function(event){
	var _this = this;
	if(!!_this.odontogram.toolSelected.zoom){
		openWindow(_this);
	}
	else {
		_this.addOperation(_this.odontogram.toolSelected);
	}
}

Tooth.prototype.cancelActive  = function(){
	var tooth = this;
	tooth.odontogram.enableTeeth();
	tooth.odontogram.deactivateTeeth();
	enableHistory();
}



Tooth.prototype.toothCopy =  function(toothPopup){
	var _this = this;
	for (var i = _this.tempOperations.length - 1; i >= 0; i--) {
		var previousOp = $.grep(_this.odontogram.operations, function (op){ return op.id == _this.tempOperations[i].id} );
		if (previousOp.length > 0){
			if(!!previousOp[0].positions){
				_this.tempOperations[i].positions = _this.tempOperations[i].positions.concat(previousOp[0].positions);
			}
			deleteOperation(previousOp[0],_this.odontogram);
		}
		if(!_this.tempOperations[i].deleted){
			_drawOperation(_this.tempOperations[i], _this.odontogram);
			_this.odontogram.operations.push(_this.tempOperations[i]);
		}
	}
	_this.tempOperations = [];
	/*
	var opn = toothPopup.find('#operations_new');
	if (opn.length){
		_this.$datadocument.html(toothPopup.children());
	}
	*/
}

var _checkClassOperation = function(oper,toolOperation,odont){
	if($.isArray(toolOperation.class) > 0){
		oper.status++;
		oper.dateop = new Date();
		if ( oper.status >= toolOperation.class.length )	{
			deleteOperation(oper,odont);
		}else{
			oper.dom.removeClass(toolOperation.class[oper.status-1]).addClass(toolOperation.class[oper.status]);
			if (toolOperation.class[oper.status] == 'operation--rotated'){
				oper.dom.attr('transform',   oper.dom.attr('transform') + ' translate(50,0)  scale(-1,1)');
			}
		}
	}
	else{
		deleteOperation(oper,odont);
	}
}
Tooth.prototype.addOperation =  function(toolOperation,callback,failure){
	var _this = this;
	var opn = _this.$datadocument.find('#operations_new');
	var newOperation = {};
	var opera = {},
	_class = '',
	_pos = -1,
	gop = {},
	$svg = {},
	callback = callback || function(){},
	failure = failure || function(){};



	if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.pieza){

		opera =_this.odontogram.findOperationsByTeeth([_this.id],_this.odontogram.toolSelected.id);
		if (opera.length > 0){
			newOperation = opera[0];
			_checkClassOperation(newOperation,toolOperation,_this.odontogram);
			/*if($.isArray(toolOperation.class) > 0){
				newOperation.status++;
				newOperation.dateop = new Date();
				if ( newOperation.status >= toolOperation.class.length )	{
					dibujarOperacion= false;
				}
			}
			else{
				dibujarOperacion= false;
			}*/
		}
		else{
			newOperation = new Operation()
			newOperation.id = _getOperationName(_this.id,toolOperation.id);
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.teeth = [];
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.teeth.push(_this.id);
			_this.odontogram.operations.push(newOperation);
			_drawOperation(newOperation,_this.odontogram);

		}

	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.todafila){
		opera =_this.odontogram.findOperationsByTeeth([_this.id],_this.odontogram.toolSelected.id);
		if (opera.length > 0 ){
			newOperation = opera[0];
			_checkClassOperation(newOperation,toolOperation,_this.odontogram);
		}
		else{
			newOperation = new Operation()
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.teeth = _this.odontogram.getAllContiguousTeeth(_this.id);
			_this.odontogram.operations.push(newOperation);
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.id = _getOperationName(newOperation.teeth,toolOperation.id);
			_drawOperation(newOperation,_this.odontogram);

		}

	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes ||
		toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.desdedienteadiente){


		if(_this.odontogram.toothSelected.length === 0){
			//Primer diente
			var teethDisable = [];
			if (toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes){
				teethDisable = _this.contiguous;
			}else{
				teethDisable = _this.odontogram.getAllContiguousTeeth(_this.id);
				teethDisable.splice(teethDisable.indexOf(_this.id),1);
			}
			_this.odontogram.disableTeeth(teethDisable);
			_this.$datadocument.removeClass('minitooth--disabled').addClass('minitooth--active');
			_this.$recuadrotooth.removeClass('tooth-rect--disabled').addClass('tooth-rect--active');
			_this.odontogram.toothSelected.push(_this);
			disableHistory(true);
		}
		else{
			//Segundo diente
			//Si es el mismo que el primero, cancelo operación
			if (_this.odontogram.toothSelected[0].id === _this.id)
			{
				_this.cancelActive();
			}
			else{
				//_this.odontogram.toothSelected.push(_this);
				//_this.odontogram.toothSelected = _this.odontogram.toothSelected.sort(function(a, b) {return a.id > b.id});
				var operFind = _this.odontogram.findOperationsByTeeth([_this.odontogram.toothSelected[0].id,_this.id],toolOperation.id);
				if (operFind.length > 0 ) {
					newOperation = operFind[0];
					_checkClassOperation(newOperation,toolOperation,_this.odontogram);
					//deleteOperation(operFind[0],_this.odontogram);
				}else{

					newOperation = new Operation()
					newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
					newOperation.text = _this.odontogram.toolSelected.text;
					newOperation.teeth = [];
					if (toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes){
						newOperation.teeth.push(_this.id);
						newOperation.teeth.push(_this.odontogram.toothSelected[0].id);
					}else{
						newOperation.teeth = _this.odontogram.getContiguousBetween2Tooth(_this.id, _this.odontogram.toothSelected[0].id);
					}
					if(!!_this.odontogram.toolSelected.boxSelected){
						newOperation.box  = _this.odontogram.toolSelected.boxSelected;
					}
					newOperation.teeth.sort();
					newOperation.id = _getOperationName(newOperation.teeth,toolOperation.id);
					_this.odontogram.operations.push(newOperation);
					_drawOperation(newOperation,_this.odontogram);

				}

				_this.cancelActive();
			}
		}
	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.detallelinea)	{

		opera = $.grep(_this.tempOperations,function(e) {
			return e.idtypeoperation == toolOperation.id && e.teeth.indexOf(_this.id) >= 0
		});

		//opera =_this.odontogram.findOperationsByTeeth([_this.id],toolOperation.id);
		if(opera.length === 0) {

			newOperation = new Operation();
			newOperation.id = _getOperationName(_this.id,_this.odontogram.toolSelected.id);
			newOperation.dom = _this.line.toothPopup.find('#'+newOperation.id )[0];
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.positions = [{
				x1: _this.line.x1.baseVal.value,
				y1: _this.line.y1.baseVal.value,
				x2: _this.line.x2.baseVal.value,
				y2: _this.line.y2.baseVal.value
			}];
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.teeth = [];
			newOperation.teeth.push(_this.id);
			newOperation.dateop = new Date();
			_this.tempOperations.push(newOperation);

		}
		else{
			newOperation = opera[0];
			newOperation.positions.push({
				x1: _this.line.x1.baseVal.value,
				y1: _this.line.y1.baseVal.value,
				x2: _this.line.x2.baseVal.value,
				y2: _this.line.y2.baseVal.value
			});
		}
		//_writeHistoryOperation(newOperation,_this.odontogram, newOperation.id 	,_this.odontogram.configuration.constants.pathSVG);
	}

}


function _mousedown(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var _x = 0;
	var _y = 0;
	var dim = {};

	if(event.which === 1 && !_tooth.dragging){
		if (_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallecirculo){
			//Dibujar circulo caries/empastes...

			_tooth.ox = event.screenX;
			_tooth.oy = event.screenY;
			_tooth.circle =  document.createElementNS('http://www.w3.org/2000/svg', 'circle');
			dim = toothPopup[0].getBoundingClientRect();
			if(!toothPopup.find('#operations_new').length) {
				createNewGroup(toothPopup[0],'operations_new');
			}
        	//Coordenadas relativas al contenedor
        	_x = event.clientX - dim.left;
        	_y = event.clientY - dim.top;
			//Coordenadas relativas al viewbox
			_x = _x*toothPopup[0].viewBox.baseVal.width/dim.width;
			_y = _y*toothPopup[0].viewBox.baseVal.height/dim.height;

			_tooth.circle.setAttributeNS(null, 'cx', _x);
			_tooth.circle.setAttributeNS(null, 'cy', _y);
			_tooth.circle.setAttributeNS(null, 'fill', _tooth.odontogram.toolSelected.color);
			if (!!_tooth.odontogram.toolSelected.class) _tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
			_tooth.circle.setAttributeNS(null, 'id', 'cd'+_tooth.id);
			_tooth.circle.setAttributeNS(null, 'data-idtooth', _tooth.id);
			_tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class+' operation--drawing');
			_tooth.circle.setAttributeNS(null, 'r', '2');
			toothPopup.find('#operations_new')[0].appendChild(_tooth.circle);
			_tooth.dragging = true;
			$('body').one('mouseup.toothpop', { tooth: _tooth, toothPopup: toothPopup } , _mouseupCircle);
			toothPopup.on('mousemove.toothpop', { tooth: _tooth, toothPopup: toothPopup }, _mousemoveCircle);
		}
		else if (_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallelinea){
			//Dibujar linea para fractura
			if(event.which === 1 && !_tooth.dragging){
				_tooth.ox = event.screenX;
				_tooth.oy = event.screenY;
				_tooth.line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
				dim = toothPopup[0].getBoundingClientRect();
				if(!toothPopup.find('#operations_new').length) {
					createNewGroup(toothPopup[0],'operations_new');
				}
				if(!toothPopup.find('#'+_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id)).length) {
					createNewGroup(toothPopup.find('#operations_new')[0],_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id));
				}
			 	//Coordenadas relativas al contenedor
			 	var _x1 = event.clientX - dim.left;
			 	var _y1 = event.clientY - dim.top;
				//Coordenadas relativas al viewbox
				var _x1 = _x1*toothPopup[0].viewBox.baseVal.width/dim.width;
				var _y1 = _y1*toothPopup[0].viewBox.baseVal.height/dim.height;

				var  _x2 = _x1;

				var  _y2 = _y1;

				_tooth.line.setAttributeNS(null, 'x1', _x1);
				_tooth.line.setAttributeNS(null, 'y1', _y1);
				_tooth.line.setAttributeNS(null, 'x2', _x2);
				_tooth.line.setAttributeNS(null, 'y2', _y2);
				_tooth.line.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class + ' operation--drawing');
				toothPopup.find('#'+_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id))[0].appendChild(_tooth.line);
				_tooth.line.toothPopup = toothPopup;
				_tooth.dragging = true;
				$('body').one('mouseup.toothpop', { tooth: _tooth, toothPopup: toothPopup } , _mouseupLine);
				toothPopup.on('mousemove.toothpop', { tooth: _tooth, toothPopup: toothPopup }, _mousemoveLine);
			}
		}
		else if(_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallezona){
			var operConfig = $.grep(_tooth.odontogram.configuration.tools,function(e) { return e.id == _tooth.odontogram.toolSelected.id })[0];
			var zoneClick = $.inArray(event.target.id, operConfig.zones);
			if (zoneClick == -1){
				zoneClick = $.inArray(event.target.parentNode.id, operConfig.zones);
				if(zoneClick> -1){
					$nodeClicked = $('#'+event.target.parentNode.id,toothPopup);
				}
			}
			else{
				$nodeClicked = $('#'+event.target.id,toothPopup);
			}
			if (zoneClick > -1){
				var zone = operConfig.zones[zoneClick];
				opera = $.grep(_tooth.tempOperations,function(e) {
					return e.idtypeoperation == _tooth.odontogram.toolSelected.id && e.teeth.indexOf(_tooth.id) >= 0 && e.zone == zone
				});

				//opera =_tooth.odontogram.findOperationsByTeeth([_tooth.id],toolOperation.id);
				if(opera.length === 0) {
					var opName = _getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id,zone);;
					opera = $.grep(_tooth.odontogram.operations,function(e) {
						return e.id == opName;
					});
					newOperation = new Operation();
					newOperation.id = opName;
					newOperation.idtypeoperation = _tooth.odontogram.toolSelected.id;
					newOperation.text = _tooth.odontogram.toolSelected.text;
					newOperation.zone = zone;
					newOperation.teeth = [];

					newOperation.teeth.push(_tooth.id);
					newOperation.dateop = new Date();
					_tooth.tempOperations.push(newOperation);
					if(opera.length===0){
						if(!!_tooth.odontogram.toolSelected.boxSelected){
							newOperation.box  = _tooth.odontogram.toolSelected.boxSelected;
							$('.pBox').text($('.pBox').text() +  newOperation.box + ' ');
						}
						if(!!operConfig.fn){
							window[operConfig.fn]($nodeClicked,newOperation,operConfig);
						}
						else{
							$nodeClicked .addClass(_tooth.odontogram.toolSelected.class);
							$nodeClicked.children().addClass(_tooth.odontogram.toolSelected.class);
							newOperation.deleted = 0;
						}
					}else{
						if (!!opera[0].box) {
							$('.pBox').text($('.pBox').text().replace(opera[0].box+ ' ',''));
						}
						$nodeClicked.find('#'+newOperation.id).remove();
						$nodeClicked.removeClass(_tooth.odontogram.toolSelected.class);
						$nodeClicked.children().removeClass(_tooth.odontogram.toolSelected.class);
						newOperation.deleted = 1;
						//if(!!newOperation.dom) { newOperation.dom.remove(); }
					}
				}
				else{
					newOperation = opera[0];
					if(!newOperation.deleted){
						if (!!newOperation.box) {
							$('.pBox').text($('.pBox').text().replace(newOperation.box+ ' ',''));
						}
						$nodeClicked.removeClass(_tooth.odontogram.toolSelected.class);
						$nodeClicked.children().removeClass(_tooth.odontogram.toolSelected.class);
						newOperation.deleted = 1;
					}else{
						if(!!_tooth.odontogram.toolSelected.boxSelected){
							newOperation.box  = _tooth.odontogram.toolSelected.boxSelected;
							$('.pBox').text($('.pBox').text() +  newOperation.box + ' ');
						}
						if(!!operConfig.fn){
							window[operConfig.fn]($nodeClicked,newOperation,operConfig);
						}else{
							$nodeClicked .addClass(_tooth.odontogram.toolSelected.class);
							$nodeClicked.children().addClass(_tooth.odontogram.toolSelected.class);

						}
						newOperation.dateop = new Date();
						newOperation.deleted = 0;
					}
				}
			}

		}
	}
}
function _getOperationName(toothId,toolId,zone){
	var strzone = '';
	if (!!zone) { strzone = '_z' +zone;}
	return 'op_t' + toothId.toString().replace(/[,]/g,'_') + '_o' + toolId + strzone;
}
function _mousemoveCircle(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var catetoX = 0,
	catetoY = 0,
	r = 0;
	if (_tooth.dragging) {
		catetoX = Math.abs(event.screenX-  _tooth.ox)*toothPopup[0].viewBox.baseVal.width/toothPopup[0].getBoundingClientRect().width;

		catetoY = Math.abs(event.screenY-  _tooth.oy)*toothPopup[0].viewBox.baseVal.height/toothPopup[0].getBoundingClientRect().height;
		r = Math.sqrt(Math.pow(catetoX,2)+ Math.pow(catetoY,2));
		if (r>= 2){
			_tooth.circle.setAttributeNS(null,'r', r);
		}
	}
}

function _mouseupCircle(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	if (_tooth.dragging){
		_tooth.dragging = false;
		_tooth.circle.setAttributeNS(null, 'fill', _tooth.odontogram.toolSelected.color);
		_tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
		toothPopup.off('mousemove.toothpop');
	}
}

function _mousemoveLine(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var _x2 = 0,
	_y2 = 0,
	dim = {};

	if (_tooth.dragging) {
		dim = toothPopup[0].getBoundingClientRect();
		_x2 = (event.clientX - dim.left)*toothPopup[0].viewBox.baseVal.width/dim.width;
		_y2 = (event.clientY - dim.top)*toothPopup[0].viewBox.baseVal.height/dim.height;

		_tooth.line.setAttributeNS(null,'x2', _x2);
		_tooth.line.setAttributeNS(null,'y2', _y2);
	}
}

function _mouseupLine(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var result = {};

	if (_tooth.dragging){
		_tooth.dragging = false;
		_tooth.line.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
		toothPopup.off('mousemove.toothpop');
		_tooth.addOperation(_tooth.odontogram.toolSelected);

	}
}

function createNewGroup(svg,id){
	var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
	g.setAttributeNS(null, 'id', id);
	svg.appendChild(g);
	return $(g);
}



