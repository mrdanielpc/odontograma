"use strict";
function Odontogram(yFinalTop,yTempsTop,yFinalBot,yTempsBot) {
	this.idodontogram = null;
	this.teeth = [];
	this.mainView = '';
	this.$mainView = {};
	this.datadocument = {};
	this.configuration = {};
	this.configurationDefault = {};
	this._toolSelected = { };
	this.operations = [];
	this.dateod = {};
	this.disabled = 0;
	this.$divToothDrag = {};
	this.$divHistory = {};
	this.$divMbox = {};
	this.ready = 0;
	var _odont = this;
	Object.defineProperty(this,'toolSelected',{
		get: function() { return this._toolSelected; },
		set: function(newValue) {
			var _this = this;
			var $divboxes = {};
			_this._toolSelected = newValue;
			if(_this.toothSelected.length >0){
				$.grep(_this.teeth, function(el) {
					//if ($.inArray(el, _this.toothSelected) >= 0) el.cancelActive();
					if (_this.toothSelected.indexOf(el) > -1){
						el.cancelActive();
					}
				});
			}
			$('.od-media__title','.od-toolbar__tool--selected').text(newValue.text);
			$('.od-media__image','.od-toolbar__tool--selected').find('img').attr('src', newValue.ico);
			$divboxes = $('.od-media__options', '.od-toolbar__tool--selected');
			$divboxes.html('');
			if (!!newValue.box){
				if(newValue.box.length> 1){
					for (var i = 0; i < newValue.box.length; i++) {


						$divboxes.append($('<div class="od-formgroup">' +
							'     <input class="od-radio" id="'+newValue.box[i]+'" name="tool" type="radio"  class="od-radio" value="'+newValue.box[i]+'">' +
							'     <label class="od-radio__label" for="'+newValue.box[i]+'">'+newValue.box[i]+'</label>' +
							'</div>'));
					}
					$('input[name=tool]:radio').change(function () {
						_this.toolSelected.boxSelected = this.id;
					});
					$('input[name=tool]:radio:first').prop( 'checked', true );

				}
				else{
					_this.toolSelected.boxSelected = newValue.box[0];
				}
			}
		}
	});
	this.toothSelected = [];
	this.configModalHist = {
		autoOpen:false,
		width:'auto',
		close: function(event) {
			$(this).html('');
			$(this).closest("div.ui-dialog").removeClass('pop-history--mousedown')
			if(!_odont.disabled) {
				enableTools();
				enableHistory();
			}
		},
		open: function(){
			disableTools();
			disableHistory();
		},
		modal:true,
	};
	this.divToothDragSave = function() {};
	this.configModalTooth = {
		width: 'auto',
		overflow: 'hidden',
		autoOpen: false,
		modal:true,
		buttons: {
			'Guardar': function(tooth) {
				$(this).data('odontogram').divToothDragSave();
				$(this).dialog( 'close' );
			},
			Cancelar: function() {
				$(this).dialog( 'close' );
			}
		},
		close: function(event) {
			$(this).html('');
			if(!$(this).data('odontogram').disabled) { enableTools(); }
		},
		open: function(){
			disableZoomTools();
		}
	};


	this.configModalMbox = {
		width: 'auto',
		overflow: 'hidden',
		autoOpen: false,
		modal:true,

		close: function(event) {
			$(this).html('');
		},

	};

	this.teethByPositions = {};
}

Odontogram.prototype.load = function (jsonLoad){
	var _this = this;
	if (!!jsonLoad.dateod) { _this.dateod = jsonLoad.dateod; }
	if (!!jsonLoad.idodontogram) { _this.idodontogram = jsonLoad.idodontogram; }
	if (!!jsonLoad.observations) { $('#txtobservations').val(jsonLoad.observations); }
	if (!!jsonLoad.specifications) { $('#txtspecifications').val(jsonLoad.specifications); }
	if (!!jsonLoad.patient) {
		if(!!jsonLoad.patient.dateborn){
			console.log(jsonLoad.patient.dateborn);
			$('#odon_birthday').html(new Date(jsonLoad.patient.dateborn.substring(0,10)).toLocaleDateString());
		}
		if(!!jsonLoad.patient.name){
			$('#odon_name').html(jsonLoad.patient.name);
		}
		if(!!jsonLoad.patient.photo){
			$('#odon_photo').attr('src',jsonLoad.patient.photo);
		}
	}
	if(!!jsonLoad.operations){
		_this.loadOperations(jsonLoad.operations);
	}
	if(!!jsonLoad.odonthist && jsonLoad.odonthist.length>0){
		_this.initHistory(jsonLoad.odonthist);
	}
	if (!!jsonLoad.closed) { _this.disable(); }


}
var _odHistory = [];
Odontogram.prototype.initHistory = function(odHistory){
	var _this = this;
	var selMenuHist = {};
	var year = '';
	var but = '';
	_odHistory = odHistory;
	selMenuHist = $('#menu-hist');
	selMenuHist.children().remove();
	for (var i = _odHistory.length - 1; i >= 0; i--) {
		_odHistory[i].dt = new Date(_odHistory[i].dateodhist.substring(0,10));
		_odHistory[i].idodontogram = _odHistory[i].idodhist;
	}
	_odHistory = _odHistory.sort(function(a,b){
		return a.dt > b.dt ? -1 : 1;
	}
	);
	for (var i = 0; i < _odHistory.length; i++) {
		year = _odHistory[i].dt.getFullYear();
		if($('.od-timeline__year[data-year="'+year+'"]',selMenuHist).length === 0){
			$('<li class="od-timeline__year" data-year="'+ year+'">'+ year+' </li>').appendTo(selMenuHist);
		}
		but = '<button class="od-btn od-btn--rounded od-btn--xs od-btn--star '+(!!_odHistory[i].fav? 'od-btn--checked':'')+'"></button>';
		$('<li class="od-timeline__item '+ ((_odHistory[i].idodhist ==_this.idodontogram) ? 'od-timeline__item--active' :'') +'" data-idodhist='+_odHistory[i].idodhist+'>'+'<a>'+
			_odHistory[i].dt.toLocaleDateString()  +'</a>'+but+'</li>').appendTo(selMenuHist);

	}
	$('.od-btn--star',selMenuHist).click(function(event){
		var li= $(event.target).closest('li');
		var idodhist =li.attr('data-idodhist');
		$(event.target).toggleClass('od-btn--checked');
		var his = $.grep(_odHistory, function(his) { return his.idodhist == idodhist  })[0];
		his.fav = !!his.fav ? 0 : 1;
		var json = { 'idodontogram' :   idodhist  , 'fav' : his.fav };
		_this.$mainView.trigger('Odontogram.updateFav',json);
		var childs = li.parents().children();
		for (var i = childs.index(li) - 1; i >= 0; i--) {
			var child = $(childs[i]);
			if(!!child.attr('data-year') && child.hasClass('od-timeline__year--collapsed')){
				li.toggle();
			}
		}
		event.preventDefault();
		event.stopPropagation();
	});
	$('.od-timeline__item',selMenuHist).click(function(event) {
		var idodhist = $(event.target).closest('li').attr('data-idodhist');
		var his = $.grep(_odHistory, function(his) { return his.idodhist === idodhist  });
		if (!!his.odontoimg) {
			_this.loadHistory(his);
		}
		else{
			var json  = { 'idodontogram' :   idodhist  };
			_this.$mainView.trigger('Odontogram.getHistory', json);
		}
	});
	$('.od-timeline__year',selMenuHist).click(function(asd,das,qwe,rewqd) {
		$(this).toggleClass('od-timeline__year--collapsed');
		var childs = $(this).parent().children();
		for (var i = childs.index(this)+1; i < childs.length; i++) {
			var child = $(childs[i]);
			if(!!child.attr('data-year')){
				break;
			}
			if(($('.od-btn--checked',child).length === 0)){
				child.toggle();
			}
		}
	} );
}


Odontogram.prototype.loadOperations = function(operationsJSON) {
	var _this = this;
	for (var i = operationsJSON.length - 1; i >= 0; i--) {
		var op = operationsJSON[i];
		var operation = new Operation();
		operation.idtypeoperation = op.idtypeoperation;
		var operConfig =  $.grep(_this.configuration.tools, function(e) { return e.id == op.idtypeoperation })[0];
		operation.dateop = op.dateop;
		operation.teeth = op.teeth;
		operation.text =  operConfig.text;
		operation.status = op.status;
		operation.action = op.action;
		if (!!op.zone){
			operation.zone = op.zone;
		}
		operation.id = _getOperationName(operation.teeth, op.idtypeoperation,op.zone);

		//if (operConfig.tipo == _this.configuration.constants.tipotool.detallelinea)
		//{
			if(!!op.positions) {
				operation.positions = op.positions;
			}
			if (!!op.box){
				operation.box =  op.box;
			}
		//}
		_drawOperation(operation,_this);
		//_writeHistoryOperation(operation, _this, operation.id, _this.configuration.constants.pathSVG);
		_this.operations.push(operation);
	}
}
Odontogram.prototype.disable = function() {
	this.disabled = 1;
	disableTools();
	disableHistory();
	disableOdontButtons();
	$('.od-toolbar').hide();
	$('.od-media-msgdisabled').show();
	this.toolSelectDefault();
	//$('.od-media__image','.od-toolbar__tool--selected').find('img').removeAttr('src');
	//$('.od-media__title','.od-toolbar__tool--selected').addClass('od-media__title--readonly').html('Odontograma en modo solo lectura');
	$('.od-media__date','.od-media-msgdisabled').html(new Date(this.dateod).toLocaleString());
}
Odontogram.prototype.enable = function() {
	this.disabled = 0;
	this.$divToothDrag.dialog('close')
	enableTools();
	enableHistory();
	enableOdontButtons();
	//$('.od-media__title','.od-toolbar__tool--selected').removeClass('od-media__title--readonly')
	$('.od-toolbar').show();
	$('.od-media-msgdisabled').hide();
	this.toolSelectDefault();
}
var enableOdontButtons = function(){
	$('.od-btn','.od-media__buttons').removeClass ('od-btn--disabled');
}

var disableOdontButtons = function(){
	$('.od-btn','.od-media__buttons').addClass('od-btn--disabled');
}

var _createDivSpinner = function(mainView){
	$('<div class="odonspinner"><div class="odonspinner__item"/></div>').insertBefore('#'+mainView);
	$(document)
        // Listener a ajaxStart
        .ajaxStart(function () {
        	Odontogram.spinnerShow();
        })
        // Listener a ajaxStop
        .ajaxStop(function () {
        	Odontogram.spinnerHide();
        });
    }

    Odontogram.spinnerShow = function(nodelay){
    	showSpinner = true;
    	if(!!nodelay){
    		$('.odonspinner').removeClass('none');
    	}
    	window.setTimeout(function () {
    		if ( !$('.odonspinner').is(':visible') ){
    			if ( showSpinner ){
    				$('.odonspinner').removeClass('none');
    			}
    		}
    	}, 300);
    };

    Odontogram.spinnerHide = function(){

    	showSpinner = false;

    	if ( $('.odonspinner').is(':visible') ){
    		$('.odonspinner').addClass('none');
    	}

    };
    Odontogram.prototype.mbox = function(title,message,buttons){
    	var _this = this;
    	var cfg = {};
    	if (!buttons) {
    		buttons = { 	Cerrar: function() { 		$(this).dialog( 'close' );    	}}
    	}
    	cfg.buttons = buttons;
    	cfg.title = title;
    	_this.$divMbox.dialog($.extend(cfg,_this.configModalMbox));
    	_this.$divMbox.html(message);
    	_this.$divMbox.dialog('open');
    }
    Odontogram.prototype.configure = function (mainView,config,failure) {
    	var _this = this,
    	failure = failure || function(){};
    	_this.configuration = configGlobal;
    	_this.configurationDefault = configGlobal;
    	_this.mainView = mainView;
    	_overlapConfig(_this.configuration,config);
    	_createDivSpinner(mainView);
    	_menuLateralLeftCreate(mainView,_this.configuration.constants.pathSVG);
    	_menuLateralRightCreate(mainView,_this.configuration.constants.pathSVG);
    	_toolboxGenerate(_this,'main-menu');
    	_loadPrintCss(_this.configuration.constants.pathPrintCss);
    	_loadTeethByPositions(_this);
    	$.ajax({
    		type: 'GET',
    		url: _this.configuration.constants.pathSVG + _this.configuration.constants.svgbase,
    		datatype: 'text',
    	}).done(function(data) {
    		_this.$mainView =$('#'+mainView);
    		_this.datadocument = data.documentElement;
		//_this.datadocument.classList.add('odontogram-base');
		$(_this.datadocument).addClass('odontogram-base');

		_this.$mainView.append( _this.datadocument);
		_loadTeeth(_this,config,failure);
		_this.$divToothDrag = $('<div id="tooth_drag">');
		_this.$divToothDrag.appendTo(_this.$mainView);
		_this.$divToothDrag.dialog(_this.configModalTooth);
		_this.$divToothDrag.closest('div.ui-dialog').addClass('pop-tooth');
		_this.$divHistory = $('<div id="odont_hist"/>');
		_this.$divHistory.appendTo(_this.$mainView.parent());
		_this.$divHistory.dialog(_this.configModalHist);
		_this.$divHistory.closest("div.ui-dialog").addClass('pop-history');
		_this.$divMbox = $('<div id="odont_mbox"/>');
		_this.$divMbox.appendTo(_this.$mainView.parent());
		_this.$divMbox.dialog(_this.configModalMbox);
		_this.$divMbox.closest("div.ui-dialog").addClass('pop-mbox');
		_this.$mainView.on( 'Odontogram.init', function(){_init(_this);});
		_createFilters(_this.datadocument);
		$(".od-tab__content").mCustomScrollbar({
			theme:"minimal-dark"

					// autoExpandScrollbar:true
				});
		$(".od-container").mCustomScrollbar({
			theme:"minimal-light",
			autoHideScrollbar: true
					// autoExpandScrollbar:true
				});

	}).fail( function(error){
		failure(error);
	});
}
var _createFilterBorder = function(id,color,docum){
	var NS = "http://www.w3.org/2000/svg";
	var filter = document.createElementNS( NS, "filter" );
	filter.setAttribute( 'id', id );
	var feMorphology = document.createElementNS( NS, 'feMorphology' );
	//feMorphology.setAttribute( 'operator', 'erade' );
	feMorphology.setAttribute( 'radius', '3' );
	feMorphology.setAttribute( 'in', 'SourceGraphic' );
	feMorphology.setAttribute( 'result', 'thickInner' );
	var feFlood = document.createElementNS( NS, 'feFlood' );
	feFlood.setAttribute( 'flood-color', color );
	feFlood.setAttribute( 'result', 'COLOR' );

	var feComposite1 = document.createElementNS( NS, 'feComposite' );
	feComposite1.setAttribute( 'in', 'COLOR' );
	feComposite1.setAttribute( 'in2', 'SourceGraphic' );
	feComposite1.setAttribute( 'operator', 'in' );
	feComposite1.setAttribute( 'result', 'thickTotal' );

	var feComposite2 = document.createElementNS( NS, 'feComposite' );
	feComposite2.setAttribute( 'in', 'COLOR' );
	feComposite2.setAttribute( 'in2', 'thickInner' );
	feComposite2.setAttribute( 'operator', 'in' );
	feComposite2.setAttribute( 'result', 'thickInner' );

	var feComposite3 = document.createElementNS( NS, 'feComposite' );
	feComposite3.setAttribute( 'operator', 'out' );
	feComposite3.setAttribute( 'in', 'thickTotal' );
	feComposite3.setAttribute( 'in2', 'thickInner' );
	feComposite3.setAttribute( 'result', 'thickDif' );

	var feComposite4 = document.createElementNS( NS, 'feComposite' );
	feComposite4.setAttribute( 'in', 'SourceGraphic' );
	feComposite4.setAttribute( 'in2', 'thickDif' );
	feComposite4.setAttribute( 'operator', 'out' );
	feComposite4.setAttribute( 'result', 'specOut' );


	var feMerge = document.createElementNS( NS, 'feMerge' );
	var feMergeNode1 = document.createElementNS( NS, 'feMergeNode' );
	feMergeNode1.setAttribute( 'in', 'specOut' );

	var feMergeNode2 = document.createElementNS( NS, 'feMergeNode' );
	feMergeNode2.setAttribute( 'in', 'thickDif' );

	document.getElementById('base').appendChild(filter);

	feMerge.appendChild(feMergeNode1);
	feMerge.appendChild(feMergeNode2);

	filter.appendChild(feMorphology);
	filter.appendChild(feFlood);
	filter.appendChild(feComposite1);
	filter.appendChild(feComposite2);
	filter.appendChild(feComposite3);
	filter.appendChild(feComposite4);
	filter.appendChild(feMerge);

}
var _createFilters = function(docu){
	_createFilterBorder('borderblue','#028bca',docu);
	_createFilterBorder('borderred','#aa0000',docu);

}
var _loadTeethByPositions = function(odont){
	for (var i = odont.configuration.teeth.length - 1; i >= 0; i--) {
		var t = odont.configuration.teeth[i];
		if (t.contiguous.length == 1){
			if($.isEmptyObject(odont.teethByPositions) || !odont.teethByPositions[t.position]){
				odont.teethByPositions[t.position] = _getAllContiguousTeethFirstTime(t.id,odont.configuration.teeth);
			}
			else{
				if(odont.teethByPositions[t.position][0] > t.id){
					odont.teethByPositions[t.position] = _getAllContiguousTeethFirstTime(t.id,odont.configuration.teeth);
				}
			}
		}
	}
}

var _menuLateralRightCreate = function(mainView,pathSVG){
	$('<div class="aside--right">' +
		'      <button class="od-btn od-btn--primary od-btn--rounded aside__trigger">' +
		'        <img src="'+ pathSVG + '/ui/panel_ico.svg" alt="" class="od-ico ico--panel">' +
		'        <img src="'+ pathSVG + '/ui/arrow_fwd.svg" alt="" class="od-ico ico--fwd">' +
		'      </button>' +
		'      <div class="panel">' +
		'        <div class="panel__title">Especificaciones</div>' +
		'        <div class="panel__body">' +
		'          <textarea name="especificaciones" id="txtspecifications" cols="30" rows="10"></textarea>' +
		'        </div>' +
		'      </div>' +
		'      <div class="panel">' +
		'        <div class="panel__title">Observaciones</div>' +
		'        <div class="panel__body">' +
		'          <textarea name="Observaciones" id="txtobservations" cols="30" rows="10"></textarea>' +
		'        </div>' +
		'      </div>' +
		'      <div class="panel panel--historia">' +
		'        <div class="panel__title">Historial</div>' +
		'        <div class="panel__body">' +
		'				<input type="radio" name="tabs-radio"  class="od-radio tabs-radio" id="od-tab__rad1" checked >'+
		'				<div class="od-tab">'+
		'          			<label for="od-tab__rad1" class="od-tabs__label">Operaciones</label>' +
		'					<div class="od-tab__content">'+
		'          				<ul class="od-list od-list--selectable" id="menu-ops">' +
		'         				</ul>' +
		'					</div>'+
		'				</div>'+
		'				<input type="radio" name="tabs-radio" id="od-tab__rad2" class="od-radio tabs-radio"  >'+
		'				<div class="od-tab">'+
		'          			<label for="od-tab__rad2" class="od-tabs__label">Odontogramas</label>' +
		'					<div class="od-tab__content">'+
		'          				<ul class="od-timeline" id="menu-hist">' +
		'          				</ul>' +
		'					</div>'+
		'				</div>'+
		'        </div>' +
		'      </div>' +
		'    </div>').insertAfter('#'+mainView);

}
var _menuLateralLeftCreate = function(mainView,pathSVG){
	$('<div id="menu-lateral" class="main-nav menu-lateral">' +
		'<header class="menu-lateral__header">' +
		'<a href="javascript:void(0);" class="main-logo">' +
		'<img src="img/hosix_logo.png" alt="HOSIX" data-pin-nopin="true">' +
		'<br> ODONTOGRAMA' +
		'</a>' +
		'</header>' +
		'<div class="od-media od-media--top od-user-profile">' +
		'<div class="od-media__image ">' +
		'<img id="odon_photo" class="od-thumbnail od-thumbnail--rounded ">' +
		'</div>' +
		'<div class="od-media__body  ">' +
		'<h4 class="od-media__title" id="odon_name"></h4>' +
		'<p class="od-media__text" id="odon_birthday"></p>' +
		'<div class="od-media__options od-media__buttons">' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Restaurar" data-balloon-pos="down" id="btnRestaurar">' +
		'<img src="'+ pathSVG + '/ui/restore.svg" alt="" class="od-ico">' +
		'</button>' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Guardar" data-balloon-pos="down" id="btnGuardar">' +
		'<img src="'+ pathSVG + '/ui/save.svg" alt="" class="od-ico">' +
		'</button>' +
		'<button class="od-btn od-btn--primary od-btn--rounded od-btn--small" data-balloon="Finalizar" data-balloon-pos="down" id="btnFinalizar">' +
		'<img src="'+ pathSVG + '/ui/lock.svg" alt="" class="od-ico">' +
		'</button>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="od-media-msgdisabled" style="display:none;" >' +
		'<h5 class="od-media__title od-media__title--readonly">Odontograma en modo solo lectura</h5>' +
		'<div class="od-media__options--left od-media__date"></div>' +
		'</div>' +
		'<div class="main-menu" id="main-menu">' +
		'</div>' +
		'</div>').insertBefore('#'+mainView);
}

var _init = function (odontograma) {
	var _this = odontograma;
	//_this.resize();


	$('.tooth-trigger',_this.$mainView).click(function(event){
		var dataid = $(this).attr('data-idtooth');
		var tooth = $.grep(_this.teeth, function(e) { return e.id == dataid});
		if(tooth.length>0) {
			tooth[0].click(event);
		}
	});

	$(document).on('keyup.tooth',{},function(e) {
		if (e.keyCode === 27) {//ESC
			_this.enableTeeth();
			_this.deactivateTeeth();
			if (!_this.disabled) {
				enableHistory();
			}
		}
	});
	$('.od-btn','#menu-hist').bind('click', { odontogram : _this  },deleteOperation);
	$('.aside__trigger').click(function() {
		$('.aside--right').toggleClass('aside--collapsed');
	});
	_this.$mainView.trigger('Odontogram.ready');
	_this.ready = 1;

	$('#btnGuardar').click(function(){
		var json = _this.save(0,_this.configuration.constants.generateimg.save);
		_this.$mainView.trigger('Odontogram.save', [json]);
	});
	$('#btnFinalizar').click(function(){
		//var json = _this.save(1,_this.configuration.constants.generateimg.end);
		//_this.$mainView.trigger('Odontogram.end', [json]);
		var buttons = {
			Finalizar: function() {
				_this.dateod = new Date();
				var json = _this.save(1,_this.configuration.constants.generateimg.end);
				_this.$mainView.trigger('Odontogram.end', [json]);
				$(this).dialog( 'close' );
			},
			Cancelar: function() {
				$(this).dialog( 'close' );
			}};
			_this.mbox('Confirmación','Se va a finalizar el odontograma y no se podrá modificar, ¿desea continuar?',buttons);
		});
	$('#btnRestaurar').click(function(){
		_this.clear();
		var json = _this.save(0,0);
		_this.$mainView.trigger('Odontogram.restore', [json]);
	});
/*
	odontograma.$divHistory.on('mousedown',function(event){
 		//$(this).closest("div.ui-dialog").addClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").toggleClass('pop-history--mousedown');
 	});*/
	/*
	odontograma.$divHistory.on('mousedown',function(event){
 		//$(this).closest("div.ui-dialog").addClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").addClass('pop-history--mousedown');
	});
	odontograma.$divHistory.on('mouseup',function(){
		//$(this).closest("div.ui-dialog").removeClass('pop-history--mousedown');
 		_this.$divHistory.closest("div.ui-dialog").removeClass('pop-history--mousedown');
 	});*/

 }

 Odontogram.prototype.loadHistory =  function(json) {
 	var _this = this;
 	var w =_this.$mainView.parent().outerWidth();
 	var h =_this.$mainView.outerHeight();

 	$('#base').attr('width',w).attr('height',  h);

 	var odhis = $.grep(_odHistory, function(his) { return his.idodontogram == json.idodontogram})[0];
 	odhis.odontoimg = json.odontoimg;
 	odhis.specifications = json.specifications;
 	odhis.observations = json.observations;
 	_this.$divHistory.html('');
 	_this.$divHistory.append($('<img class="pop-history__img"  data-balloon="Click en la imagen para hacerla transparente"  data-balloon-pos="down" src="'+odhis.odontoimg+'"/>'));
 	_this.$divHistory.append(
 		$('<div class="pop-history__text">' +
 			'<h2 class="pop-history__heading">Especificaciones</h2>'+
 			'<p class="pop-history__p">'+odhis.specifications+'</p>' +
 			'<h2 class="pop-history__heading">Observaciones</h2>'+
 			'<p class="pop-history__p">'+odhis.observations+ '</p>' +
 			'</div>'
 			));
 	$('.pop-history__text',this.$divHistory).mCustomScrollbar({
 		theme:"minimal-dark"
 	});


 	_this.$divHistory.dialog({
 		'title': function() { $(this).html( 'Odontograma Histórico: <strong>'+odhis.dt.toLocaleDateString()+'</strong>')} ,
 		'width' : w-100,
 		'height' : h-100,
 	}).dialog('open');
 	$('.pop-history__img',_this.$divHistory).on('click',function(event){
 		_this.$divHistory.closest("div.ui-dialog").toggleClass('pop-history--mousedown');
 	});;


 }

 Odontogram.prototype.save =  function(close,generateImg) {
 	return odontogramToJSON(this,close,generateImg);
 }


 Odontogram.prototype.resize =  function() {
 	var _this = this.$mainView;
 	var w =_this.outerWidth();
 	var h =_this.outerHeight();

 	$('#base').attr('width',w).attr('height',  h);
 }

 Odontogram.prototype.clear =  function() {
 	var _this = this;
 	_clearOperations(_this);
 }


 var _overlapConfig =function(defaultConfig, newConfig) {
 	var keys = Object.keys(newConfig);
 	if(newConfig.hasOwnProperty('id')){
 		keys.splice(keys.indexOf('id'),1);
 	}
 	_subOverLapConfig(defaultConfig,newConfig,keys);
 }

 var _subOverLapConfig = function(defaultConfig,newConfig, keys){
 	for (var i = keys.length - 1; i >= 0; i--) {
 		if(typeof(defaultConfig[keys[i]]) === 'object'){
 			var  nextDefConfig = defaultConfig[keys[i]];
 			if(!defaultConfig[keys[i]].hasOwnProperty('id')){
 				_overlapConfig(defaultConfig[keys[i]],newConfig[keys[i]]);
 			}
 			else{
 				nextDefConfig = $.grep(defaultConfig, function(e){ return e.id == newConfig[keys[i]].id; })[0];
 				_overlapConfig(nextDefConfig,newConfig[keys[i]]);
 			}
 		}
 		else{
 			defaultConfig[keys[i]] = newConfig[keys[i]];
 		}
 	}
 }
 var _loadTeeth = function(odontogram,config,failure) {
 	var dien =  {},
 	toothConfiguration = {},
 	i = 0,
 	j =0,
 	objToothConfig = {},
 	times = {},
 	failure = failure || function(){};

 	times = { number: 0 ,total: odontogram.configuration.teeth.length};
 	for (i = 0; i < odontogram.configuration.teeth.length; i++) {
 		odontogram.teeth.push(new Tooth(odontogram.configuration.teeth[i]));
 		dien = odontogram.teeth[odontogram.teeth.length-1];
 		dien.callSVG(odontogram,function(){_teethWait(times,odontogram.$mainView);},failure);
 	}
 }
 var _teethWait = function(times,mainView){
 	times.number++;
 	if (times.number === times.total){
 		mainView.trigger('Odontogram.init');
 	}
 }

 Odontogram.prototype.toolSelectDefault =  function() {
 	$('.od-btn--active').removeClass('od-btn--active');
 	var _this = this;
 	_this.toolSelected = $.grep(_this.configuration.tools, function(obj) {
 		return obj.default === 1;
 	})[0];
 	if(!!_this.toolSelected.box) {
 		_this.toolSelected.boxSelected= _this.toolSelected.box[0];
 	}else{
 		_this.toolSelected.boxSelected= null;
 	}
 	if (!_this.toolSelected.hidden) {
 		$('.od-toolbar__btn[data-idtool='+_this.toolSelected.id+']').addClass('od-btn--active');
 	}
 }

 Odontogram.prototype.disableTeeth =  function(exceptions) {
 	var _this = this,
 	exceptions = exceptions || [];
 	for (var i = _this.teeth.length - 1; i >= 0; i--) {
 		if($.inArray(_this.teeth[i].id, exceptions)==-1){
 			_this.teeth[i].$datadocument.addClass('minitooth--disabled');
 			_this.teeth[i].$recuadrotooth.addClass('tooth-rect--disabled');
 		}
 	}
 }
 Odontogram.prototype.disableTeethExceptOperation =  function(operationId) {
 	var _this = this;
 	var oper = $.grep(_this.operations,function(e) { return e.id == operationId })[0];
 	var operConfig =  $.grep(_this.configuration.tools, function(e) { return e.id == oper.idtypeoperation })[0];
 	if (operConfig.tipo == _this.configuration.constants.tipotool.detallezona || $.isEmptyObject(oper.dom)){
 		var dtotal = $('#d'+oper.teeth[0]).find('#diente_total');
		//$('[id=diente_total]').not(dtotal).addClass('disabled');
		_this.disableTeeth(oper.teeth);
		$('.operation[id!='+operationId+']').addClass('disabled');
		//$('[id!='+oper.zone+']',dtotal).not('#dibujo').not('#corona').addClass('disabled')
		$.each(this.configuration.constants.zones, function(pos,zone)
			{if(zone != oper.zone)
				$('[id=' +zone+']',dtotal).addClass('disabled');
			});

	}
	else{
		//$('[id=diente_total]').addClass('disabled');
		_this.disableTeeth(oper.teeth);
		$('.operation[id!='+operationId+']').addClass('disabled');
	}
}

Odontogram.prototype.enableTeeth =  function() {
	$('.minitooth--disabled').removeClass('minitooth--disabled');
	$('.tooth-rect--disabled').removeClass('tooth-rect--disabled');
	$('[id=diente_total]').removeClass('disabled');
	$('.operation').removeClass('disabled');
	$.each(this.configuration.constants.zones, function(pos,zone) {$('[id=' +zone+']').removeClass('disabled');});
}
Odontogram.prototype.deactivateTeeth =  function() {
	var _this = this;
	$('.minitooth--active').removeClass('minitooth--active');
	$('.tooth-rect--active').removeClass('tooth-rect--active');
	_this.toothSelected = [];
}

Odontogram.prototype.findOperationsByTeeth =  function(teethIds,toolId){
	var _this = this;
	result = $.grep(_this.operations,function(e) {
		return (!toolId || e.idtypeoperation == toolId) && teethIds.every(function (t) { return e.teeth.indexOf(t) >= 0 })
	});
	return	 result;
}


_getAllContiguousTeethFirstTime = function(toothId,confTeeth){
	var _this = this;
	var wholeContiguous = [toothId];
	return _continueGetAllContiguousTeeth(toothId,wholeContiguous,confTeeth);
}

var _continueGetAllContiguousTeeth = function(toothId,wholeContiguous,configTeeth){
	var teethCfg  =$.grep(configTeeth, function(t) { return t.id == toothId})[0];
	for (var i = teethCfg.contiguous.length - 1; i >= 0; i--) {
		if(wholeContiguous.indexOf(teethCfg.contiguous[i]) == -1){
			wholeContiguous.push(teethCfg.contiguous[i]);
			_continueGetAllContiguousTeeth(teethCfg.contiguous[i],wholeContiguous,configTeeth);
		}
	}
	return wholeContiguous;
}

Odontogram.prototype.getAllContiguousTeeth = function(toothId){
	var tooth = $.grep(this.configuration.teeth, function(t) {  return t.id == toothId})[0];
	var copy = this.teethByPositions[tooth.position].slice(0);
	copy.sort();
	return copy;
}


Odontogram.prototype.getContiguousBetween2Tooth = function(toothIni,toothEnd){
	var tooth = $.grep(this.configuration.teeth, function(t) {  return t.id == toothIni})[0];
	var copy = this.teethByPositions[tooth.position].slice(0);
	var indexIni = copy.indexOf(toothIni);
	var indexEnd = copy.indexOf(toothEnd);
	if (indexIni > indexEnd){
		indexIni = [indexEnd, indexEnd = indexIni][0];//Esto es un swap de las variables
	}
	return copy.slice(indexIni,indexEnd+1).sort();
}




