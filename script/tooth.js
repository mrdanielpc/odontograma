"use strict";
function Tooth(obj) {
	var objProp, i =0;
	this.id = 0;
	this.position = -1;
	this.map = '';
	this.svg = '';
	this.text = '0';
	this.x = 0;
	this.y = 0;
	this.$map = {};
	this.odontogram = {};
	this.$recuadrotooth = {};
	this.$datadocument = '';
	this.dragging = false;
	this.ox = 0;
	this.oy = 0;
	this.circle = {};
	this.line = {};
	this.tempOperations = [];
	this.box = [];
	objProp = Object.keys(obj);
	for (i = objProp.length - 1; i >= 0; i--) {
		this[objProp[i]] = obj[objProp[i]];
	}
}

Tooth.prototype.callSVG = function(odontog,callback,failure) {
	var _this = this,
	callback = callback || function(){},
	failure = failure || function(){};
	_this.odontogram = odontog;
	$.ajax({
		type: 'GET',
		url: _this.odontogram.configuration.constants.pathSVG + _this.svg,
		datatype: 'HTML'
	}).done(function(data,status,response){
		_this.drawSVG(data);
		if (!!_this.map) {
			_this.drawRectTooth();
			_this.drawNumber();
		}


		callback();
	}).fail(function(error){
		failure(error);
	});
}


Tooth.prototype.drawSVG =  function(data) {
	var _this = this;
	var bbox = {};
	var $circChild = {};
	_this.$datadocument = $(data.documentElement).addClass('tooth-trigger').attr('data-idtooth', _this.id);
	_this.$map = $('#'+_this.map);
	//_this.$map.addClass('tooth-trigger');
	_this.$datadocument.addClass('minitooth');
   	//Cambiar el id del tooth
   	_this.$datadocument.find('#diente-total').attr('id','dd'+_this.id).addClass('tooth');
    //Cambiar el número del text
    _this.$datadocument.find('#txtnumero').text(_this.text).addClass('tooth__txt');
    //Cambiar la posición del tooth y ajustarla al centro del circulo de la base.
    $circChild = _this.$map.children();
    if (!!_this.$map[0]) {
    	_this.x = Math.round($circChild.attr('cx'));

    	_this.y = Math.round($circChild.attr('cy'));
    	_this.$map.attr('transform','translate('+_this.x+','+_this.y+')');

    	_this.$map.html(data.documentElement);
	    //El positionamiento hay que hacerlo después de insertar el html
	    //ya que no se sabe lo que ocupa en pantalla previamente
	    _this.x = Math.round(_this.x-_this.$map[0].childNodes[0].width.baseVal.value/2);
	    _this.y = Math.round(_this.y-_this.$map[0].childNodes[0].height.baseVal.value/2);
	    _this.$map.attr('transform','translate('+_this.x+','+_this.y+')');
	    _this.$map.attr('data-idtooth', _this.id);
	}
	else{

		_this.map = '';
	}
}
Tooth.prototype.drawRectTooth = function() {
	var _this = this;
	var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
	var y = 0;
	var $gR  = null;
	var  txt = null;
	//rect.setAttributeNS(null, 'x', _this.x);
	rect.setAttributeNS(null, 'height', '50');
	rect.setAttributeNS(null, 'width', _this.$datadocument.attr('width'));
	rect.setAttributeNS(null, 'fill', 'transparent');
	rect.setAttributeNS(null, 'stroke', '#134876');
	rect.setAttributeNS(null, 'id', 'rd'+_this.id);
	rect.setAttributeNS(null, 'data-idtooth', _this.id);
	rect.setAttributeNS(null, 'class', 'tooth-rect tooth-trigger');

	if (_this.position === _this.odontogram.configuration.constants.positiondent.top.final){
		y = _this.odontogram.configuration.constants.yrect.finalTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.top.temp){
		y = _this.odontogram.configuration.constants.yrect.tempsTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.final){
		y = _this.odontogram.configuration.constants.yrect.finalBot;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.temp){
		y = _this.odontogram.configuration.constants.yrect.tempsBot;
	}
	//rect.setAttributeNS(null, 'y', y);
	if(!$(_this.odontogram.datadocument).find('#rects').length) {
		createNewGroup(_this.odontogram.datadocument,'rects');
	}
	$gR = createNewGroup($(_this.odontogram.datadocument).find('#rects')[0],'grd'+_this.id);
	$gR.attr('transform','translate('+ _this.x+','+y+')');
	$gR[0].appendChild(rect);
	_this.$recuadrotooth = $gR;
	txt = document.createElementNS('http://www.w3.org/2000/svg', 'text');
	$gR[0].appendChild(txt);

}

Tooth.prototype.drawNumber = function() {
	var _this = this;
	var txtNumber = document.createElementNS('http://www.w3.org/2000/svg', 'text');
	var data = document.createTextNode(_this.text);
	var y = 0;
	txtNumber.setAttributeNS(null, 'x', (_this.x + _this.$map[0].getBBox().width/2));
	txtNumber.setAttributeNS(null, 'fill', 'red');
	txtNumber.setAttributeNS(null, 'stroke', '#134876');
	txtNumber.setAttributeNS(null, 'id', 'nd'+_this.id);
	txtNumber.setAttributeNS(null, 'data-idtooth', _this.id);
	txtNumber.setAttributeNS(null, 'class', 'tooth-text tooth-trigger');
	txtNumber.setAttributeNS(null, 'text-anchor', 'middle');
	//txtNumber.innerHTML = _this.text;
	txtNumber.appendChild(data);
	if (_this.position === _this.odontogram.configuration.constants.positiondent.top.final){
		y = _this.odontogram.configuration.constants.ynumber.finalTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.top.temp){
		y = _this.odontogram.configuration.constants.ynumber.tempsTop;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.final){
		y = _this.odontogram.configuration.constants.ynumber.finalBot;
	}
	else if(_this.position === _this.odontogram.configuration.constants.positiondent.bot.temp){
		y = _this.odontogram.configuration.constants.ynumber.tempsBot;
	}
	txtNumber.setAttributeNS(null, 'y', y);
	if(!$(_this.odontogram.datadocument).find('#numbers').length) {
		createNewGroup(_this.odontogram.datadocument,'numbers');
	}
	$(_this.odontogram.datadocument).find('#numbers')[0].appendChild(txtNumber);
	//_this.$recuadrotooth = $('#'+txtNumber.id,_this.odontogram.$mainView);
}

var openWindow = function(tooth){
	var _this = tooth;
	var toothPopup = _this.$datadocument.clone().attr('viewbox','0 0 50 130').removeClass('minitooth').addClass('maxitooth').removeAttr('width').attr('height',_this.odontogram.$mainView.height()-250);
	var boxT ='';
	$.each(tooth.box, function(pos,box) { boxT = boxT + box.text + ' ' });
	var srcOperations =$.map(tooth.odontogram.operations, function(op) { if(op.teeth.indexOf(tooth.id) >= 0) { return op.idtypeoperation } });
	var srcs = [];
	for (var i = srcOperations.length - 1; i >= 0; i--) {
		$.each(tooth.odontogram.configuration.tools, function(pos,tool) {
			if (!tool.zoom){
				if (tool.id == srcOperations[i]) {
					if(srcs.indexOf(tool.ico) == -1) {
						srcs.push(tool.ico);
					}}
				}});
	}


	var icons=$('<div class="tooth__icons">' +

		'</div>'
		);
	for (var i = srcs.length - 1; i >= 0; i--) {
		$('<img class="tooth__icon" src="'+ srcs[i] +'">').appendTo(icons);
	}
	var p = $('<p class="pBox">' +boxT+ '</p>');
	_this.odontogram.$divToothDrag.html();
	if (srcs.length > 0){	_this.odontogram.$divToothDrag.append(icons); }
	_this.odontogram.$divToothDrag.append(p);
	_this.odontogram.$divToothDrag.append(toothPopup);
	_this.tempOperations= [];

	toothPopup.on('mousedown.toothpop',{ tooth: _this, toothPopup: toothPopup } , _mousedown);
	_this.odontogram.divToothDragSave = function() { _this.toothCopy(toothPopup); }
	_this.odontogram.$divToothDrag.dialog(
	{

		'title': function() { $(this).html( 'Detalle Pieza: <strong>'+_this.text+'</strong>')} ,
		// 'class':'pepito2'
	}
	).data('odontogram',_this.odontogram).dialog('open');
}

Tooth.prototype.click =  function(event){
	var _this = this;
	if(!!_this.odontogram.toolSelected.zoom){
		openWindow(_this);
	}
	else {
		_this.addOperation(_this.odontogram.toolSelected);
	}
}

Tooth.prototype.cancelActive  = function(){
	var tooth = this;
	tooth.odontogram.enableTeeth();
	tooth.odontogram.deactivateTeeth();
	enableHistory();
}



Tooth.prototype.toothCopy =  function(toothPopup){
	var _this = this;
	for (var i = _this.tempOperations.length - 1; i >= 0; i--) {
		var previousOp = $.grep(_this.odontogram.operations, function (op){ return op.id == _this.tempOperations[i].id} );
		if (previousOp.length > 0){
			if(!!previousOp[0].positions){
				_this.tempOperations[i].positions = _this.tempOperations[i].positions.concat(previousOp[0].positions);
			}
			deleteOperation(previousOp[0],_this.odontogram);
		}
		if(!_this.tempOperations[i].deleted){
			_drawOperation(_this.tempOperations[i], _this.odontogram);
			_this.odontogram.operations.push(_this.tempOperations[i]);
		}
	}
	_this.tempOperations = [];
	/*
	var opn = toothPopup.find('#operations_new');
	if (opn.length){
		_this.$datadocument.html(toothPopup.children());
	}
	*/
}

var _checkClassOperation = function(oper,toolOperation,odont){
	if($.isArray(toolOperation.class) > 0){
		oper.status++;
		oper.dateop = new Date();
		if ( oper.status >= toolOperation.class.length )	{
			deleteOperation(oper,odont);
		}else{
			oper.dom.removeClass(toolOperation.class[oper.status-1]).addClass(toolOperation.class[oper.status]);
			if (toolOperation.class[oper.status] == 'operation--rotated'){
				oper.dom.attr('transform',   oper.dom.attr('transform') + ' translate(50,0)  scale(-1,1)');
			}
		}
	}
	else{
		deleteOperation(oper,odont);
	}
}
Tooth.prototype.addOperation =  function(toolOperation,callback,failure){
	var _this = this;
	var opn = _this.$datadocument.find('#operations_new');
	var newOperation = {};
	var opera = {},
	_class = '',
	_pos = -1,
	gop = {},
	$svg = {},
	callback = callback || function(){},
	failure = failure || function(){};



	if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.pieza){

		opera =_this.odontogram.findOperationsByTeeth([_this.id],_this.odontogram.toolSelected.id);
		if (opera.length > 0){
			newOperation = opera[0];
			_checkClassOperation(newOperation,toolOperation,_this.odontogram);
			/*if($.isArray(toolOperation.class) > 0){
				newOperation.status++;
				newOperation.dateop = new Date();
				if ( newOperation.status >= toolOperation.class.length )	{
					dibujarOperacion= false;
				}
			}
			else{
				dibujarOperacion= false;
			}*/
		}
		else{
			newOperation = new Operation()
			newOperation.id = _getOperationName(_this.id,toolOperation.id);
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.teeth = [];
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.teeth.push(_this.id);
			_this.odontogram.operations.push(newOperation);
			_drawOperation(newOperation,_this.odontogram);

		}

	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.todafila){
		opera =_this.odontogram.findOperationsByTeeth([_this.id],_this.odontogram.toolSelected.id);
		if (opera.length > 0 ){
			newOperation = opera[0];
			_checkClassOperation(newOperation,toolOperation,_this.odontogram);
		}
		else{
			newOperation = new Operation()
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.teeth = _this.odontogram.getAllContiguousTeeth(_this.id);
			_this.odontogram.operations.push(newOperation);
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.id = _getOperationName(newOperation.teeth,toolOperation.id);
			_drawOperation(newOperation,_this.odontogram);

		}

	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes ||
		toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.desdedienteadiente){


		if(_this.odontogram.toothSelected.length === 0){
			//Primer diente
			var teethDisable = [];
			if (toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes){
				teethDisable = _this.contiguous;
			}else{
				teethDisable = _this.odontogram.getAllContiguousTeeth(_this.id);
				teethDisable.splice(teethDisable.indexOf(_this.id),1);
			}
			_this.odontogram.disableTeeth(teethDisable);
			_this.$datadocument.removeClass('minitooth--disabled').addClass('minitooth--active');
			_this.$recuadrotooth.removeClass('tooth-rect--disabled').addClass('tooth-rect--active');
			_this.odontogram.toothSelected.push(_this);
			disableHistory(true);
		}
		else{
			//Segundo diente
			//Si es el mismo que el primero, cancelo operación
			if (_this.odontogram.toothSelected[0].id === _this.id)
			{
				_this.cancelActive();
			}
			else{
				//_this.odontogram.toothSelected.push(_this);
				//_this.odontogram.toothSelected = _this.odontogram.toothSelected.sort(function(a, b) {return a.id > b.id});
				var operFind = _this.odontogram.findOperationsByTeeth([_this.odontogram.toothSelected[0].id,_this.id],toolOperation.id);
				if (operFind.length > 0 ) {
					newOperation = operFind[0];
					_checkClassOperation(newOperation,toolOperation,_this.odontogram);
					//deleteOperation(operFind[0],_this.odontogram);
				}else{

					newOperation = new Operation()
					newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
					newOperation.text = _this.odontogram.toolSelected.text;
					newOperation.teeth = [];
					if (toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.dosdientes){
						newOperation.teeth.push(_this.id);
						newOperation.teeth.push(_this.odontogram.toothSelected[0].id);
					}else{
						newOperation.teeth = _this.odontogram.getContiguousBetween2Tooth(_this.id, _this.odontogram.toothSelected[0].id);
					}
					if(!!_this.odontogram.toolSelected.boxSelected){
						newOperation.box  = _this.odontogram.toolSelected.boxSelected;
					}
					newOperation.teeth.sort();
					newOperation.id = _getOperationName(newOperation.teeth,toolOperation.id);
					_this.odontogram.operations.push(newOperation);
					_drawOperation(newOperation,_this.odontogram);

				}

				_this.cancelActive();
			}
		}
	}
	else if(toolOperation.tipo === _this.odontogram.configuration.constants.tipotool.detallelinea)	{

		opera = $.grep(_this.tempOperations,function(e) {
			return e.idtypeoperation == toolOperation.id && e.teeth.indexOf(_this.id) >= 0
		});

		//opera =_this.odontogram.findOperationsByTeeth([_this.id],toolOperation.id);
		if(opera.length === 0) {

			newOperation = new Operation();
			newOperation.id = _getOperationName(_this.id,_this.odontogram.toolSelected.id);
			newOperation.dom = _this.line.toothPopup.find('#'+newOperation.id )[0];
			newOperation.idtypeoperation = _this.odontogram.toolSelected.id;
			newOperation.text = _this.odontogram.toolSelected.text;
			newOperation.positions = [{
				x1: _this.line.x1.baseVal.value,
				y1: _this.line.y1.baseVal.value,
				x2: _this.line.x2.baseVal.value,
				y2: _this.line.y2.baseVal.value
			}];
			if(!!_this.odontogram.toolSelected.boxSelected){
				newOperation.box  = _this.odontogram.toolSelected.boxSelected;
			}
			newOperation.teeth = [];
			newOperation.teeth.push(_this.id);
			newOperation.dateop = new Date();
			_this.tempOperations.push(newOperation);

		}
		else{
			newOperation = opera[0];
			newOperation.positions.push({
				x1: _this.line.x1.baseVal.value,
				y1: _this.line.y1.baseVal.value,
				x2: _this.line.x2.baseVal.value,
				y2: _this.line.y2.baseVal.value
			});
		}
		//_writeHistoryOperation(newOperation,_this.odontogram, newOperation.id 	,_this.odontogram.configuration.constants.pathSVG);
	}

}


function _mousedown(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var _x = 0;
	var _y = 0;
	var dim = {};

	if(event.which === 1 && !_tooth.dragging){
		if (_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallecirculo){
			//Dibujar circulo caries/empastes...

			_tooth.ox = event.screenX;
			_tooth.oy = event.screenY;
			_tooth.circle =  document.createElementNS('http://www.w3.org/2000/svg', 'circle');
			dim = toothPopup[0].getBoundingClientRect();
			if(!toothPopup.find('#operations_new').length) {
				createNewGroup(toothPopup[0],'operations_new');
			}
        	//Coordenadas relativas al contenedor
        	_x = event.clientX - dim.left;
        	_y = event.clientY - dim.top;
			//Coordenadas relativas al viewbox
			_x = _x*toothPopup[0].viewBox.baseVal.width/dim.width;
			_y = _y*toothPopup[0].viewBox.baseVal.height/dim.height;

			_tooth.circle.setAttributeNS(null, 'cx', _x);
			_tooth.circle.setAttributeNS(null, 'cy', _y);
			_tooth.circle.setAttributeNS(null, 'fill', _tooth.odontogram.toolSelected.color);
			if (!!_tooth.odontogram.toolSelected.class) _tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
			_tooth.circle.setAttributeNS(null, 'id', 'cd'+_tooth.id);
			_tooth.circle.setAttributeNS(null, 'data-idtooth', _tooth.id);
			_tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class+' operation--drawing');
			_tooth.circle.setAttributeNS(null, 'r', '2');
			toothPopup.find('#operations_new')[0].appendChild(_tooth.circle);
			_tooth.dragging = true;
			$('body').one('mouseup.toothpop', { tooth: _tooth, toothPopup: toothPopup } , _mouseupCircle);
			toothPopup.on('mousemove.toothpop', { tooth: _tooth, toothPopup: toothPopup }, _mousemoveCircle);
		}
		else if (_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallelinea){
			//Dibujar linea para fractura
			if(event.which === 1 && !_tooth.dragging){
				_tooth.ox = event.screenX;
				_tooth.oy = event.screenY;
				_tooth.line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
				dim = toothPopup[0].getBoundingClientRect();
				if(!toothPopup.find('#operations_new').length) {
					createNewGroup(toothPopup[0],'operations_new');
				}
				if(!toothPopup.find('#'+_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id)).length) {
					createNewGroup(toothPopup.find('#operations_new')[0],_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id));
				}
			 	//Coordenadas relativas al contenedor
			 	var _x1 = event.clientX - dim.left;
			 	var _y1 = event.clientY - dim.top;
				//Coordenadas relativas al viewbox
				var _x1 = _x1*toothPopup[0].viewBox.baseVal.width/dim.width;
				var _y1 = _y1*toothPopup[0].viewBox.baseVal.height/dim.height;

				var  _x2 = _x1;

				var  _y2 = _y1;

				_tooth.line.setAttributeNS(null, 'x1', _x1);
				_tooth.line.setAttributeNS(null, 'y1', _y1);
				_tooth.line.setAttributeNS(null, 'x2', _x2);
				_tooth.line.setAttributeNS(null, 'y2', _y2);
				_tooth.line.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class + ' operation--drawing');
				toothPopup.find('#'+_getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id))[0].appendChild(_tooth.line);
				_tooth.line.toothPopup = toothPopup;
				_tooth.dragging = true;
				$('body').one('mouseup.toothpop', { tooth: _tooth, toothPopup: toothPopup } , _mouseupLine);
				toothPopup.on('mousemove.toothpop', { tooth: _tooth, toothPopup: toothPopup }, _mousemoveLine);
			}
		}
		else if(_tooth.odontogram.toolSelected.tipo == _tooth.odontogram.configuration.constants.tipotool.detallezona){
			var operConfig = $.grep(_tooth.odontogram.configuration.tools,function(e) { return e.id == _tooth.odontogram.toolSelected.id })[0];
			var zoneClick = $.inArray(event.target.id, operConfig.zones);
			if (zoneClick == -1){
				zoneClick = $.inArray(event.target.parentNode.id, operConfig.zones);
				if(zoneClick> -1){
					$nodeClicked = $('#'+event.target.parentNode.id,toothPopup);
				}
			}
			else{
				$nodeClicked = $('#'+event.target.id,toothPopup);
			}
			if (zoneClick > -1){
				var zone = operConfig.zones[zoneClick];
				opera = $.grep(_tooth.tempOperations,function(e) {
					return e.idtypeoperation == _tooth.odontogram.toolSelected.id && e.teeth.indexOf(_tooth.id) >= 0 && e.zone == zone
				});

				//opera =_tooth.odontogram.findOperationsByTeeth([_tooth.id],toolOperation.id);
				if(opera.length === 0) {
					var opName = _getOperationName(_tooth.id,_tooth.odontogram.toolSelected.id,zone);;
					opera = $.grep(_tooth.odontogram.operations,function(e) {
						return e.id == opName;
					});
					newOperation = new Operation();
					newOperation.id = opName;
					newOperation.idtypeoperation = _tooth.odontogram.toolSelected.id;
					newOperation.text = _tooth.odontogram.toolSelected.text;
					newOperation.zone = zone;
					newOperation.teeth = [];

					newOperation.teeth.push(_tooth.id);
					newOperation.dateop = new Date();
					_tooth.tempOperations.push(newOperation);
					if(opera.length===0){
						if(!!_tooth.odontogram.toolSelected.boxSelected){
							newOperation.box  = _tooth.odontogram.toolSelected.boxSelected;
							$('.pBox').text($('.pBox').text() +  newOperation.box + ' ');
						}
						if(!!operConfig.fn){
							window[operConfig.fn]($nodeClicked,newOperation,operConfig);
						}
						else{
							$nodeClicked .addClass(_tooth.odontogram.toolSelected.class);
							$nodeClicked.children().addClass(_tooth.odontogram.toolSelected.class);
							newOperation.deleted = 0;
						}
					}else{
						if (!!opera[0].box) {
							$('.pBox').text($('.pBox').text().replace(opera[0].box+ ' ',''));
						}
						$nodeClicked.find('#'+newOperation.id).remove();
						$nodeClicked.removeClass(_tooth.odontogram.toolSelected.class);
						$nodeClicked.children().removeClass(_tooth.odontogram.toolSelected.class);
						newOperation.deleted = 1;
						//if(!!newOperation.dom) { newOperation.dom.remove(); }
					}
				}
				else{
					newOperation = opera[0];
					if(!newOperation.deleted){
						if (!!newOperation.box) {
							$('.pBox').text($('.pBox').text().replace(newOperation.box+ ' ',''));
						}
						$nodeClicked.removeClass(_tooth.odontogram.toolSelected.class);
						$nodeClicked.children().removeClass(_tooth.odontogram.toolSelected.class);
						newOperation.deleted = 1;
					}else{
						if(!!_tooth.odontogram.toolSelected.boxSelected){
							newOperation.box  = _tooth.odontogram.toolSelected.boxSelected;
							$('.pBox').text($('.pBox').text() +  newOperation.box + ' ');
						}
						if(!!operConfig.fn){
							window[operConfig.fn]($nodeClicked,newOperation,operConfig);
						}else{
							$nodeClicked .addClass(_tooth.odontogram.toolSelected.class);
							$nodeClicked.children().addClass(_tooth.odontogram.toolSelected.class);

						}
						newOperation.dateop = new Date();
						newOperation.deleted = 0;
					}
				}
			}

		}
	}
}
function _getOperationName(toothId,toolId,zone){
	var strzone = '';
	if (!!zone) { strzone = '_z' +zone;}
	return 'op_t' + toothId.toString().replace(/[,]/g,'_') + '_o' + toolId + strzone;
}
function _mousemoveCircle(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var catetoX = 0,
	catetoY = 0,
	r = 0;
	if (_tooth.dragging) {
		catetoX = Math.abs(event.screenX-  _tooth.ox)*toothPopup[0].viewBox.baseVal.width/toothPopup[0].getBoundingClientRect().width;

		catetoY = Math.abs(event.screenY-  _tooth.oy)*toothPopup[0].viewBox.baseVal.height/toothPopup[0].getBoundingClientRect().height;
		r = Math.sqrt(Math.pow(catetoX,2)+ Math.pow(catetoY,2));
		if (r>= 2){
			_tooth.circle.setAttributeNS(null,'r', r);
		}
	}
}

function _mouseupCircle(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	if (_tooth.dragging){
		_tooth.dragging = false;
		_tooth.circle.setAttributeNS(null, 'fill', _tooth.odontogram.toolSelected.color);
		_tooth.circle.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
		toothPopup.off('mousemove.toothpop');
	}
}

function _mousemoveLine(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var _x2 = 0,
	_y2 = 0,
	dim = {};

	if (_tooth.dragging) {
		dim = toothPopup[0].getBoundingClientRect();
		_x2 = (event.clientX - dim.left)*toothPopup[0].viewBox.baseVal.width/dim.width;
		_y2 = (event.clientY - dim.top)*toothPopup[0].viewBox.baseVal.height/dim.height;

		_tooth.line.setAttributeNS(null,'x2', _x2);
		_tooth.line.setAttributeNS(null,'y2', _y2);
	}
}

function _mouseupLine(event){
	var _tooth = event.data.tooth;
	var toothPopup = event.data.toothPopup;
	var result = {};

	if (_tooth.dragging){
		_tooth.dragging = false;
		_tooth.line.setAttributeNS(null, 'class', _tooth.odontogram.toolSelected.class);
		toothPopup.off('mousemove.toothpop');
		_tooth.addOperation(_tooth.odontogram.toolSelected);

	}
}

function createNewGroup(svg,id){
	var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
	g.setAttributeNS(null, 'id', id);
	svg.appendChild(g);
	return $(g);
}



