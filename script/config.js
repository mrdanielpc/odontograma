var configGlobal = {
    'constants': {
        'sex': {
            'male':0,
            'female':1,
            'unknown':2
        },
        'ajaxLoader': 1,
        'pathSVG': './dist/svg',
        'pathPrintCss': './dist/print.css',
        'idsvgbase': 'base',
        'svgbase': '/baseTotal.svg',
        'generateimg': {
            'save' : 0,
            'end' : 1
        },
        'yrect': {
            'finalBot': 950,
            'finalTop': 0,
            'tempsBot': 900,
            'tempsTop': 50
        },
        'zones': {
            'mesial'     : 'm',
            'lingual'    : 'l',
            'distal'     : 'd',
            'oclusal'    : 'o',
            'vestibular' : 'v',
            'raiz1'      : 'r1',
            'raiz2'      : 'r2',
            'raiz3'      : 'r3'

        },
        'ynumber': {
            'finalBot': 850,
            'finalTop': 150,
            'tempsBot': 520,
            'tempsTop': 490
        },
        'positiondent'  : {
            'top': {
                'final': 0,
                'temp': 1
            },
            'bot': {
                'temp': 2,
                'final': 3
            }
        },
        'actiontype' : {
            'new' : 1,
            'remain' : 2
        },
        'tipotool'  : {
            'desdedienteadiente' : 1,
            'todafila' : 2,
            'detallezona' : 3,
            'pieza': 4,
            'grupopiezas': 5,
            'dosdientes': 6,
            'detallelinea': 7,
            'detallecirculo': 8
        }
    },
    'teeth': [
    {
        'contiguous': [
        21,
        12
        ],
        'id': 11,
        'map': 'd11',
        'position': 0,
        'svg': '/dientes/d11.svg',
        'text': 11
    },
    {
        'contiguous': [
        14,
        16
        ],
        'id': 15,
        'map': 'd15',
        'position': 0,
        'svg': '/dientes/d15.svg',
        'text': 15
    },
    {
        'contiguous': [
        17,
        15
        ],
        'id': 16,
        'map': 'd16',
        'position': 0,
        'svg': '/dientes/d16.svg',
        'text': 16
    },
    {
        'contiguous': [
        11,
        13
        ],
        'id': 12,
        'map': 'd12',
        'position': 0,
        'svg': '/dientes/d12.svg',
        'text': 12
    },
    {
        'contiguous': [
        12,
        14
        ],
        'id': 13,
        'map': 'd13',
        'position': 0,
        'svg': '/dientes/d13.svg',
        'text': 13
    },
    {
        'contiguous': [
        13,
        15
        ],
        'id': 14,
        'map': 'd14',
        'position': 0,
        'svg': '/dientes/d14.svg',
        'text': 14
    },
    {
        'contiguous': [
        11,
        22
        ],
        'id': 21,
        'map': 'd21',
        'position': 0,
        'svg': '/dientes/d21.svg',
        'text': 21
    },
    {
        'contiguous': [
        21,
        23
        ],
        'id': 22,
        'map': 'd22',
        'position': 0,
        'svg': '/dientes/d22.svg',
        'text': 22
    },
    {
        'contiguous': [
        22,
        24
        ],
        'id': 23,
        'map': 'd23',
        'position': 0,
        'svg': '/dientes/d23.svg',
        'text': 23
    },
    {
        'contiguous': [
        23,
        25
        ],
        'id': 24,
        'map': 'd24',
        'position': 0,
        'svg': '/dientes/d24.svg',
        'text': 24
    },
    {
        'contiguous': [
        24,
        26
        ],
        'id': 25,
        'map': 'd25',
        'position': 0,
        'svg': '/dientes/d25.svg',
        'text': 25
    },
    {
        'contiguous': [
        25,
        27
        ],
        'id': 26,
        'map': 'd26',
        'position': 0,
        'svg': '/dientes/d26.svg',
        'text': 26
    },
    {
        'contiguous': [
        26,
        28
        ],
        'id': 27,
        'map': 'd27',
        'position': 0,
        'svg': '/dientes/d27.svg',
        'text': 27
    },
    {
        'contiguous': [
        27
        ],
        'id': 28,
        'map': 'd28',
        'position': 0,
        'svg': '/dientes/d28.svg',
        'text': 28
    },
    {
        'contiguous': [
        84
        ],
        'id': 85,
        'map': 'd85',
        'position': 2,
        'svg': '/dientes/d85.svg',
        'text': 85
    },
    {
        'contiguous': [
        46,
        44
        ],
        'id': 45,
        'map': 'd45',
        'position': 3,
        'svg': '/dientes/d45.svg',
        'text': 45
    },
    {
        'contiguous': [
        18,
        16
        ],
        'id': 17,
        'map': 'd17',
        'position': 0,
        'svg': '/dientes/d17.svg',
        'text': 17
    },
    {
        'contiguous': [
        17
        ],
        'id': 18,
        'map': 'd18',
        'position': 0,
        'svg': '/dientes/d18.svg',
        'text': 18
    },
    {
        'contiguous': [
        55,
        53
        ],
        'id': 54,
        'map': 'd54',
        'position': 1,
        'svg': '/dientes/d54.svg',
        'text': 54
    },
    {
        'contiguous': [
        54,
        52
        ],
        'id': 53,
        'map': 'd53',
        'position': 1,
        'svg': '/dientes/d53.svg',
        'text': 53
    },
    {
        'contiguous': [
        53,
        51
        ],
        'id': 52,
        'map': 'd52',
        'position': 1,
        'svg': '/dientes/d52.svg',
        'text': 52
    },
    {
        'contiguous': [
        52,
        61
        ],
        'id': 51,
        'map': 'd51',
        'position': 1,
        'svg': '/dientes/d51.svg',
        'text': 51
    },
    {
        'contiguous': [
        51,
        62
        ],
        'id': 61,
        'map': 'd61',
        'position': 1,
        'svg': '/dientes/d61.svg',
        'text': 61
    },
    {
        'contiguous': [
        61,
        63
        ],
        'id': 62,
        'map': 'd62',
        'position': 1,
        'svg': '/dientes/d62.svg',
        'text': 62
    },
    {
        'contiguous': [
        62,
        64
        ],
        'id': 63,
        'map': 'd63',
        'position': 1,
        'svg': '/dientes/d63.svg',
        'text': 63
    },
    {
        'contiguous': [
        85,
        83
        ],
        'id': 84,
        'map': 'd84',
        'position': 2,
        'svg': '/dientes/d84.svg',
        'text': 84
    },
    {
        'contiguous': [
        84,
        82
        ],
        'id': 83,
        'map': 'd83',
        'position': 2,
        'svg': '/dientes/d83.svg',
        'text': 83
    },
    {
        'contiguous': [
        83,
        81
        ],
        'id': 82,
        'map': 'd82',
        'position': 2,
        'svg': '/dientes/d82.svg',
        'text': 82
    },
    {
        'contiguous': [
        82,
        71
        ],
        'id': 81,
        'map': 'd81',
        'position': 2,
        'svg': '/dientes/d81.svg',
        'text': 81
    },
    {
        'contiguous': [
        81,
        72
        ],
        'id': 71,
        'map': 'd71',
        'position': 2,
        'svg': '/dientes/d71.svg',
        'text': 71
    },
    {
        'contiguous': [
        71,
        73
        ],
        'id': 72,
        'map': 'd72',
        'position': 2,
        'svg': '/dientes/d72.svg',
        'text': 72
    },
    {
        'contiguous': [
        72,
        74
        ],
        'id': 73,
        'map': 'd73',
        'position': 2,
        'svg': '/dientes/d73.svg',
        'text': 73
    },
    {
        'contiguous': [
        73,
        75
        ],
        'id': 74,
        'map': 'd74',
        'position': 2,
        'svg': '/dientes/d74.svg',
        'text': 74
    },
    {
        'contiguous': [
        74
        ],
        'id': 75,
        'map': 'd75',
        'position': 2,
        'svg': '/dientes/d75.svg',
        'text': 75
    },
    {
        'contiguous': [
        63,
        65
        ],
        'id': 64,
        'map': 'd64',
        'position': 1,
        'svg': '/dientes/d64.svg',
        'text': 64
    },
    {
        'contiguous': [
        64
        ],
        'id': 65,
        'map': 'd65',
        'position': 1,
        'svg': '/dientes/d65.svg',
        'text': 65
    },
    {
        'contiguous': [
        47
        ],
        'id': 48,
        'map': 'd48',
        'position': 3,
        'svg': '/dientes/d48.svg',
        'text': 48
    },
    {
        'contiguous': [
        48,
        46
        ],
        'id': 47,
        'map': 'd47',
        'position': 3,
        'svg': '/dientes/d47.svg',
        'text': 47
    },
    {
        'contiguous': [
        47,
        45
        ],
        'id': 46,
        'map': 'd46',
        'position': 3,
        'svg': '/dientes/d46.svg',
        'text': 46
    },
    {
        'contiguous': [
        45,
        43
        ],
        'id': 44,
        'map': 'd44',
        'position': 3,
        'svg': '/dientes/d44.svg',
        'text': 44
    },
    {
        'contiguous': [
        44,
        42
        ],
        'id': 43,
        'map': 'd43',
        'position': 3,
        'svg': '/dientes/d43.svg',
        'text': 43
    },
    {
        'contiguous': [
        43,
        41
        ],
        'id': 42,
        'map': 'd42',
        'position': 3,
        'svg': '/dientes/d42.svg',
        'text': 42
    },
    {
        'contiguous': [
        42,
        31
        ],
        'id': 41,
        'map': 'd41',
        'position': 3,
        'svg': '/dientes/d41.svg',
        'text': 41
    },
    {
        'contiguous': [
        41,
        32
        ],
        'id': 31,
        'map': 'd31',
        'position': 3,
        'svg': '/dientes/d31.svg',
        'text': 31
    },
    {
        'contiguous': [
        31,
        33
        ],
        'id': 32,
        'map': 'd32',
        'position': 3,
        'svg': '/dientes/d32.svg',
        'text': 32
    },
    {
        'contiguous': [
        32,
        34
        ],
        'id': 33,
        'map': 'd33',
        'position': 3,
        'svg': '/dientes/d33.svg',
        'text': 33
    },
    {
        'contiguous': [
        33,
        35
        ],
        'id': 34,
        'map': 'd34',
        'position': 3,
        'svg': '/dientes/d34.svg',
        'text': 34
    },
    {
        'contiguous': [
        34,
        36
        ],
        'id': 35,
        'map': 'd35',
        'position': 3,
        'svg': '/dientes/d35.svg',
        'text': 35
    },
    {
        'contiguous': [
        35,
        37
        ],
        'id': 36,
        'map': 'd36',
        'position': 3,
        'svg': '/dientes/d36.svg',
        'text': 36
    },
    {
        'contiguous': [
        36,
        38
        ],
        'id': 37,
        'map': 'd37',
        'position': 3,
        'svg': '/dientes/d37.svg',
        'text': 37
    },
    {
        'contiguous': [
        37
        ],
        'id': 38,
        'map': 'd38',
        'position': 3,
        'svg': '/dientes/d38.svg',
        'text': 38
    },
    {
        'contiguous': [
        54
        ],
        'id': 55,
        'map': 'd55',
        'position': 1,
        'svg': '/dientes/d55.svg',
        'text': 55
    }
    ],
    'tools': [
    {

        'id'      : 1,
        'ico'     : '/tools/01_ApOrtFijo.svg',
        'text'    : 'Ap. Ort. Fijo',
        'desc'    : 'Aparato ortodóntico fijo',
        'tipo'    : 1,
        'svg'     : '/ops/01_ApOrtFIjo_Medio.svg',
        'svg2'    : '/ops/01_ApOrtFIjo_Extremo.svg',
        'class'   : ['operation--blue', 'operation--red'],
        'lastturned': 1,
        'reversible' : 1,
        'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },



    },
    {

        'id'      : 2,
        'ico'     : '/tools/02_ApOrtRem.svg',
        'text'    : 'Ap. Ort. Remov.',
        'desc'    : 'Aparato ortodóntico removible',
        'tipo'    : 2,
        'svg'     : '/ops/02_ApOrtRem.svg',
        'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },

    },
    {
        'id'    : 3,
        'ico'   : '/tools/03_Caries.svg',
        'text'  : 'Caries',
        'desc'  : 'Caries',
        'tipo'  : 3,
        'zoom'  : true,
        'class' : 'caries',
        'zones': ['v','o','m','l','d','r1','r2','r3']

    },
    {
        'id'       : 4,
        'box' : ['CC','CF','CMC','3/4','4/5','7/8','CV','CJ'],
        'ico'      : '/tools/04_CoronaDef.svg',
        'text'     : 'Corona Definitiva',
        'desc'     : 'Corona Definitiva',
        'tipo'     : 4,
        'svg'        : '/ops/04_CoronaDef.svg',
        'reversible' : 1,
        'txtclass'            : 'od-op-txt od-op-txt--blue',

    },
    {
        'color'            : 'red',
        'id'               : 5,
        'box'         : ['CC','CF','CMC','3/4','4/5','7/8','CV','CJ'],
        'txtclass'            : 'od-op-txt od-op-txt--blue',
        'ico'              : '/tools/05_CoronaTemp.svg',
        'text'             : 'Corona Temporal',
        'desc'             : 'Corona Temporal',
        'tipo'             : 4,
        'svg'        : '/ops/05_CoronaTemp.svg',
        'reversible' : 1,
    },

    {
     'id'         : 6,
     'ico'        : '/tools/06_Diastema.svg',
     'text'       : 'Diastema',
     'desc'       : 'Diastema',
     'tipo'       : 6,
     'svg'        : '/ops/06_Diastema.svg',
     'reversible' : 1
 },
 {
     'color'      : 'blue',
     'id'         : 7,
     'ico'        : '/tools/07_Ausente.svg',
     'text'       : 'Diente Ausente',
     'desc'       : 'Diente Ausente',
     'tipo'       : 4,
     'svg'        : '/ops/07_Ausente.svg',
     'reversible' : 1
 },
 {
    'color'            : 'blue',
    'id'               : 8,
    'box'              : ['DIS'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
    'ico'              : '/tools/08_Discromico.svg',
    'text'             : 'Discrómico',
    'desc'             : 'Diente Discrómico',
    'tipo'             : 4,

},
{
 'id'         : 9,
 'ico'        : '/tools/09_Ectopico.svg',
 'text'       : 'Ectópico',
 'desc'       : 'Diente Ectópico',
 'tipo'       : 4,
    'box'              : ['E'],
    'class'            : 'operation--blue',
    'txtclass'            : 'od-op-txt od-op-txt--blue',

},
{
 'color'      : 'blue',
 'id'         : 10,
 'ico'        : '/tools/10_Erupcion.svg',
 'text'       : 'Erupción',
 'desc'       : 'Diente En Erupción',
 'tipo'       : 4,
 'svg'        : '/ops/10_Erupcion.svg',
 'reversible' : 1
},
{
    'id'               : 11,
    'ico'              : '/tools/11_Clavija.svg',
    'text'             : 'Clavija',
    'desc'             : 'Diente En Clavija',
    'svg'        : '/ops/11_Clavija.svg',
    'tipo'             : 4,
    'offsetsvg' : {
                'y': -55,
                'y1': 135,
                'y2': -65,
                'y3': 110,
             },
},
{
    'color'            : 'blue',
    'id'               : 12,
    'ico'              : '/tools/12_Extrusion.svg',
    'text'             : 'Extrusión',
    'desc'             : 'Diente Extruido',
      'svg'        : '/ops/12_Extrusion.svg',
    'tipo'             : 4,
    'offsetsvg' : {
                'y': 135,

             },
    'reversible' :1,
},
{

    'id'               : 13,
    'ico'              : '/tools/13_Intrusion.svg',
    'text'             : 'Intrusión',
    'desc'             : 'Diente Intruido',
    'svg'        : '/ops/13_Intrusion.svg',
    'tipo'      : 4,
        'offsetsvg' : {
                'y': 135,
             },
    'reversible' :1,

},
{
    'color'            : 'blue',
    'id'               : 14,
    'ico'              : '/tools/14_Edentulo.svg',
    'text'             : 'Edéntulo Total',
    'desc'             : 'Edéntulo Total',
    'tipo'             : 2,
    'svg'              : '/ops/14_Edentulo.svg',
    'offsetsvg' : {
                'x': -7,
                'y': 85,
                'y2': 0,
                'y3': 0
             },
},
{
    'id'    : 15,
    'ico'   : '/tools/15_Fractura.svg',
    'text'  : 'Fractura',
    'desc'  : 'Fractura',
    'tipo'  : 7,
    'zoom'  : true,
    'class' : 'fractura'


},
{
    'id'               : 16,
    'ico'              : '/tools/16_Geminacion.svg',
    'svg'              : '/ops/16_Geminacion.svg',
    'text'             : 'Geminación Fusión',
    'desc'             : 'Geminación Fusión',
    'tipo'             : 6,
    'reversible' : 1,
    'deciduoinverso' : 1,
    'offsetsvg' : {
        'x':-20 ,
        'y': -40,
        'y3': -25,
        'xtemp':-23,
        'ytemp': 145
    }
},
{
    'color'            : 'blue',
    'id'               : 17,
    'ico'              : '/tools/17_Giroversion.svg',
    'text'             : 'Giroversión',
    'desc'             : 'Giroversión',
    'svg'              : '/ops/17_Giroversion.svg',
    'tipo'             : 4,
    'class'            : ['operation--noturned', 'operation--rotated'],
        'offsetsvg' : {
                'y': 120,

             },
    'reversible' : 2,
},
{
    'color'            : 'blue',
    'id'               : 18,
    'ico'              : '/tools/18_Impactacion.svg',
    'text'             : 'Impactación',
    'desc'             : 'Impactación',
    'tipo'             : 4,
    'box'              : ['I'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 19,
    'ico'              : '/tools/19_Implante.svg',
    'text'             : 'Implante',
    'desc'             : 'Implante',
    'tipo'             : 4,
      'box'              : ['IMP'],
      'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 20,
    'ico'              : '/tools/20_Macrodoncia.svg',
    'text'             : 'Macrodoncia',
    'desc'             : 'Macrodoncia',
    'tipo'             : 4,
    'box'              : ['MAC'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 21,
    'ico'              : '/tools/21_Microdoncia.svg',
    'text'             : 'Microdoncia',
    'desc'             : 'Microdoncia',
    'tipo'             : 4,
       'box'              : ['MIC'],
       'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 22,
    'ico'              : '/tools/22_Migracion.svg',
    'svg'              : '/ops/22_Migracion.svg',
    'text'             : 'Migración',
    'desc'             : 'Migración',
    'tipo'             : 4,
    'class'            : ['operation--noturned', 'operation--rotated'],
            'offsetsvg' : {
                'y': 120,
 'y2': -40,
 'y3': -40,
             },
    'reversible' :0,

},
{
    'color'            : 'blue',
    'id'               : 23,
    'ico'              : '/tools/23_Movilidad.svg',
    'text'             : 'Movilidad',
    'desc'             : 'Movilidad',
    'tipo'             : 4,
    'box'              : ['M1','M2','M3'],
    'class'            : 'operation--blue',
    'txtclass'            : 'od-op-txt od-op-txt--blue',
},
{
 'class'            : ['operation--blue', 'operation--red'],
 'id'               : 24,
 'ico'              : '/tools/24_Perno.svg',
 'text'             : 'Perno M. Espigo. M.',
 'desc'             : 'Perno Muñón Espigo Muñón',
 'tipo'             : 4,
 'svg'              : '/ops/24_Perno.svg',
 'reversible'        : 1

},
{
    'color'            : 'blue',
    'id'               : 25,
    'ico'              : '/tools/25_ProtesisFija.svg',
    'text'             : 'Prótesis Fija',
    'desc'             : 'Prótesis Fija',
    'svg'              : '/ops/25_ProtesisFija_Medio.svg',
    'svg2'              : '/ops/25_ProtesisFija_Extremo.svg',
    'tipo'             : 1,
    'lastturned'        :1,
    'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },
},
{
    'color'            : 'blue',
    'id'               : 26,
    'ico'              : '/tools/26_ProtesisRemov.svg',
    'text'             : 'Prótesis Removible',
    'desc'             : 'Prótesis Removible',
    'svg'              : '/ops/26_ProtesisRemov.svg',
    'tipo'             : 1,
    'class'   : ['operation--blue', 'operation--red'],
        'reversible' : 1,
            'offsetsvg' : {
                'x': -2 ,
                'y': -65,
                'xtemp': -2,
                'ytemp': -35
             },

},
{
    'color'            : 'blue',
    'id'               : 27,
    'ico'              : '/tools/27_ProtesisTotal.svg',
    'class'   : ['operation--blue', 'operation--red'],
    'text'             : 'Prótesis Total',
    'desc'             : 'Prótesis Total',
    'svg'              : '/ops/27_ProtesisTotal.svg',
    'tipo'             : 2,
    'offsetsvg' : {
                'x': -7,
                'y': 85,
                'y2': 0,
                'y3': 0
             },
},
{
    'color'      : 'red',
    'id'         : 28,
    'ico'        : '/tools/28_RemRadic.svg',
    'text'       : 'Remanente Radicular',
    'desc'       : 'Remanente Radicular',
    'tipo'       : 4,
    'svg'        : '/ops/28_RemRadic.svg',
    'reversible' : 0
},
{
    'id'               : 29,
    'ico'              : '/tools/29_RestDef.svg',
    'text'             : 'Rest. Definitiva',
    'box'           :  ['AM','R','IV','IM','IE','CP'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'desc'             : 'Restauración Definitiva',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'restDef',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 30,
    'ico'              : '/tools/30_RestTemp.svg',
    'text'             : 'Rest. Temporal',
    'desc'             : 'Restauración Temporal',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'restTemp',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 31,
    'ico'              : '/tools/31_Sellantes.svg',
    'text'             : 'Sellantes',
    'desc'             : 'Sellantes',
    'tipo'             : 3,
    'zoom'             : true,
    'class'            : 'sellantes',
    'zones': ['v','o','m','l','d','r1','r2','r3']
},
{
    'id'               : 32,
    'ico'              : '/tools/32_SemiImpactacion.svg',
    'text'             : 'Semi-Impactación',
    'desc'             : 'Semi-Impactación',
    'tipo'             : 4,
    'box'              : ['SI'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'color'            : 'blue',
    'id'               : 33,
    'ico'              : '/tools/33_SuperfDesgast.svg',
    'text'             : 'Superficie Desgastada',
    'desc'             : 'Superficie Desgastada',
    'tipo'             : 4,
    'box'              : ['DES'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',
    'class'            : 'operation--blue',
},
{
    'id'               : 34,
    'ico'              : '/tools/34_Supernumerario.svg',
    'text'             : 'Super numerario',
    'desc'             : 'Supernumerario',
    'tipo'             : 6,
    'svg'              : '/ops/34_Supernumerario.svg',
    'offsetsvg':  {'x':0,'y':-20},
    'reversible' :1
},
{
    'id'               : 35,
    'ico'              : '/tools/35_Transposicion.svg',
    'svg'              : '/ops/35_Transposicion.svg',
    'text'             : 'Transposición',
    'desc'             : 'Transposición',
    'tipo'             : 6,
    'offsetsvg':  {'x':0,'y':-40,
 'ytemp': -20},
    'reversible': 1
},
{

    'id'   : 36,
    'ico'  : '/tools/36_TratPulpar.svg',
    'text' : 'Trat. Pulpar',
    'desc' : 'Tratamiento Pulpar',
    'tipo' : 3,
    'zoom': 1,
    'tipo'  : 3,
    'class' : 'tratpulpar',
    'zones': ['r1','r2','r3'],
    'fn' : 'drawMiddleLane',
    'box' : ['TC','PC','PP'],
    'txtclass'            : 'od-op-txt od-op-txt--blue',

},
{
    'id'               : 0,
    'ico'              : '/tools/00_Zoom.svg',
    'text'             : 'Zoom',
    'desc'             : 'Zoom',
    'tipo'             : 0,
    'default'   :1,
    'hidden' : 1,
    'zoom': 1
}

]
}