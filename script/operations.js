"use strict";
function Operation() {
	this.id = 0;
	this.idtypeoperation = 0;
	this.dom = {};
	this.teeth = [];
	this.text = '';
	this.dateop = new Date();
	this.status = 0; // 0 – mal estado 1- buen estado
	this.action = 1; //Nuevo / mantenida
	this.positions = []; //Solo para detalles de linea
}

var _replaceArray = ('dom id').split( ' '  );
function replacer(key, value) {
	if (_replaceArray.indexOf(key) >= 0){
		return undefined;
	}
	return value;
}
var operationToJSON = function(operation){
	// var operationCopy = $(this).clone(boolean, boolean);
	//var operationCopy = {};
	return JSON.stringify(operation,replacer)
	/*
	$.extend( operationCopy, operation );
	delete operationCopy['dom'];
	delete operationCopy['teeth'];
	return JSON.stringify(operationCopy);*/
}
var odontogramToJSON = function(odontogram,closed,genretaImg){
	var jsonCreator = {};
	jsonCreator.dateod = new Date();
	jsonCreator.observations = $('#txtobservations').val();
	jsonCreator.specifications = $('#txtspecifications').val();
	jsonCreator.active = 1;
	jsonCreator.closed = closed;
	jsonCreator.operations = [];
	if (!!odontogram.idodontogram) jsonCreator.idodontogram = odontogram.idodontogram;
	if(!!genretaImg) {
		jsonCreator.odontosvg = _generateImg();
	}
	for (var i = odontogram.operations.length - 1; i >= 0; i--) {
		jsonCreator.operations.push(JSON.parse(operationToJSON(odontogram.operations[i])));
	}
	return JSON.stringify(jsonCreator);
}

/*
var _drawSVGOperation = function(operation,tooth,data,$node) {
	var bbox = {};
	var $circChild = {};
	var $datadocument = $(data.documentElement);
	if ($node.find('#op'+operation.id)){

	}
	$datadocument.attr('id','op'+operation.id).addClass(operation.class);
	if (!!operation.class) {
		if ($.isArray(operation.class)) {
			$datadocument.addClass(operation.class[0]);
		}else		{
			$datadocument.addClass(operation.class);
		}
	}
	$node.append( $datadocument[0]);
}*/
var _checkClassSVG = function(configClass,operation,$datadocument,odont){
	if (!!configClass) {
		if ($.isArray(configClass)) {
			$datadocument.addClass(configClass[operation.status]);
			operation.class = configClass;
			if(configClass.length > 1){
				$datadocument.addClass('operation--click')
				$datadocument.click(function(){

					_clickStatusSVG(configClass,operation,odont);
				});
			}
		}else		{
			$datadocument.addClass(configClass);
			operation.class = configClass;
		}
	}
}

var _clickStatusSVG = function(configClass,operation,odont){
	var oldClass;
	var newClass;
	if($.isArray(configClass) > 0){
		operation.status++;
		operation.dateop = new Date();
		if ( operation.status >= configClass.length )	{
			operation.status= 0;
			oldClass = configClass[configClass.length-1];
			newClass = configClass[operation.status];
		}else{
			newClass = configClass[operation.status];
			oldClass = configClass[operation.status-1];
		}
		$('.'+oldClass,operation.dom).removeClass(oldClass).addClass(newClass);
		operation.dom.removeClass(oldClass).addClass(newClass);

		if(newClass == 'operation--rotated'){
			operation.dom.attr('transform',   operation.dom.attr('transform') + ' translate(50,0)  scale(-1,1)');
		}else if(oldClass){
			operation.dom.attr('transform', operation.dom.attr('transform').replace(' translate(50,0)  scale(-1,1)',''));
		}
		_writeHistoryOperation(operation, odont, operation.id, odont.configuration.constants.pathSVG);
	}
}
var _drawSVGOperation = function(operation,odont,$node) {
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var opname = _getOperationName(operation.teeth, operation.idtypeoperation);
	var $datadocument = {};

	_checkClassSVG(operConfig.class,operation,$node,odont);

	if (operConfig.tipo == odont.configuration.constants.tipotool.pieza ||
		operConfig.tipo == odont.configuration.constants.tipotool.dosdientes	)
	{
		$datadocument = operConfig.$svgdata.clone();
	//	_checkClassSVG(operConfig.class,operation,$datadocument,odont);

	$node.append( $datadocument[0]);
	_adjustPosition(operation,odont,$node,operConfig);
}
else{
	if(!!operConfig.$svgdata2){
		for (var i = operation.teeth.length - 1; i >= 0; i--) {
			var tooth = $.grep(odont.configuration.teeth, function(t) { return t.id == operation.teeth[i];})[0];
					//tooth.contiguous.length == 1  Eso es que es una esquina, por lo que tiene el data de la esquina
					if(tooth.contiguous.length == 1  ||
						!tooth.contiguous.every(function (tid) { return operation.teeth.indexOf(tid) >= 0 })) {
						$datadocument = operConfig.$svgdata2.clone();
				}
				else{
					$datadocument = operConfig.$svgdata.clone();
				}
		//		_checkClassSVG(operConfig.class,operation,$datadocument,odont);
		createNewGroup($node[0],operation.id+'_d'+tooth.id).append($datadocument[0]);
	}
	_adjustPosition(operation,odont,$node,operConfig);
}else{
	for (var i = operation.teeth.length - 1; i >= 0; i--) {
		$datadocument = operConfig.$svgdata.clone();
			//	_checkClassSVG(operConfig.class,operation,$datadocument,odont);

			createNewGroup($node[0],operation.id+'_d'+operation.teeth[i]).append($datadocument[0]);
		}
		_adjustPosition(operation,odont,$node,operConfig);
	}
}

}


var _checkOffsetConfig = function(operConfig,tooth){
	var offsetConfigx = 0;
	var offsetConfigy = 0;
	if (!!operConfig.offsetsvg) {

		if (tooth.position === 1 ||tooth.position === 2){
			offsetConfigx = !!operConfig.offsetsvg.xtemp ? operConfig.offsetsvg.xtemp : operConfig.offsetsvg.x;
			offsetConfigy = !!operConfig.offsetsvg.ytemp ? operConfig.offsetsvg.ytemp : operConfig.offsetsvg.y;
		}
		else{
			offsetConfigx=operConfig.offsetsvg.x;
			offsetConfigy=operConfig.offsetsvg.y;
		}
		if (operConfig.offsetsvg['x'+tooth.position] != null){
			offsetConfigx=operConfig.offsetsvg['x'+tooth.position];
		}
		if (operConfig.offsetsvg['y'+tooth.position] != null){
			offsetConfigy=operConfig.offsetsvg['y'+tooth.position];
		}
	}
	if (!offsetConfigx) offsetConfigx = 0;
	if (!offsetConfigy) offsetConfigy = 0;
	return  { "x": offsetConfigx, "y" : offsetConfigy};
}



var _adjustPosition = function(operation,odont,$node,operConfig){

	var x1=0;
	var x2=0;
	var y =0;
	var x = 0;
	var toothSelected = {};
	var teeth =  $.grep(odont.teeth , function(t) { return operation.teeth.indexOf(t.id) >=0 });

	var offsetConfig = _checkOffsetConfig(operConfig,teeth[0]);

	if (teeth.length == 2 && operConfig.tipo == odont.configuration.constants.tipotool.dosdientes){
		x1 = teeth[0].x;
		x2 = teeth[1].x;
		y = teeth[0].y;
		x = x1+(x2-x1)/2;
	}
	else if (teeth.length > 1){
		var xMax = {x:-1};
		var xMin = {x:10000};
		var mov = {x:0, y:0};
		for (var i = teeth.length - 1; i >= 0; i--) {
			if (operConfig.reversible) {
				if(teeth[i].position > 1){
					mov.x = -teeth[i].x;
					mov.y = -teeth[i].y;
				}else{
					mov.x = teeth[i].x;
					mov.y = teeth[i].y;
				}
			}else{
				mov.x = teeth[i].x;
				mov.y = teeth[i].y;
			}
			if(teeth[i].x > xMax.x) {
				xMax = teeth[i];
			}
			if(teeth[i].x < xMin.x) {
				xMin = teeth[i];
			}
			$('#'+operation.id + '_d'+teeth[i].id).attr('transform','translate('+mov.x +','+mov.y+') ');
		}
		if (!!operConfig.lastturned) 	{
			if (operConfig.reversible) {
				if(xMax.position > 1){
					$('#'+operation.id + '_d'+xMin.id).attr('transform', $('#'+operation.id + '_d'+xMin.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
				}
				else{
					$('#'+operation.id + '_d'+xMax.id).attr('transform', $('#'+operation.id + '_d'+xMax.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
				}
			}else{
				$('#'+operation.id + '_d'+xMax.id).attr('transform', $('#'+operation.id + '_d'+xMax.id).attr('transform')  + ' translate(50,0)  scale(-1,1) ' );
			}
		}
	}
	$node.attr('transform','');
	if (!!operConfig.reversible) {
		if(teeth[0].position > 1){
			if(operConfig.reversible == 1) {
				$node.attr('transform',' translate(50,130)  scale(-1,-1) ');
			}else{
				$node.attr('transform',' translate(0,130)  scale(1,-1) ');
			}
		}
	}

	$node.attr('transform','translate('+x+','+y+') '+  $node.attr('transform') + 'translate('+offsetConfig.x+','+ offsetConfig.y+')');
	if($node.hasClass('operation--rotated')){
		$node.attr('transform',   $node.attr('transform') + ' translate(50,0)  scale(-1,1)');
	}
}

var disableHistory = function(disableHover){
	$('.od-btn','.od-history').addClass ('od-btn--disabled');
	if(!!disableHover){
		$('.od-history','.od-list--selectable').addClass('disabled');
	}
}
var enableHistory = function(){
	$('.od-btn','.od-history').removeClass ('od-btn--disabled');
	$('.od-history','.od-list--selectable').removeClass('disabled');
}
var _writeHistoryOperation = function(operation,odont ,id,pathSVG){

	if($('#menu-ops').find('#hist_'+id).length==0){
		var linew = $('<li/>' ,{'class': 'od-list__item od-history', 'id': 'hist_'+id});
		var textOp ='';
		if (operation.teeth.length <3) {
			var strzone= '';
			if (!!operation.zone){ strzone =' (' + operation.zone + ')';}
			textOp = operation.teeth.toString().replace(/[,]/g,'-') + strzone;
		}else{
			var min = 1000;
			var max = -1;
			var position= $.grep(odont.configuration.teeth, function(t) { return t.id == operation.teeth[0]; })[0].position;
			for (var i = operation.teeth.length - 1; i >= 0; i--) {

				var comp = odont.teethByPositions[position].indexOf(operation.teeth[i]);
				if(comp != -1){
					if(min >comp){
						min = comp;
					}
					if (max < comp){
						max= comp;
					}
				}
			}
			textOp = '['+ odont.teethByPositions[position][min]+ '-' + odont.teethByPositions[position][max]+']';
		}
		var spanId = $('<span/>',{'class': 'od-history__id'}).html(textOp);
		var spanDesc = $('<span/>',{'class': 'od-history__desc'}).html(operation.text);
		var btn = $('<button/>',{'class': 'od-btn od-btn--primary od-btn--rounded od-btn--xs'}).attr('data-balloon','Eliminar operación').attr('data-balloon-pos','left');
		var img = $('<img/>',{'class': 'od-ico'}).attr('src',pathSVG + '/ui/delete.svg');
		linew.append(spanId);
		linew.append(spanDesc);
		btn.append(img);
		linew.append(btn);
		btn.bind('click', { odontogram : odont  },deleteOperationEvent);
		linew.hover( function(){
			odont.disableTeethExceptOperation(id);
		},function(){
			odont.enableTeeth();
		});
		$('#menu-ops').prepend(linew);
		if(odont.disabled){
			btn.addClass('od-btn--disabled');
		}
	}else{
		var $li = $('#menu-ops').find('#hist_'+id);
		$li.parent().prepend($li);
	}
}
var _checkSVGData = function(operation, odont,callback){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	callback = callback || function(){};
	$.ajax({
		type: 'GET',
		url: odont.configuration.constants.pathSVG + operConfig.svg,
		datatype: 'text'
	}).done(function(data){
		if (!!operConfig.svg2){
			$.ajax({
				type: 'GET',
				url: odont.configuration.constants.pathSVG + operConfig.svg2,
				datatype: 'text'
			}).done(function(data2){
				operConfig.$svgdata = $(data.documentElement);
				operConfig.$svgdata2 = $(data2.documentElement);
				callback(operation,odont);

			});
		}
		else{
			operConfig.$svgdata = $(data.documentElement);
			callback(operation,odont);
		}
	});
}

var _drawOperation = function(operation,odont){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	//SVG data contiene la cache de los svgs para no tener que consultarlos todo el rato
	if(!operConfig.$svgdata)
	{
		//Compruebo si tiene imagen
		if(!!operConfig.svg)
		{
			_checkSVGData(operation,odont,_drawOperation);
			//Si entra por aquí  tiene que acabar
			return;
		}
	}
	else{
		var $node= {};
		if (operConfig.tipo == odont.configuration.constants.tipotool.pieza){
			$node = $('#'+operation.id);
			if (!$node.length){
				var tooth = $.grep(odont.teeth, function(t) { return t.id == operation.teeth[0];})[0];
				$node = createNewGroup(tooth.$map[0],operation.id);
				$node.attr('class', 'operation');
			}
		}
		else{
			$node = $(odont.datadocument).find('#'+operation.id);
			if(!$node.length) {
				$node = createNewGroup(odont.datadocument,operation.id);
				$node.attr('class', 'operation');
			}

		}
		_drawSVGOperation(operation,odont,$node);
		operation.dom = $node;
	}
	if (operation.positions.length > 0){
		//if(operConfig.tipo == odont.configuration.constants.tipotool.detallelinea) {
			_drawLine(operation,odont);
		//}
	}
	else{
		if(!!operation.zone){
			_changeClassTooth(operation,odont,1);
		}
	}
	if (!!operation.box){
	//	if(!operation.class && !!operConfig.class){ operation.class = $.isArray(operConfig.class)? operConfig.class[0] : operConfig.class; }
		$.each(operation.teeth, function(index, val) {
			$.grep(odont.teeth, function(t) { return t.id == val })[0].box.push({
				'text' : operation.box,
				'class' : operConfig.txtclass,
				'id' : operation.id,
			}
			);
		});
		_writeBox(operation.teeth,odont);
	}
	_writeHistoryOperation(operation, odont, operation.id, odont.configuration.constants.pathSVG);
}
var _changeClassTooth = function(operation,odont,insert){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var tooth = $.grep(odont.teeth,function(e){return e.id ==  operation.teeth[0]})[0];
	if (insert){
		tooth.$datadocument.find('#'+operation.zone).addClass(operConfig.class);
		tooth.$datadocument.find('#'+operation.zone).children().addClass(operConfig.class);
		operation.class = operConfig.class;
	}else{
		tooth.$datadocument.find('#'+operation.zone).children().removeClass(operConfig.class);
		tooth.$datadocument.find('#'+operation.zone).removeClass(operConfig.class);
		operation.class = operConfig.class;
	}
}
var _drawLine = function(operation,odont){
	var operConfig = $.grep(odont.configuration.tools,function(e) { return e.id == operation.idtypeoperation })[0];
	var line = {};
	var nameOp = '';
	var tooth = $.grep(odont.teeth,function(e){return e.id ==  operation.teeth[0]})[0];
	var group = {};
	nameOp = !!operation.zone ?  _getOperationName(operation.teeth,operConfig.id,operation.zone) : _getOperationName(operation.teeth,operConfig.id);
	group = tooth.$datadocument.find('#'+nameOp);


	if(!group.length) {
		group =  !!operation.zone ?	createNewGroup(tooth.$datadocument.find('#'+operation.zone)[0],nameOp) : createNewGroup(tooth.$datadocument[0],nameOp) ;
		group.attr('class', 'operation');
	}
	for (var i = operation.positions.length - 1; i >= 0; i--) {
		var pos = operation.positions[i];
		if (!!pos.x1)
		{
			line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
			line.setAttributeNS(null, 'x1', pos.x1);
			line.setAttributeNS(null, 'y1', pos.y1);
			line.setAttributeNS(null, 'x2', pos.x2);
			line.setAttributeNS(null, 'y2', pos.y2);
			line.setAttributeNS(null, 'class', operConfig.class);
			operation.class = operConfig.class;
			group.append(line);
		}
	}
	operation.dom = group;
}

var deleteOperationEvent = function(event) {
	var odont = event.data.odontogram;
	var histOper = $(this).closest('.od-history');
	var id = histOper.attr('id').replace('hist_','');
	var oper = $.grep(odont.operations,function(e) { return e.id == id })[0];
	if(!!oper){
		deleteOperation(oper,odont);
		odont.enableTeeth();
	}
}
var deleteOperation = function(operation,odont,deleteBox){
	var histOper =  $('#menu-ops').find('#hist_'+operation.id);

	if (!!operation.box && !deleteBox){
		$.each(operation.teeth, function(index,val){
			var tooth = $.grep(odont.teeth, function(t) { return t.id == val})[0];
			tooth.box = tooth.box.filter(function( obj ) {
				return (!(obj.id == operation.id ));
			});
		});
		_writeBox(operation.teeth,odont);
	}
	if (!!deleteBox){
			$.each(operation.teeth, function(index,val){
			var tooth = $.grep(odont.teeth, function(t) { return t.id == val})[0];
			tooth.box = [];
		})
		_writeBox(operation.teeth,odont);
	}
	if (operation.dom.length > 0) {
		operation.dom.remove();
	}
	else{
		_changeClassTooth(operation, odont, 0);

	}

	odont.operations = $.grep(odont.operations, function(op) { return op.id !== operation.id});
	histOper.remove();
}

var _clearOperations = function(odont) {
	var oper = {};
	var histOper = {};
	for (var i = odont.operations.length - 1; i >= 0; i--) {
		oper = odont.operations[i];
		deleteOperation(oper,odont,1);
		/*histOper = $('#menu-ops').find('#hist_'+oper.id);
		if (oper.dom.length > 0) oper.dom.remove();
		histOper.remove();*/
	}
	odont.operations = [];
}




///Funciones para dibujar el tratamiento pulpar
function drawMiddleLane(triangulo,operation,operConfig) {
	var t = triangulo.find('polygon');
	var myPoints=t.attr('points').split(" ");

	var finalPoints=[];
	for (var i=0; i<myPoints.length; i++){
		if (myPoints[i].replace(/\s/g, '').length) {
			var arrayMyPoints =  myPoints[i].split(",");
			finalPoints[i] = [];
			finalPoints[i][0] = parseFloat(arrayMyPoints[0]);
			finalPoints[i][1] = parseFloat(arrayMyPoints[1]);
		}
	}
	var matrizSorted=_calculaVerticeDispar(finalPoints);
  //calculamos coordenadas finales
  var _x1, _y1, _x2, _y2;
  _x1=finalPoints[matrizSorted[0]][0];
  _y1=finalPoints[matrizSorted[0]][1];
  _x2=(finalPoints[matrizSorted[1]][0]+finalPoints[matrizSorted[2]][0])/2;
  _y2=(finalPoints[matrizSorted[1]][1]+finalPoints[matrizSorted[2]][1])/2;
  //dibujamos la linea
  var line =  document.createElementNS('http://www.w3.org/2000/svg', 'line');
  line.setAttributeNS(null, 'x1', _x1);
  line.setAttributeNS(null, 'y1', _y1);
  line.setAttributeNS(null, 'x2', _x2);
  line.setAttributeNS(null, 'y2', _y2);
  //line.setAttributeNS(null, 'stroke', "red");
  line.setAttributeNS(null, 'class', operConfig.class);
  operation.positions = [{
  	x1: _x1,
  	y1: _y1,
  	x2: _x2,
  	y2: _y2
  }];
  triangulo.append(line);
  operation.dom = line;
}
function _calculaVerticeDispar(matriz){
//metemos los y en un array
pointsY=[];
for (var i=0; i<matriz.length; i++){
	pointsY[i]=matriz[i][1];
}
  //calculamos los puntos de los bordes
  maxIndex=_indexOfMax(pointsY);
  minIndex=_indexOfMin(pointsY);

  //y descartamos el que no es
  var discarded, dispar, otro;
  for (var i=0; i<matriz.length; i++){
  	if ((i!=maxIndex) && (i!=minIndex) ){
  		discarded=i;
  		break;
  	}
  }


  //calculamos las diferencias entre el descartado y los otros dos y el más separado es el dispar
  var dist1=Math.abs(pointsY[discarded]-pointsY[maxIndex]);
  var dist2=Math.abs(pointsY[discarded]-pointsY[minIndex]);
  if (dist1>dist2) {
  	dispar=maxIndex;
  	otro=minIndex;
  } else {
  	dispar=minIndex;
  	otro=maxIndex;
  }


  var matrizSorted=[]
  matrizSorted[0]=dispar;
  matrizSorted[1]=discarded;
  matrizSorted[2]=otro;
  return matrizSorted;


}

function _indexOfMax(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var max = arr[0];
	var maxIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] > max) {
			maxIndex = i;
			max = arr[i];
		}
	}

	return maxIndex;
}
function _indexOfMin(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var min = arr[0];
	var minIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] < min) {
			minIndex = i;
			min = arr[i];
		}
	}

	return minIndex;
}
var _writeBox = function(teeth,odont){
	var text=null;
	for (var i = 0; i < teeth.length; i++) {
		var tooth = $.grep(odont.teeth, function(t) { return t.id == teeth[i] })[0];
		iniX=2;
		incY=20;
		incX=5;
		posX=iniX;
		posY=0;
		maxX=48;
		maxY=50;
		fontSize = 22;
		 text = tooth.$recuadrotooth.find('text');
		if(tooth.box.length > 0){
			text.attr('style','font-size:'+fontSize+'px');
			_continueWriteBox(tooth.box, text)
		}else{
				text.children().remove();
		}
	}

}
var iniX=2;
var incY=20;
var incX=5;
var posX=iniX;
var posY=0;
var maxX=48;
var maxY=50;
var fontSize = 22;

var _continueWriteBox = function(opers,texto){
	texto.children().remove();
	for (var i = 0; i < opers.length ; i++) {
		var tspan =  document.createElementNS('http://www.w3.org/2000/svg','tspan');
		tspan.textContent = opers[i].text;
		if(!!opers[i].class){ tspan.setAttribute('class',opers[i].class); }
		tspan.setAttribute('dx',incX + 'px');

		texto.append( tspan);

		var widthTspan = texto[0].getBBox().width;

		if(texto.children().length > 1){
			if (widthTspan  > maxX){
				posX=iniX;
				tspan.setAttribute('dx','0px');
				tspan.setAttribute('x',posX + 'px');

				posY=incY;
			} else {
				posY=0;
			}
		}else{
			tspan.setAttribute('dx', iniX+ 'px');
       //  tspan.setAttribute('y',(incY)+'px');
       posY = incY;

   }
   tspan.setAttribute('dy',posY +'px');


}

//tspan.getBBox().height
if(texto[0].getBBox().height > maxY){
	fontSize= fontSize-0.5;
	incY = fontSize *1.1;
	posY=0;

	texto.attr('style','font-size:'+fontSize+'px');
	_continueWriteBox(opers,texto);
	return;
}
}