"use strict";
jQuery.fn.extend({
	createOd: function(customConfig) {
		var _this = this;
		var Odon = new Odontogram();
		customConfig = customConfig || function(){},

		Odon.configure(_this[0].id,customConfig);

		return Odon;
	},
	clearOd: function(){
		return '' ;
	}
});
$.attrHooks['viewbox'] = {
	set: function(elem, value, name) {
		elem.setAttributeNS(null, 'viewBox', value + '');
		return value;
	}
};