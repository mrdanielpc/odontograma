"use strict";

var _toolboxGenerate = function(odontogram,elementId){
  //var divulNew = $('<div/>',{'class': 'od-toolbar-div '});
  var ulNew = $('<ul/>' ,{'class': 'od-toolbar'});
  var _elementId = elementId;
  var items = [];
  var ulTools = {};
  var liContainer = {};
//divulNew.append(ulNew);
  ulNew.append($('<li/>', {
   'class': 'od-toolbar__title',
   html: 'Operaciones'
 }));

  ulNew.append(
    $('          <li class="od-toolbar__tool--selected">' +
      '            <div class="od-media od-media--top ">' +
      '              <div class="od-media__image">' +
      '                <img >' +
      '              </div>' +
      '              <div class="od-media__body  ">' +
      '                <h5 class="od-media__title" ></h4>' +
      '' +
      '<div class="od-media__options od-media__options--left">' +
      '</div>' +
      '                </div>' +
      '              </div>' +
      '            </li>'));

//despu�s de a�adir la lista de seleccionadas hacemos un li que va a contener todas las tools
 liContainer = $('<li/>' ,{'class': 'od-container'});
 ulNew.append(liContainer);

  ulTools = $('<ul/>' ,{'class': 'od-tools'});

  $.each( odontogram.configuration.tools, function( key, val ) {
    val.ico = odontogram.configuration.constants.pathSVG + val.ico;
    var pos = 'up';
    if(key % 4 == 0){
      pos = 'right';
    }
    else if(key % 4 == 3){
      pos = 'left';
    }
    else if(key <4){
       pos = 'down';
    }
    if (!!val.default && !val.hidden )
    {
      odontogram.toolSelected = val;
      ulTools.append(
       $('<li data-balloon="' + val.desc + '" data-balloon-pos="'+pos+'"><button class="od-toolbar__btn od-btn--active" data-develop="'+ !!val.develop +'" data-zoom="'+ !!val.zoom +'" data-idtool="'+val.id+'"><img class="od-btn__ico" src="' + val.ico + '">' +
        '<span class="od-btn__text">' + val.text + '</span>' +
        '</button></li>')
       );
    }
    else if(!val.hidden){
      ulTools.append(
       $('<li data-balloon="' + val.desc + '" data-balloon-pos="'+pos+'"><button class="od-toolbar__btn"  data-develop="'+ !!val.develop +'"  data-zoom="'+ !!val.zoom +'" data-idtool="'+val.id+'"><img class="od-btn__ico" src="' + val.ico + '" >' +
        '<span class="od-btn__text">' + val.text + '</span>' +
        '</button></li>')
       );
    }
  });

  liContainer.append(ulTools);
  ulNew.append(liContainer);
  ulNew.appendTo('#' + elementId);
  odontogram.toolSelectDefault();

  $('.od-toolbar__btn').click(function(event) {
   var $this = $(this);
   if ($this.hasClass('od-btn--active')){
    odontogram.toolSelectDefault();
  }
  else	{
    $('.od-btn--active').removeClass('od-btn--active');
    $this.addClass('od-btn--active');
    odontogram.toolSelected = $.grep(odontogram.configuration.tools, function(obj) {
      return obj.id == $this.attr('data-idtool');
    })[0];
    if(!!odontogram.toolSelected.box) {
      odontogram.toolSelected.boxSelected= odontogram.toolSelected.box[0];
    }else{
      odontogram.toolSelected.boxSelected= null;
    }
  }
}
);
}

var disableTools = function(){
 $('.od-toolbar__btn').addClass ('od-btn--disabled');
 $('textarea','.panel__body').attr('disabled','disabled');
}

var disableZoomTools = function(){
  $('.od-toolbar__btn[data-zoom!=true]').addClass('od-btn--disabled');
}

var enableTools = function(){
  $('.od-toolbar__btn').removeClass ('od-btn--disabled');
 $('textarea','.panel__body').removeAttr('disabled');
}

var printCssData = undefined;
var _loadPrintCss = function(printcss){
  $.ajax({
    type: 'GET',
    url: printcss,
    datatype: 'text',
  }).done(function(data) {
    printCssData = data;
  });
}
var _generateImg = function(){
  if (!!printCssData){
    var svg  = $(document.querySelector( "svg" )).clone().removeAttr('width').removeAttr('height')[0];
    var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rect.setAttribute('fill', 'white');
    rect.setAttribute('width', '100%');
    rect.setAttribute('height', '100%');
    svg.insertBefore(rect,svg.firstChild);

    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = printCssData;
    svg.insertBefore(style,svg.firstChild);

    var svgString = new XMLSerializer().serializeToString(svg);
    var data = "data:image/svg+xml;base64," + btoa(svgString);
    return  data;
  }
}








